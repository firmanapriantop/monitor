<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MONITOR | Monitoring and Evaluation External Loan Project</title>

	<!-- Web fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

	

	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- <link href="<?php //echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css"> -->
	<link href="<?php echo base_url(); ?>assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	
	
	
</head>

<body class="sidebar-xs has-detached-right">
	<div id="tmpModal"></div>
	
	<!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header" >
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block ">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				
			</ul>

			

			<p class="navbar-text">
			</p>


			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" alt="">
						<span><?php echo $this->session->userdata('nama_depan').' '. $this->session->userdata('nama_belakang').' '.$this->session->userdata('id_user_level'); ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="<?php echo base_url(); ?>dashboard/logout"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content" >

					<!-- User menu -->
					<!-- 
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/face11.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">
										<?php 
											//echo $this->session->userdata('nama_depan').' '. $this->session->userdata('nama_belakang'); 
											
										?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Super Administrator
									</div>
								</div>
							</div>
						</div>
					</div>
-->
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
								<li <?php if($this->uri->segment(1)=="dashboard" or $this->uri->segment(1)==""){ echo "class='active'"; }?>><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								
								<li <?php if($this->uri->segment(1)=="proyek"){ echo "class='active'"; }?>><a href="<?php echo base_url(); ?>proyek"><i class="icon-stack2"></i> <span class="text-bold">Projects</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span class="text-bold">Rekapitulasi</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Cetak Laporan</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Pengumuman</span></a></li>
								<li class="navigation-header"><span>Data Master</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Instansi</span></a></li>
								<li><a href="<?php echo base_url(); ?>user_role"><i class="icon-stack2"></i> <span>User Role</span></a></li>
								<li class="navigation-header"><span>Documentation</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
								<li><a href="<?php echo base_url(); ?>"><i class="icon-stack2"></i> <span>Manual</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

