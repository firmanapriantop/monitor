
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MONEV PHLN | Sistem Informasi Pemantauan dan Evaluasi Pelaksanaan Kinerja Proyek Pinjaman dan/atau Hibah Luar Negeri</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<?php echo form_open('dashboard/do_login',array('name'=>'login_form', 'class'=>'m-t'));?>
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
								<h5 class="content-group"><span class="text-semibold">Login to MONEV PHLN</text> <small class="display-block">Kementerian PPN/BAPPENAS</small></h5>
							</div>
							<?php if ($this->session->flashdata()) { ?>
								<div class="alert alert-warning">
									<?php $this->session->flashdata('msg'); ?>
								</div>
							<?php } ?>
							<div class="form-group has-feedback has-feedback-left">
								<input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<input type="text" value="<?php echo $current_link; ?>" name="current_link">

							<div class="text-right">
								<a href="login_password_recover.html">Forget Password?</a>
							</div>
						</div>
					</form>
					<!-- /simple login form -->
					<div class="login-form">
						<div class="text-center">
							Don't have an account? <a href="<?php  echo base_url().'sign-up.html'; ?>">Sign Up</a>
						</div>
						<div class="text-center">
							Email aktivasi belum terkirim, <a href="<?php  echo base_url().'register.html'; ?>">Kirim Ulang!</a>
						</div>
					</div>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
