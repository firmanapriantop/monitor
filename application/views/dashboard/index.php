<!--

<div class="page-header page-header-inverse" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd; ">
    <div class="page-header-content bg-indigo-400 bg-image overflow-hidden" style="background: url(<?php //echo base_url(); ?>assets/images/backgrounds/boxed_bg.png); background-position: 0 50%; margin: 0 auto;">
        <div class="page-title">
            <h5>-->
                <!-- 
                <span><img src="<?php //echo base_url(); ?>assets/css/images/logo_icon_light.png" height="42" width="42">--><!--</span>Selamat Datang di  <span class="text-semibold">MONITOR</span>
                <small class="display-block">Sistem Informasi Pemantuan dan Evaluasi Perkembangan Pelaksanaan Proyek Yang Dibiayai Dari Pinjaman Luar Negeri</small>
            </h5>
        </div>

        <div class="heading-elements">
            <button class="btn btn-link btn-icon btn-sm heading-btn"><i class="icon-gear"></i></button>
        </div>
    </div>
</div>
-->

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-home2 position-left"></i> <span class="text-semibold">Dashboard</span> - MONITOR</h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>" class="active"><i class="icon-home2 position-left"></i> Dashboard</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row" >
        <div id="info">
        </div>
        
    </div>

    <div class="row" id="grafik">
        

    </div>



</div>
<!-- /content area -->


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>
    $(document).ready(function(){

        loadData("<?php echo base_url().'dashboard/info'; ?>", "info");

        loadData("<?php echo base_url().'dashboard/grafik'; ?>", "grafik");

    });
  </script>