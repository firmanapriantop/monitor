<div class="row"> 
    <div class="col-md-6" id="perkembangan_pinjaman">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">Perkembangan Pinjaman<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
            </div>
            <!--  Grafik line chart, perkembangan nilai, dan realisasi per tahun -->
            <div class="panel-body" id="line_chart">
                
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">Sebaran Pinjaman Berdasarkan Sektor<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="panel-body" id="pie">
                Grafik line chart, perkembangan nilai, dan realisasi per tahun
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">Sebaran Pinjaman Berdasarkan Lender<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            </div>
            <!--  Grafik line chart, perkembangan nilai, dan realisasi per tahun -->
            <div class="panel-body" id="pie_lender">
            </div>
        </div>
    </div>





</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>



<script>
    $(document).ready(function(){
      

        $('#line_chart').highcharts({
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },

            yAxis: {
                title: {
                    text: 'Nilai'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2018
                }
            },
            series: [
                {
                    name: 'Pinjaman',
                    data: [524450000]
                }, 
                {
                    name: 'Penyerapan',
                    data: [<?= $penyerapan ?>]
                },
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });

        var data = [
			<?php foreach($sektor->result() as $row): ?>
				{ name: '<?= $row->sektor ?>', y: <?= $row->nilai ?>},
			<?php endforeach; ?>
		];

        $('#pie').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Pinjaman',
                colorByPoint: true,
                data: data,
            }]
        });

        var data_lender = [
			<?php foreach($mitra->result() as $row): ?>
				{ name: '<?= $row->mitra ?>', y: <?= $row->nilai ?>},
			<?php endforeach; ?>
		];

        $('#pie_lender').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Pinjaman',
                colorByPoint: true,
                data: data_lender,
            }]
        });
    });
</script>