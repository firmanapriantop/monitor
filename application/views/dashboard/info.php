<div class="col-lg-8">
    <div class="row">
        <div class="col-lg-4" id="on_going">
            <div class="panel bg-success-400">
                <div class="panel-body">
                    <h3 class="no-margin">$<?= number_format($on_going->nilai/1000000, 2) ?>Jt / $<?= number_format($on_going->realisasi/1000000, 2) ?>Jt</h3>
                    On Schedule
                    <div class="text-muted text-size-small"><?= number_format($on_going->jlh) ?> proyek</div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel bg-warning">
                <div class="panel-body">
                    <h3 class="no-margin">$<?= number_format($behind->nilai/1000000, 2) ?>Jt / $<?= number_format($behind->realisasi/1000000, 2) ?>Jt</h3>
                    Behind Schedule
                    <div class="text-muted text-size-small"><?= number_format($behind->jlh) ?> proyek</div>
                </div>

                <div id="today-revenue"></div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel bg-danger-800">
                <div class="panel-body">
                    <h3 class="no-margin">$<?= number_format($risk->nilai/1000000, 2) ?>Jt / $<?= number_format($risk->realisasi/1000000, 2) ?>Jt</h3>
                    At Risk
                    <div class="text-muted text-size-small"><?= number_format($risk->jlh) ?> proyek</div>

                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="col-lg-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel bg-indigo-400">
                <div class="panel-body">
                    <h4 class="no-margin">$<?= number_format($closed->nilai/1000000, 2) ?>Jt / $<?= number_format($closed->realisasi/1000000, 2) ?>Jt</h4>
                    Closed
                    <div class="text-muted text-size-small"><?= number_format($closed->jlh) ?> proyek</div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel bg-info-400">
                <div class="panel-body">
                    <h4 class="no-margin">$<?= number_format($canceled->nilai/1000000, 2) ?>Jt / $<?= number_format($canceled->realisasi/1000000, 2) ?>Jt</h4>
                    Canceled
                    <div class="text-muted text-size-small"><?= number_format($canceled->jlh) ?> proyek</div>
                </div>
            </div>
        </div>
    </div>
</div>