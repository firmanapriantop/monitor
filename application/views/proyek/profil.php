<div class="panel-heading panel-white">
    <h6 class="panel-title">
        <i class="icon-git-commit position-left"></i> 
            Profil
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs heading-btn" 
            id="reload_profil">
                <b><i class="icon-loop position-left"></i></b>
            </button>
        </div>
    </div>
</div>



<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td style="width: 20%;">
                    Nama Proyek
                </td>
                <td class="text-bold">
                    <?php echo $proyek->nama_en; ?> <br>
                    <small><?php echo $proyek->nama_id; ?></small>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Nilai Pinjaman
                </td>
                <td class="text-bold">
                    <small><?php echo $proyek->mata_uang; ?> </small>
                    <?php echo number_format($proyek->nilai, 2); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Nomor 
                </td>
                <td class="text-bold">
                    No. NPPHLN : <?php echo $proyek->no_npphln; ?> / 
                    No. Register : <?php echo $proyek->no_register; ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Tanggal Penandatanganan PHLN 
                </td>
                <td class="text-bold">
                    <?php echo date("j F Y", strtotime($proyek->tgl_ttd)); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Tanggal Efektif tentatif 
                </td>
                <td class="text-bold">
                    <?php echo date("j F Y", strtotime($proyek->tgl_efektif_tentatif)); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Tanggal Efektif Riil 
                </td>
                <td class="text-bold">
                    <?php echo date("j F Y", strtotime($proyek->tgl_efektif_riil)); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Tanggal Penutupan NPPHLN (Original) 
                </td>
                <td class="text-bold">
                    <?php echo date("j F Y", strtotime($proyek->tgl_tutup_original)); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    Tanggal Penutupan NPPHLN (Actual) 
                </td>
                <td class="text-bold">
                    <?php echo date("j F Y", strtotime($proyek->tgl_tutup_actual)); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">Masa Pakai</td>
                <td class="text-bold">
                    <?php //echo number_format($proyek->waktu_terpakai, 2); ?>%
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">Instansi Penanggungjawab</td>
                <td class="text-bold"><?php echo $proyek->instansi; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Instansi Pelaksana</td>
                <td class="text-bold"><?php echo $proyek->instansi_pelaksana; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Sektor</td>
                <td class="text-bold"><?php echo $proyek->sektor; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Ruang Lingkup</td>
                <td class="text-bold"><?php echo $proyek->ruang_lingkup; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Tujuan</td>
                <td class="text-bold"><?php echo $proyek->tujuan; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Sasaran</td>
                <td class="text-bold"><?php echo $proyek->sasaran; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
    
        $('#reload_profil').on('click', function() {
            loadData("<?php echo base_url().'proyek-profil/'.$proyek->parent_id; ?>", "profil");
        });

    });

</script>