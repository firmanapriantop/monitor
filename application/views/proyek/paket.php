<div class="panel-heading">
    <h6>
        <span class="text-semibold">
            Paket Kontrak
        </span>
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
            id="reload_paket">
                <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_paket">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">Paket Kontrak</th>
            <th rowspan="2">Kategori</th>
            <th rowspan="2">Nomor</th>
            <th rowspan="2">Tanggal Kontrak</th>
            <th rowspan="2">Tanggal Akhir</th>
            <th rowspan="2">Waktu Terpakai</th>
            <th rowspan="2">Nilai</th>
            <th rowspan="2">Penyerapan</th>
            <th rowspan="2">%</th>
            <th colspan="2" class="text-center">PENELAAHAN</th>
            <th rowspan="2">Aksi</th>
        </tr>
        <tr>
            <th>PPN</th>
            <th>KEU</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
                //$id_proyek = $row->id_proyek;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td>
                        
                        <a href="">
                            <?php echo $row->nama; ?>
                        </a>
                    </td>
                    <td><?php echo $row->kat; ?></td>
                    <td><?php echo $row->no; ?></td>
                    <td><?php echo $row->tgl; ?></td>
                    <td><?php echo $row->tgl_akhir; ?></td>
                    <td>
                        9.88%
                    </td>
                    <td>
                        <small class='pull-left text-muted'>
                            <?php echo $row->mata_uang; ?>
                        </small> 
                        <?php echo number_format($row->nilai/1000000, 2); ?>
                    </td>
                    <td>
                        <small class='pull-left text-muted'>
                            <?php echo $row->mata_uang; ?>
                        </small> 
                        <?php echo number_format($row->penyerapan_kum/1000000, 2); ?>
                    </td>
                    <td><?php echo number_format($row->persen, 2); ?>%</td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().'proyek-paket-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" data-popup="tooltip" title="Edit" data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().'proyek-paket-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" data-popup="tooltip" title="Edit" data-placement="bottom">
                                            <i class="icon-pencil7"></i> Addendum
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger" onclick="deleteData('<?php echo base_url().'proyek-paket-delete/'.$row->id; ?>', '<?php echo base_url().'proyek-paket/'.$row->parent_id_proyek; ?>', 'paket')" data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        $('#reload_paket').on('click', function() {
            loadData("<?php echo base_url().'proyek-paket/'.$parent_id_proyek; ?>", "paket");
            loadData("<?php echo base_url().'proyek-paket-target-realisasi/'.$parent_id_proyek; ?>", "paket_target_realisasi");
        });

        $('#add_paket').click(function(){
            CallPage("<?php echo base_url().'proyek-paket-add/'.$parent_id_proyek; ?>", "tmpModal", "modalAdd");
        });

    });

</script>