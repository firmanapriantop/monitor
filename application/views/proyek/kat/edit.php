<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Edit Data</h6>
            </div>

            <form class="form-horizontal" action="#" 
            id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nama">
                                    Nama
                                </label>
                                <input type="text" name="nama" 
                                id="nama" class="form-control" 
                                placeholder="Masukkan nama kategori"
                                value="<?php echo $data->nama; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nilai">
                                    Nilai
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-cash2"></i>
                                    </span>
                                    <input type="text" name="nilai" id="nilai" 
                                    class="form-control" 
                                    placeholder="Masukkan nilai" 
                                    value="<?php echo $data->nilai; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nama">
                                    Keterangan
                                </label>
                                <textarea rows="5" cols="5" name="ket" 
                                id="ket" class="form-control" 
                                placeholder="Masukkan keterangan"><?php echo $data->ket; ?></textarea> 
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" id="id"  
                    class="form-control" value="<?php echo $data->id; ?>">
                    <input type="hidden" name="parent_id_proyek" id="parent_id_proyek"  
                    class="form-control" value="<?php echo $data->parent_id_proyek; ?>">
                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

            var me 			    = $(this);
            
            var id = $("#id").val();
            var parent_id_proyek = $("#parent_id_proyek").val();
            var nama = $("#nama").val();
            var nilai = $("#nilai").val();
            var ket = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('parent_id_proyek', parent_id_proyek);
            form_data.append('nama', nama);
            form_data.append('nilai', nilai);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-kategori-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
                        loadData("<?php echo base_url().'proyek-kategori/'.$data->parent_id_proyek; ?>", "kat");
						notif("Informasi", "Data berhasil disimpan.");
                        
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
