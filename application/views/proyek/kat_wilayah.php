<div class="panel-heading">
    <h6 class="panel-title">
        <i class="icon-git-commit position-left"></i> 
            Kategori PHLN per Wilayah
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
            id="reload_kat_wilayah">
                <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_kat_wilayah">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>
<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Kategori</th>
            <th>Lokasi</th>
            <th>Nilai</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->kat; ?></td>
                    <td><?php echo $row->nama_wilayah; ?></td>
                    <td class="text-right">
                        <small class='pull-left text-muted'>
                            <?php echo $row->mata_uang; ?>
                        </small> 
                        <?php echo number_format($row->nilai/1000000, 2); ?>
                    </td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url(). 'proyek-kategori-wilayah-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" 
                                        data-popup="tooltip" title="Edit" 
                                        data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger" 
                                        onclick="deleteData('<?php 
                                        echo base_url().'proyek-kategori-wilayah-delete/'.$row->id; ?>', '<?php echo base_url().'proyek-kategori-wilayah/'.$row->parent_id_proyek; ?>', 'kat_wilayah')" 
                                        data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){

        $('#reload_kat_wilayah').on('click', function() {
            loadData("<?php echo base_url().'proyek-kategori-wilayah/'.$parent_id_proyek; ?>", "kat_wilayah");
        });

        $('#add_kat_wilayah').click(function(){
            CallPage("<?php echo base_url().'proyek-kategori-wilayah-add/'.$parent_id_proyek; ?>", "tmpModal", "modalAdd");
        });

    });

</script>