<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6>
                    <span class="text-semibold">
                        Target - Realisasi Paket Kontrak
                    </span>
                </h6>
                <div class="heading-elements">
                    <div class="heading-btn">
                        <form class="heading-form" action="#">
                            <div class="form-group">
                                <select class="select form-control" 
                                id="pilih_paket">
                                    <option value="0">-- Pilih Paket Kontrak --</option>
                                    <?php 
                                        foreach($paket->result() AS $row):
                                            echo "<option value=".$row->parent_id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div style="margin: 5px">
                <div class="row" style="margin: 5px">
                    <div class="col-md-12">
                        <div class="panel panel-flat" id="paket_target">
                        
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-flat" id="paket_realisasi">
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        loadData("<?php echo base_url().'proyek-paket-target/0'; ?>", "paket_target");

        loadData("<?php echo base_url().'proyek-paket-realisasi/0'; ?>", "paket_realisasi");
        
        $('.select').select2();

        $('#pilih_paket').change(function(){
            //if ($(this).val() != 0){
                loadData("<?php echo base_url().'proyek-paket-target/'; ?>"+$(this).val(), "paket_target");
                loadData("<?php echo base_url().'proyek-paket-realisasi/'; ?>"+$(this).val(), "paket_realisasi");
                //loadData("<?php //echo base_url().'proyek-paket-masalah/'; ?>"+$(this).val(), "paket_masalah");
            //}
        });

    });

</script>