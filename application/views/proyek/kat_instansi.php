<div class="panel-heading">
    <h6 class="panel-title">
        <i class="icon-git-commit position-left"></i> 
            Kategori PHLN per Instansi Pelaksana
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
            id="reload_kat_instansi">
                <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_kat_instansi">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>
<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Kategori</th>
            <th>Instansi</th>
            <th>Nilai</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
                $parent_id_proyek = $row->parent_id_proyek;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->kat; ?></td>
                    <td><?php echo $row->instansi; ?></td>
                    <td class="text-right">
                        <small class='pull-left text-muted'>
                            <?php echo $row->mata_uang; ?>
                        </small> 
                        <?php echo number_format($row->nilai/1000000, 2); ?>
                    </td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().
                                        'proyek-kategori-instansi-edit/'.$row->id; ?>', 
                                        'tmpModal', 'modalEdit')" 
                                        data-popup="tooltip" title="Edit" 
                                        data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <?php 
                                            $link           = "'".base_url()."proyek-efektif-delete/".$row->parent_id_proyek."'";
                                        ?>
                                        <a href="#" class="text-danger" 
                                        onclick="deleteData('<?php 
                                        echo base_url().'proyek-kategori-instansi-delete/'.
                                        $row->id; ?>
                                        ', '<?php echo base_url().
                                        'proyek-kategori-instansi/'.$row->parent_id_proyek; ?>', 'kat_instansi')" 
                                        data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){

        $('#reload_kat_instansi').on('click', function() {
            loadData("<?php echo base_url().'proyek-kategori-instansi/'.$parent_id_proyek; ?>", "kat_instansi");
        });

        $('#add_kat_instansi').click(function(){
            CallPage("<?php echo base_url().'proyek-kategori-instansi-add/'.$parent_id_proyek; ?>", "tmpModal", "modalAdd");
        });

    });

</script>