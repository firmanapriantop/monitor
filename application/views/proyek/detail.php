<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat" id="profil">
        
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat" id="kat">
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-flat" id="kat_instansi">
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-flat" id="kat_wilayah">
                </div>
            </div>
        </div>
    </div>

</div>

<div class="panel panel-flat" id="realisasi">
    
</div>

<div class="panel panel-flat" id="paket">

</div>

<div class="panel panel-flat" id="paket_target_realisasi">

</div>



<div class="panel panel-flat" id="masalah">
    
</div>

<style>
    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){
        showLoading();

        loadData("<?php echo base_url().'proyek-profil/'.$parent_id_proyek; ?>", "profil");

        loadData("<?php echo base_url().'proyek-kategori/'.$parent_id_proyek; ?>", "kat");

        loadData("<?php echo base_url().'proyek-kategori-instansi/'.$parent_id_proyek; ?>", "kat_instansi");

        loadData("<?php echo base_url().'proyek-kategori-wilayah/'.$parent_id_proyek; ?>", "kat_wilayah");

        loadData("<?php echo base_url().'proyek-paket/'.$parent_id_proyek; ?>", "paket");

        loadData("<?php echo base_url().'proyek-paket-target-realisasi/'.$parent_id_proyek; ?>", "paket_target_realisasi");

        loadData("<?php echo base_url().'proyek-masalah/'.$parent_id_proyek; ?>", "masalah");
        
        //loadData("<?php //echo base_url().'proyek-realisasi/'.$parent_id_proyek; ?>", "realisasi");

        
        

        $('.table-togglable').footable();

        $('[data-popup="lightbox"]').fancybox({
            padding: 3
        });

        

        // ========================================
        //
        // Heading elements
        //
        // ========================================


        // Heading elements toggler
        // -------------------------

        // Add control button toggler to page and panel headers if have heading elements
        $('.panel-footer').has('> .heading-elements:not(.not-collapsible)').prepend('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');
        $('.page-title, .panel-title').parent().has('> .heading-elements:not(.not-collapsible)').children('.page-title, .panel-title').append('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');


        // Toggle visible state of heading elements
        $('.page-title .heading-elements-toggle, .panel-title .heading-elements-toggle').on('click', function() {
            $(this).parent().parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });
        $('.panel-footer .heading-elements-toggle').on('click', function() {
            $(this).parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });

        // Breadcrumb elements toggler
        // -------------------------

        // Add control button toggler to breadcrumbs if has elements
        $('.breadcrumb-line').has('.breadcrumb-elements').prepend('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>');


        // Toggle visible state of breadcrumb elements
        $('.breadcrumb-elements-toggle').on('click', function() {
            $(this).parent().children('.breadcrumb-elements').toggleClass('visible-elements');
        });

        $('.select').select2();
    /*
        $('#tabel').DataTable({
            "paging":   false,
            //"ordering": false,
            "info":     false,
            "searching": false
        });

        $('#tabel_2').DataTable({
            "paging":   false,
            //"ordering": false,
            "info":     false,
            "searching": false
        });
        
      */  
        $('#btnTambah').click(function(){
            CallPage("<?php echo base_url(); ?>proyek-efektif-add", "tmpModal", "modalAdd");
        });
        
        $('[data-popup="tooltip"]').tooltip();



    });
</script>

