
<table class="table  table-hover" id="tabel">
 <!--<table class="table datatable-responsive-column-controlled table-bordered table-striped table-hover" id="tabel"> -->
	<thead>
		<tr>
			<th>Kode</th>
			<th>Proyek</th>
			<th>Instansi</th>
			<th>Donor</th>			
			<th>Tanggal <br> Tutup</th>
			<th>Pinjaman</th>
			<th>Status</th>
			<th class="text-center">Actions</th>
		</tr>
	</thead>
	<tbody >

		<?php foreach ($proyek->result() AS $row) { ?>
			<tr>
				<td><?php echo $row->kode; ?></td>
				<td><a href="#" onclick="loadData('<?php echo base_url(); ?>proyek/proyek_detail/<?php echo $row->id; ?>', 'detail')"><?php echo $row->nama; ?></a><!-- <span style=" text-overflow: ellipsis; break-word: break-word ; display: block;  overflow: hidden"></span>--></td>
				<td><?php echo $row->instansi_singkatan; ?></td>
				<td><?php echo $row->mitra_singkatan; ?></td>	
				<td>22 Jun 1972</td>
				<td>32.0</td>
				<td><span class="label label-success">Active</span></td>	
				<td class="text-center">
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
								<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
								<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
							</ul>
						</li>
					</ul>
				</td>
			</tr>
		<?php } ?>
	</tbody>

</table>


<script>
	
	$(document).ready(function(){
		/*$('#tabel').DataTable({
			"responsive": true,
			"dom": 'T<"clear">lfrtip'
			
		});
		*/
		showLoading();

		$('#tabel').DataTable({
			"responsive": true,
			"dom": 'T<"clear">lfrtip',
			language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
			
		});

		$('.dataTables_filter input[type=search]').attr('placeholder','Cari di sini...');
		$('.dataTables_length select').select2({
			minimumResultsForSearch: "-1"
		});
	});
</script> 
