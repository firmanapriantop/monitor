<div class="panel-heading">
    <h6>
        <span class="text-semibold">
            Target
        </span>
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
                id="reload_paket_target">
                    <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_paket_target">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Triwulan</th>
            <th>Nilai</th>
            <th>PPN</th>
            <th>KEU</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td>
                            <?php echo $row->tw; ?>
                    </td>
                    <td class="text-right">
                        <?php 
                            $nilai_kurs = $row->nilai_kurs;
                            $nilai_asli = $row->nilai;
                            $nilai = $nilai_kurs * $nilai_asli;
                            if($row->mata_uang <> "USD"){
                                echo "<small class='pull-left text-muted'> ".$row->mata_uang."</small> ".number_format($row->nilai/1000000, 2); 
                                echo "<br />";
                            }
                            echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->nilai/1000000, 2);
                        ?>
                    </td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().'proyek-paket-target-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" data-popup="tooltip" title="Edit" data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger" 
                                        onclick="deleteData('<?php echo base_url().'proyek-paket-target-delete/'.$row->id; ?>', '<?php echo base_url().'proyek-paket-target/'.$row->parent_id_paket; ?>', 'paket_target')" 
                                        data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        $('#reload_paket_target').on('click', function() {
            loadData("<?php echo base_url().'proyek-paket-target/'; ?>"+$("#pilih_paket").val(), "paket_target");
        });

        $('#add_paket_target').click(function(){
            if ($("#pilih_paket").val() != 0){
                CallPage("<?php echo base_url().'proyek-paket-target-add/'.$parent_id_paket; ?>", "tmpModal", "modalAdd");
            }
            
        });

        $('.select').select2();

    });

</script>