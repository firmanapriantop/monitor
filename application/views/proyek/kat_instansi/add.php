<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlFormKatInstansi" 
            method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="id_kat">
                                    Kategori
                                </label>
                                <select class="select" data-placeholder="Pilih kategori" 
                                id="id_kat" name="id_kat" required>
                                    <option></option>
                                    <?php 
                                        foreach($kat->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="instansi">
                                    Instansi Pelaksana
                                </label>
                                <input type="text" name="instansi" 
                                id="instansi" class="form-control" 
                                placeholder="Masukkan instansi pelaksana" 
                                required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nilai">
                                    Nilai Alokasi
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <?php echo $mata_uang; ?>
                                    </span>
                                    <input type="text" name="nilai" id="nilai" 
                                    class="form-control" 
                                    placeholder="Masukkan nilai alokasi" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="ket">
                                    Keterangan
                                </label>
                                <textarea rows="5" cols="5" name="ket" 
                                id="ket" class="form-control" 
                                placeholder="Masukkan keterangan"></textarea> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" 
                        data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">
                        Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<script>

    $(document).ready(function(){

        $('.select').select2();

        $('#htmlFormKatInstansi').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id_kat = $("#id_kat").val();
            var instansi = $("#instansi").val();
            var nilai = $("#nilai").val();
            var ket = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id_kat', id_kat);
            form_data.append('instansi', instansi);
            form_data.append('nilai', nilai);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-kategori-instansi-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
                        loadData("<?php echo base_url().'proyek-kategori-instansi/'.$parent_id_proyek; ?>", "kat_instansi");
						notif("Informasi", "Data berhasil disimpan.");
                        
					} else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
