<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" role="dialog" 
aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" 
                data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Edit Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" 
            method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label" for="nama">
                            Triwulan
                        </label>
                        <select class="select" 
                        data-placeholder="Pilih triwulan" 
                        id="id_triwulan" name="id_triwulan" required>
                            <option value="<?php echo $data->id_triwulan; ?>"><?php echo $data->tw; ?></option>
                            <?php 
                                foreach($triwulan->result() AS $row):
                                    echo "<option value=".$row->id.">".$row->nama."</option>";
                                endforeach;
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="uang">
                            Nilai
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <?php echo $data->mata_uang; ?>
                            </span>
                            <input type="text" name="uang" id="uang" 
                            class="form-control" 
                            placeholder="Masukkan realisasi keuangan" 
                            value="<?php echo $data->uang; ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="fisik">
                            Progres Fisik
                        </label>
                        <div class="input-group">
                            <input type="text" name="fisik" id="fisik" 
                            class="form-control" 
                            placeholder="Masukkan progres fisik" 
                            value="<?php echo $data->fisik; ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="ket">
                            Keterangan
                        </label>
                        <div class="form-group">
                            <textarea rows="5" cols="5" name="ket" 
                            id="ket" class="form-control" 
                            placeholder="Masukkan keterangan"><?php echo $data->ket; ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id"  
                    class="form-control" value="<?php echo $data->id; ?>">
                    <input type="hidden" name="parent_id_paket" id="parent_id_paket"  
                    class="form-control" value="<?php echo $data->parent_id_paket; ?>">
                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){

        $('.select').select2();

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id = $("#id").val();
            var parent_id_paket = $("#parent_id_paket").val();
            var id_triwulan = $("#id_triwulan").val();
            var uang = $("#uang").val();
            var fisik = $("#fisik").val();
            var ket = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('parent_id_paket', parent_id_paket);
            form_data.append('id_triwulan', id_triwulan);
            form_data.append('uang', uang);
            form_data.append('fisik', fisik);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-paket-realisasi-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
                        loadData("<?php echo base_url().'proyek-paket-realisasi/'.$parent_id_paket; ?>","paket_realisasi");
						notif("Informasi", "Data berhasil disimpan.");
                        
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
