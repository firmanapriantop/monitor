<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog" 
aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" 
                data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" 
            method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label" for="id_triwulan_mulai">
                                Triwulan Mulai
                            </label>
                            <select class="select" 
                            data-placeholder="Pilih triwulan" 
                            id="id_triwulan_mulai" name="id_triwulan_mulai" required>
                                <option></option>
                                <?php 
                                    foreach($triwulan->result() AS $row):
                                        echo "<option value=".$row->id.">".$row->nama."</option>";
                                    endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label" for="id_triwulan_selesai">
                                Triwulan Selesai
                            </label>
                            <select class="select" 
                            data-placeholder="Pilih triwulan" 
                            id="id_triwulan_selesai" name="id_triwulan_selesai">
                                <option></option>
                                <?php 
                                    foreach($triwulan->result() AS $row):
                                        echo "<option value=".$row->id.">".$row->nama."</option>";
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="id_masalah_kat">
                            Kategori Masalah
                        </label>
                        <select class="select" 
                        data-placeholder="Pilih kategori" 
                        id="id_masalah_kat" name="id_masalah_kat" required>
                            <option></option>
                            <?php 
                                foreach($kat_masalah->result() AS $row):
                                    echo "<option value=".$row->id.">".$row->nama."</option>";
                                endforeach;
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="masalah">
                            Masalah
                        </label>
                        <textarea rows="5" cols="5" name="masalah" id="masalah" class="form-control" placeholder="Masukkan masalah" required></textarea>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="tindak_lanjut">
                            Tindak Lanjut
                        </label>
                        <textarea rows="5" cols="5" name="tindak_lanjut" id="tindak_lanjut" class="form-control" placeholder="Masukkan tindak lanjut" required></textarea>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="progres">
                            Progres Penyelesaian Masalah
                        </label>
                        <input type="text" name="progres" id="progres" class="form-control" placeholder="Masukkan jumlah progres" required>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="ket">
                            Keterangan
                        </label>
                        <div class="form-group">
                            <textarea rows="5" cols="5" name="ket" 
                            id="ket" class="form-control" 
                            placeholder="Masukkan keterangan"></textarea>
                        </div>
                    </div>

                    <input type="text" name="parent_id_proyek" id="parent_id_proyek"  
                    class="form-control" value="<?php echo $parent_id_proyek; ?>">
                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){

        $('.select').select2();

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var parent_id_proyek = $("#parent_id_proyek").val();
            var id_triwulan_mulai = $("#id_triwulan_mulai").val();
            var id_triwulan_selesai = $("#id_triwulan_selesai").val();
            var id_masalah_kat = $("#id_masalah_kat").val();
            var masalah = $("#masalah").val();
            var tindak_lanjut = $("#tindak_lanjut").val();
            var progres = $("#progres").val();
            var ket = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('parent_id_proyek', parent_id_proyek);
            form_data.append('id_triwulan_mulai', id_triwulan_mulai);
            form_data.append('id_triwulan_selesai', id_triwulan_selesai);
            form_data.append('id_masalah_kat', id_masalah_kat);
            form_data.append('masalah', masalah);
            form_data.append('tindak_lanjut', tindak_lanjut);
            form_data.append('progres', progres);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-masalah-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
                        loadData("<?php echo base_url().'proyek-masalah/'.$parent_id_proyek; ?>", "masalah");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
