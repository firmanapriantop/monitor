<div class="panel-heading">
    <h6>
        <span class="text-semibold">
            Realisasi
        </span>
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
                id="reload_paket_realisasi">
                    <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_paket_realisasi">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Triwulan</th>
            <th>Uang</th>
            <th>Fisik</th>
            <th>PPN</th>
            <th>KEU</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->tw; ?></td>
                    <td class="text-right">
                        <small class='pull-left text-muted'>
                            <?php echo $row->mata_uang; ?>
                        </small> 
                        <?php echo number_format($row->uang/1000000, 2); ?>
                    </td>
                    <td>
                        <?php echo number_format($row->fisik, 2); ?>
                    </td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td class="text-center"><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().'proyek-paket-realisasi-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" data-popup="tooltip" title="Edit" data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger" 
                                        onclick="deleteData('<?php echo base_url().'proyek-paket-realisasi-delete/'.$row->id; ?>', '<?php echo base_url().'proyek-paket-target/'.$row->parent_id_paket; ?>', 'paket_realisasi')" 
                                        data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        $('#reload_paket_realisasi').on('click', function() {
            loadData("<?php echo base_url().'proyek-paket-realisasi/'; ?>"+$("#pilih_paket").val(), "paket_realisasi");
        });

        $('#add_paket_realisasi').click(function(){
            if ($("#pilih_paket").val() != 0){
                CallPage("<?php echo base_url().'proyek-paket-realisasi-add/'.$parent_id_paket; ?>", "tmpModal", "modalAdd");
            }
            
        });

        $('.select').select2();

    });

</script>