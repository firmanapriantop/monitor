<div class="panel-heading">
    <h6>
        <span class="text-semibold">
            Realisasi Penyerapan
        </span>
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
            id="reload_realisasi">
                <b><i class="icon-loop position-left"></i></b>
            </button>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Triwulan</th>
            <th>Target</th>
            <th>Penyerapan</th>
            <th>Nilai</th>
            <th>Penyerapan<br>Kumulatif</th>
            <th>%</th>
            <th>Sisa<br>Pinjaman</th>
            <th>Waktu<br>Terpakai</th>
            <th>PV<br>PPN</th>
            <th>PV<br>KEU</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
                //$parent_id_proyek = $row->parent_id_proyek;
        ?>
                <tr>
                    <td>#</td>
                    <td><?php echo $row->tw; ?></td>
                    <td><?php echo number_format($row->target_nilai, 2); ?></td>
                    <td><?php echo number_format($row->uang, 2); ?></td>
                    <td><?php echo number_format($row->nilai, 2); ?></td>
                    <td><?php echo number_format($row->kum, 2); ?></td>
                    <td>%</td>
                    <td>Sisa<br>Pinjaman</td>
                    <td>Waktu<br>Terpakai</td>
                    <td>PV<br>PPN</td>
                    <td>PV<br>KEU</td>
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        $('#reload_realisasi').on('click', function() {
            loadData("<?php echo base_url().'proyek-realisasi/'.$parent_id_proyek; ?>", "realisasi");
        });

        $('.select').select2();

    });

</script>