<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" role="dialog" 
aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" 
                data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" 
            method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nama">
                                    Nama
                                </label>
                                <input type="text" name="nama" 
                                id="nama" class="form-control" 
                                placeholder="Masukkan nama kontrak" 
                                value="<?php echo $data->nama; ?>" 
                                required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="no">
                                    Nomor
                                </label>
                                <input type="text" name="no" 
                                id="no" class="form-control" 
                                placeholder="Masukkan nomor kontrak" 
                                value="<?php echo $data->no; ?>" 
                                required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl">
                                    Tanggal Kontrak
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl" id="tgl" 
                                    class="form-control daterange-single" 
                                    value="<?php echo $data->tgl; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_setuju_peminjam">
                                    Tanggal Persetujuan Peminjam
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_setuju_peminjam" 
                                    id="tgl_setuju_peminjam" 
                                    class="form-control daterange-single" 
                                    value="<?php echo $data->tgl_setuju_peminjam; ?>" 
                                    required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="periode">
                                    Periode
                                </label>
                                <input type="text" name="periode" 
                                id="periode" class="form-control" 
                                placeholder="Masukkan periode kontrak" 
                                value="<?php echo $data->periode; ?>" required>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_akhir">
                                    Tanggal Akhir Kontrak
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_akhir" 
                                    id="tgl_akhir" 
                                    class="form-control daterange-single" 
                                    value="<?php echo $data->tgl_akhir; ?>" 
                                    required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="kontraktor">
                                    Kontraktor
                                </label>
                                <input type="text" name="kontraktor" 
                                id="kontraktor" class="form-control" 
                                placeholder="Masukkan nama kontraktor" 
                                value="<?php echo $data->kontraktor; ?>" required>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="nilai">
                                    Nilai
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <?php echo $data->mata_uang; ?>
                                    </span>
                                    <input type="text" name="nilai" id="nilai" 
                                    class="form-control" 
                                    placeholder="Masukkan nilai kontrak" 
                                    value="<?php echo $data->nilai; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="status_terakhir">
                                    Status Terakhir
                                </label>
                                <textarea rows="5" cols="5" name="status_terakhir" 
                                id="status_terakhir" class="form-control" 
                                placeholder="Masukkan status terakhir">
                                <?php echo $data->status_terakhir; ?>
                                </textarea> 
                            </div>
                        </div>
                    </div>

                    <input type="text" name="id" id="id"
                    class="form-control" value="<?php echo $data->id; ?>">

                    <input type="text" name="id_proyek" id="id_proyek"  
                    class="form-control" value="<?php echo $data->id_proyek; ?>">
                    <input type="text" name="kode" id="kode" class="form-control"
                    value="<?php echo $data->kode; ?>">
                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){

        $('.select').select2();

        $('.daterange-single').daterangepicker({ 
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id = $("#id").val();
            var id_proyek = $("#id_proyek").val();
            var no = $("#no").val();
            var nama = $("#nama").val();
            var tgl = $("#tgl").val();
            var tgl_setuju_peminjam = $("#tgl_setuju_peminjam").val();
            var periode = $("#periode").val();
            var tgl_akhir = $("#tgl_akhir").val();
            var kontraktor = $("#kontraktor").val();
            var nilai = $("#nilai").val();
            var status_terakhir = $("#status_terakhir").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('id_proyek', id_proyek);
            form_data.append('no', no);
            form_data.append('nama', nama);
            form_data.append('tgl', tgl);
            form_data.append('tgl_setuju_peminjam', tgl_setuju_peminjam);
            form_data.append('periode', periode);
            form_data.append('tgl_akhir', tgl_akhir);
            form_data.append('kontraktor', kontraktor);
            form_data.append('nilai', nilai);
            form_data.append('status_terakhir', status_terakhir);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-paket-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
                        loadData("<?php echo base_url().'proyek-paket/'.$id_proyek; ?>",
                        "paket");
						notif("Informasi", "Data berhasil disimpan.");
                        
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
