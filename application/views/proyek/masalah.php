<div class="panel-heading">
    <h6>
        <span class="text-semibold">
            Masalah & Tindak Lanjut
        </span>
    </h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <button type="button" class="btn btn-default btn-xs" 
                id="reload_masalah">
                    <b><i class="icon-loop position-left"></i></b>
            </button>
            <button type="button" 
            class="btn btn-default btn-xs" 
            id="add_masalah">
                <b><i class="icon-plus-circle2 position-left"></i></b>
            </button>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Mulai</th>
            <th>Selesai</th>
            <th>Kategori</th>
            <th>Permasalahan</th>
            <th>Tindak Lanjut</th>
            <th>Progress</th>
            <th>PPN</th>
            <th>KEU</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=0;
            foreach($data->result() AS $row):
                $no++;
        ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->tw_mulai; ?></td>
                    <td><?php echo $row->tw_selesai; ?></td>
                    <td><?php echo $row->kat_masalah; ?></td>
                    <td><?php echo $row->masalah; ?></td>
                    <td><?php echo $row->tindak_lanjut; ?></td>
                    <td><?php echo number_format($row->progres, 2); ?>%</td>
                    <td><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td><i class="icon-checkbox-checked2 position-left text-success" data-popup="tooltip" data-placement="bottom"></i></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" 
                                data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#" onclick="CallPage('<?php echo base_url().'proyek-masalah-edit/'.$row->id; ?>', 'tmpModal', 'modalEdit')" data-popup="tooltip" title="Edit" data-placement="bottom">
                                            <i class="icon-pencil7"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger" 
                                        onclick="deleteData('<?php echo base_url().'proyek-masalah-delete/'.$row->id; ?>', '<?php echo base_url().'proyek-masalah/'.$row->parent_id_proyek; ?>', 'masalah')" 
                                        data-popup="tooltip" title="Delete" data-placement="bottom">
                                            <i class="icon-trash"></i> 
                                            <span class="text-danger"><b>Hapus</b></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        
        $('#reload_masalah').on('click', function() {
            loadData("<?php echo base_url().'proyek-masalah/'.$parent_id_proyek; ?>", "masalah");
        });

        $('#add_masalah').click(function(){
            CallPage("<?php echo base_url().'proyek-masalah-add/'.$parent_id_proyek; ?>", "tmpModal", "modalAdd");
            
        });

        $('.select').select2();

    });

</script>