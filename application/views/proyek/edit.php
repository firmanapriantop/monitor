<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" 
role="dialog" aria-hidden="true" data-backdrop="static" 
data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" 
                class="close" 
                data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Edit Data</h6>
            </div>

            <form class="form-horizontal" 
            action="#" id="htmlForm" 
            method="post" 
            enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label" for="jenis">Jenis Proyek</label>
                                <select class="select" 
                                data-placeholder="Pilih jenis proyek" 
                                id="jenis" name="jenis" required>
                                    <option value="<?php echo $data->jenis; ?>">
                                        <?php echo $data->jenis_proyek ?>
                                    </option>
                                    <option value="pinjaman">Pinjaman Luar Negeri</option>
                                    <option value="hibah">Hibah Luar Negeri</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="id_sektor">Sektor/Bidang</label>
                                <select class="select" 
                                data-placeholder="Pilih sektor/bidang" 
                                id="id_sektor" name="id_sektor" required>
                                    <option value="<?php echo $data->id_sektor ?>">
                                        <?php echo $data->sektor; ?>
                                    </option>
                                    <?php 
                                        foreach($sektor->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="id_mitra">Mitra Pembangunan</label>
                                <select class="select"
                                data-placeholder="Pilih mitra pembangunan" 
                                id="id_mitra" name="id_mitra" required>
                                    <option value="<?php echo $data->id_mitra ?>">
                                        <?php echo $data->mitra; ?>
                                    </option>
                                    <?php 
                                        foreach($mitra->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="id_instansi">
                                    Instansi Penanggung Jawab
                                </label>
                                <select class="select" 
                                data-placeholder="Pilih instansi penanggung jawab" 
                                id="id_instansi" name="id_instansi" required>
                                    <option value="<?php echo $data->id_instansi; ?>">
                                        <?php  echo $data->instansi ?>
                                    </option>
                                    <?php 
                                        foreach($instansi->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="kode">
                                    Kode Pinjaman/Hibah
                                </label>
                                <input type="text" name="kode" 
                                id="kode" class="form-control" 
                                placeholder="Masukkan kode pinjaman/hibah"
                                value="<?php echo $data->kode; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" 
                                for="no_npphln">No. NPPHLN</label>
                                <input type="text" name="no_npphln" 
                                id="no_npphln" class="form-control" 
                                placeholder="Masukkan Nomor NPPHLN"
                                value="<?php echo $data->no_npphln; ?>" required>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" 
                                for="no_register">No. Register</label>
                                <input type="text" name="no_register" 
                                id="no_register" class="form-control" 
                                placeholder="Masukkan Nomor Register"
                                value="<?php echo $data->no_register; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nama_en">
                                    Judul Proyek (EN)
                                </label>
                                <input type="text" name="nama_en" 
                                id="nama_en" class="form-control" 
                                placeholder="Masukkan judul proyek dalam bahasa Inggris (EN)"
                                value="<?php echo $data->nama_en; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nama_id">
                                    Judul Proyek (ID)
                                </label>
                                <input type="text" name="nama_id" 
                                id="nama_id" class="form-control" 
                                placeholder="Masukkan judul proyek dalam bahasa Indonesia (ID)"
                                value="<?php echo $data->nama_id; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label" for="tgl_ttd">
                                    Tanggal Penandatanganan NPPHLN
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_ttd" 
                                    id="tgl_ttd"  class="form-control daterange-single"
                                    value="<?php echo $data->tgl_ttd; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="tgl_efektif_tentatif">
                                    Tanggal Efektif Tentatif
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_efektif_tentatif" 
                                    id="tgl_efektif_tentatif"  class="form-control daterange-single"
                                    value="<?php echo $data->tgl_efektif_tentatif; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="tgl_efektif_riil">
                                    Tanggal Efektif Riil
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_efektif_riil" 
                                    id="tgl_efektif_riil"  class="form-control daterange-single"
                                    value="<?php echo $data->tgl_efektif_riil; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_tutup_original">
                                    Tanggal Penutupan NPPHLN (Original)
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_tutup_original" 
                                    id="tgl_tutup_original"  class="form-control daterange-single"
                                    value="<?php echo $data->tgl_tutup_original; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_tutup_actual">
                                    Tanggal Penutupan NPPHLN (Actual)
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar22"></i>
                                    </span>
                                    <input type="text" name="tgl_tutup_actual" 
                                    id="tgl_tutup_actual"  class="form-control daterange-single"
                                    value="<?php echo $data->tgl_tutup_actual; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="id_mata_uang">
                                    Mata Uang
                                </label>
                                <select class="select" id="id_mata_uang" 
                                name="id_mata_uang" data-placeholder="Pilih mata uang">
                                    <option value="<?php echo $data->id_mata_uang; ?>">
                                        <?php echo $data->mata_uang; ?>
                                    </option>
                                    <?php 
                                        foreach($mata_uang->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="nilai">
                                    Nilai
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-cash2"></i>
                                    </span>
                                    <input type="text" name="nilai" id="nilai" class="form-control" 
                                    placeholder="Masukkan nilai pinjaman atau hibah luar negeri"
                                    value="<?php echo number_format($data->nilai, 2); ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="instansi_pelaksana">
                                    Instansi Pelaksana
                                </label>
                                <textarea rows="5" cols="5" name="instansi_pelaksana" 
                                id="instansi_pelaksana" class="form-control" 
                                placeholder="Masukkan instansi pelaksana" 
                                required><?php echo $data->instansi_pelaksana; ?>
                                </textarea> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="ruang_lingkup">
                                    Ruang Lingkup
                                </label>
                                <textarea rows="5" cols="5" name="ruang_lingkup" 
                                id="ruang_lingkup" class="form-control" 
                                placeholder="Masukkan ruang lingkup" 
                                required><?php echo $data->ruang_lingkup; ?>
                                </textarea> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="tujuan">
                                    Tujuan
                                </label>
                                <textarea rows="5" cols="5" name="tujuan" 
                                id="tujuan" class="form-control" 
                                placeholder="Masukkan tujuan" 
                                required><?php echo $data->tujuan; ?></textarea> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="sasaran">
                                    Sasaran
                                </label>
                                <textarea rows="5" cols="5" name="sasaran" 
                                id="sasaran" class="form-control" 
                                placeholder="Masukkan sasaran" 
                                required><?php echo $data->sasaran; ?>
                                </textarea> 
                            </div>
                        </div>
                    </div>
                    <input type="text" name="id" id="id" class="form-control" value="<?php echo $data->id; ?>">
                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>
    $(document).ready(function(){

        $('.select').select2();

        $('.daterange-single').daterangepicker({ 
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id                      = $("#id").val();
            var jenis                   = $("#jenis").val();
            var id_instansi             = $("#id_instansi").val();
            var id_mitra                = $("#id_mitra").val();
            var id_sektor               = $("#id_sektor ").val();
            var kode                    = $("#kode").val();
            var no_npphln               = $("#no_npphln").val();
            var no_register             = $("#no_register").val();
            var nama_en                 = $("#nama_en").val();
            var nama_id                 = $("#nama_id").val();
            var tgl_ttd                 = $("#tgl_ttd").val();
            var tgl_efektif_tentatif    = $("#tgl_efektif_tentatif").val();
            var tgl_efektif_riil        = $("#tgl_efektif_riil").val();
            var tgl_tutup_original      = $("#tgl_tutup_original").val();
            var tgl_tutup_actual        = $("#tgl_tutup_actual").val();
            var id_mata_uang            = $("#id_mata_uang").val();
            var nilai                   = $("#nilai").val();
            var instansi_pelaksana      = $("#instansi_pelaksana").val();
            var ruang_lingkup           = $("#ruang_lingkup").val();
            var tujuan                  = $("#tujuan").val();
            var sasaran                 = $("#sasaran").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('jenis', jenis);
            form_data.append('id_instansi', id_instansi);
            form_data.append('id_mitra', id_mitra);
            form_data.append('id_sektor', id_sektor);
            form_data.append('kode', kode);
            form_data.append('no_npphln', no_npphln);
            form_data.append('no_register', no_register);
            form_data.append('nama_en', nama_en);
            form_data.append('nama_id', nama_id);
            form_data.append('tgl_ttd', tgl_ttd);
            form_data.append('tgl_efektif_tentatif', tgl_efektif_tentatif);
            form_data.append('tgl_efektif_riil', tgl_efektif_riil);
            form_data.append('tgl_tutup_original', tgl_tutup_original);
            form_data.append('tgl_tutup_actual', tgl_tutup_actual);
            form_data.append('id_mata_uang', id_mata_uang);
            form_data.append('nilai', nilai);
            form_data.append('instansi_pelaksana', instansi_pelaksana);
            form_data.append('ruang_lingkup', ruang_lingkup);
            form_data.append('tujuan', tujuan);
            form_data.append('sasaran', sasaran);
    
            $.ajax({
                url: '<?php echo base_url(); ?>proyek-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
						loadData("<?php echo base_url(); ?>proyek-load/"+<?php echo $id_tw; ?>, "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
        
	});
</script>
