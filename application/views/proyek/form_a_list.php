<h6 class="content-group text-semibold">Formulir A : Umum</h6>
<div class="table-responsive">
    <table class="table table-xs">
        <tbody>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Instansi Penanggung Jawab</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Nama Pemberi PHLN</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Nama Proyek Pinjaman/Hibah</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Nomor/Kode Pinjaman/Hibah</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tanggal Penandatanganan NPPHLN</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tanggal Efektif NPPHLN (Tentatif)</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tanggal Efektif NPPHLN (Riil)</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tanggal Penutupan NPPHLN (Original)</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tanggal Penutupan NPPHLN (Aktual/Revisi)</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Tujuan</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Sasaran</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Ruang Lingkup</span>
                </td>
                <td style="width: 70%">
                    For muted text color use <code>.text-muted</code> class
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <span class="text-regular">Instansi Pelaksana</span>
                </td>
                <td style="width: 70%">
                <table class="table  table-hover" id="tabel">
                    <thead>
                        <tr>
                            <th>Instansi Pelaksana</th>
                            <th>Kategori</th>
                            <th>Instansi</th>
                            <th>Alokasi Dana</th>	
                            <th>Revisi</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody >
                </td>
            </tr>
        </tbody>
    </table>
</div>