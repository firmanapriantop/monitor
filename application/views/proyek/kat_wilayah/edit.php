<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Edit Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" 
            method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="id_kat">
                            Kategori
                        </label>
                        <select class="select" data-placeholder="Pilih kategori" 
                        id="id_kat" name="id_kat" required>
                            <option value="<?php echo $data->id_kat ?>">
                                <?php echo $data->kat; ?>
                            </option>
                            <?php 
                                foreach($kat->result() AS $row):
                                    echo "<option value=".$row->id.">".$row->nama."</option>";
                                endforeach;
                            ?>
                        </select> 
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="id_wilayah">
                            Lokasi
                        </label>
                        <select class="select" data-placeholder="Pilih lokasi" 
                        id="id_wilayah" name="id_wilayah" required>
                        <option value="<?php echo $data->id_wilayah ?>">
                                <?php echo $data->wilayah; ?>
                            </option>
                            <?php 
                                foreach($wilayah->result() AS $row):
                                    echo "<option value=".$row->id.">".$row->nama."</option>";
                                endforeach;
                            ?>
                        </select> 
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="nilai">
                            Nilai Alokasi
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <?php echo $data->mata_uang; ?>
                            </span>
                            <input type="text" name="nilai" id="nilai" 
                            class="form-control" 
                            placeholder="Masukkan nilai alokasi" 
                            value="<?php echo $data->nilai; ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="ket">
                            Keterangan
                        </label>
                        <textarea rows="5" cols="5" name="ket" 
                        id="ket" class="form-control" 
                        placeholder="Masukkan keterangan"></textarea>
                    </div>

                    <input type="text" class="form-control" name="id" id="id"
                    value="<?php echo $data->id; ?>">

                    <div class="form-group text-right">
                        <hr>
                        <button type="button" class="btn btn-danger btn-xs" 
                        data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">
                        Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<script>

    $(document).ready(function(){

        $('.select').select2();

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id = $("#id").val();
            var id_kat = $("#id_kat").val();
            var id_wilayah = $("#id_wilayah").val();
            var nilai = $("#nilai").val();
            var ket = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('id_kat', id_kat);
            form_data.append('id_wilayah', id_wilayah);
            form_data.append('nilai', nilai);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-kategori-wilayah-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
                        loadData("<?php echo base_url().'proyek-kategori-wilayah/'.$parent_id_proyek; ?>", "kat_wilayah");
						notif("Informasi", "Data berhasil disimpan.");
                        
					} else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
