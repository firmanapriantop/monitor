<div class="panel-heading">
    <span class="label label-info"><?php echo $proyek->kode; ?></span> <span class="label label-info"><?php echo $proyek->mitra; ?></span><h6 class="panel-title"><span class="text-semibold"><?php echo $proyek->nama_en; ?></span></h6>
    <small><i><?php echo $proyek->nama_id; ?></i></small>
    <div class="heading-elements">
        <span class="label label-success heading-text">ON-GOING</span>
    </div>
</div>

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td style="width: 20%;">Masa Pakai</td>
                <td class="text-bold"><?php echo date("j F Y", strtotime($proyek->tgl_mulai)).' - '.date("j F Y", strtotime($proyek->tgl_selesai)); ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Instansi Penanggungjawab</td>
                <td class="text-bold"><?php echo $proyek->instansi; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Instansi Pelaksana</td>
                <td class="text-bold"><?php echo $proyek->instansi_pelaksana; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Sektor</td>
                <td class="text-bold"><?php echo $proyek->sektor; ?></td>
            </tr>
            <tr>
                <td style="width: 20%;">Ruang Lingkup</td>
                <td class="text-bold"><?php echo $proyek->ruang_lingkup; ?></td>
            </tr>
        </tbody>
    </table>
</div>