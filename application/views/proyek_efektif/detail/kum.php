<div class="panel-heading">
    <h6><span class="text-semibold">Realisasi Penarikan</span></h6>
    <div class="heading-elements">
        <div class="heading-btn">
            <?php 
                if ($this->session->userdata('level') == "operator" || 
                    $this->session->userdata('level') == "admin"
                    ){
            ?>
                <button type="button" class="btn btn-success btn-xs" id="btnTambah">
                    <i class="icon-plus-circle2 position-left"></i> Tambah Data
                </button>
            <?php } ?>
        </div>
    </div>
</div>

<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Triwulan</th>
            <th>Masa Laku</th>
            <th>Nilai <br /> Kurs</th>
            <th>Nilai <br />Pinjaman</th>
            <th>Penarikan <br /> Kumulatif</th>
            <th>%</th>
            <th>Pinjaman <br /> Belum Ditarik</th>
            <th>PV <br />PPN</th>
            <th>PV <br />KEU</th>
            <th>Target</th>
            <th>Realisasi</th>
            <th>%</th>
            <th>Realisasi <br /> Triwulan</th>
            <th>Status</th>
            <th>Bila - Multi</th>
            <th>Sisdur</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($realisasi->result() AS $row):
        ?>
        <tr>
            <td class="text-center"></td>
            <td class="text-left"><?php echo $row->triwulan; ?></td>
            <td class="text-right"><?php echo number_format($row->waktu_terpakai, 2); ?></td>
            <td class="text-right"> <!-- Nilai Kurs -->
                <small class='pull-left text-muted'>USD</small> <?php echo number_format($row->nilai_kurs, 2); ?>
            </td>
            <td class="text-right">
                <?php 
                    if($row->nama_mata_uang <> "USD"){
                        echo "<small class='pull-left text-muted'>".$row->nama_mata_uang."</small> ".number_format($row->nilai_pinjaman_ASLI/1000000, 2); 
                        echo "<br />";
                    }
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->nilai_pinjaman_USD/1000000, 2); 
                ?>
            </td>
            
            <td class="text-right"> <!-- Nilai Pinjaman -->
                <?php 
                    if($row->nama_mata_uang <> "USD"){
                        echo "<small class='pull-left text-muted'>".$row->nama_mata_uang."</small> ".number_format($row->realisasi_kum_ASLI/1000000, 2); 
                        echo "<br />";
                    }
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->realisasi_kum_USD/1000000, 2); 
                ?>
            </td>
            <td class="text-right"><?php echo number_format($row->persen_kum, 2); ?></td>
            <td class="text-right">
                <?php   
                    if($row->nama_mata_uang <> "USD"){
                        echo "<small class='pull-left text-muted'>".$row->nama_mata_uang."</small> ".number_format($row->sisa_pinjaman_ASLI/1000000, 2); 
                        echo "<br />";
                    }
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->sisa_pinjaman_USD/1000000, 2); 
                ?>
            </td>
            <td class="text-center">
                <?php 
                    if($row->pv <= -30){
                        echo '<span class="label label-danger">'.number_format($row->pv, 2).'</span>';
                    }else{
                        echo '<span class="label label-success">'.number_format($row->pv, 2).'</span>';
                    }
                    //echo number_format($row->pv, 2); 
                ?>
            </td>
            <td class="text-center"> <!-- PV Kemenkeu -->
                <?php 
                /*
                    if($row->pv <= -30){
                        echo '<span class="label label-danger">'.number_format($row->pv, 2).'</span>';
                    }else{
                        echo '<span class="label label-success">'.number_format($row->pv, 2).'</span>';
                    }
                    */
                    //echo number_format($row->pv, 2); 
                ?>
            </td>
            <td class="text-right">
                <?php 
                    echo "<small class='pull-left text-muted'>Rp</small> ".number_format($row->nilai_target_rp/1000000, 2); 
                    echo "<br />";
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->nilai_target_USD/1000000, 2); 
                ?>
            </td>
            <td class="text-right">
                <?php 
                    echo "<small class='pull-left text-muted'>".$row->nama_mata_uang."</small> ".number_format($row->realisasi_thn_ASLI/1000000, 2); 
                    echo "<br />";
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->realisasi_thn_USD/1000000, 2); 
                ?>
            </td>
            <td class="text-right"><?php echo number_format($row->persen_realisasi_thn, 2); ?></td>
            <td class="text-right">
                <?php 
                    echo "<small class='pull-left text-muted'>".$row->nama_mata_uang."</small> ".number_format($row->realisasi_tw_ASLI/1000000, 2); 
                    echo "<br />";
                    echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->realisasi_tw_USD/1000000, 2); 
                ?>
            </td>
            <td class="text-center"> <!-- satatus -->
                <?php 
                
                    if($row->status == "pending-add"){
                        echo '<span class="label label-warning">'.$row->status.'</span></td>';
                    }elseif($row->status == "ready"){
                        echo '<span class="label label-success">'.$row->status.'</span></td>';
                    }elseif($row->status == "submit-add"){
                        echo '<span class="label label-danger">'.$row->status.'</span></td>';
                    }else{
                        echo '<span class="label label-info">'.$row->status.'</span></td>';
                    }
                    
                ?>
            </td>
            <td class="text-center"> <!-- verifikasi bila-multi -->
                <?php 
                
                    //$link       	= "'".base_url()."realisasi-verify/".$row->id_proyek."/".$tahun_tw."/".$id_tw."/verifybilamulti'";
                    $link       	= "'".base_url()."realisasi-verify/".$row->id_proyek."/verifybilamulti'";
                    $tmpModal       = "'tmpModal'";
                    $modalVerify	= "'modalVerify'";

                    if($row->verify_bila_multi == "0"){
                        $verify_class = "icon-checkbox-unchecked text-grey position-left";
                    }elseif($row->verify_bila_multi == "1"){
                        $verify_class = "icon-cancel-square2 position-left text-danger";
                    }elseif($row->verify_bila_multi == "2"){
                        $verify_class = "icon-checkbox-checked2 position-left text-success";
                    }

                    if (($row->status == "submit-add" OR $row->status == "being-verified" OR $row->status == "recom-to-del" OR $row->status == "submit-edit") AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "bila-multi")){
                        echo '<a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalVerify.')" ><i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_bila_multi.'" data-placement="bottom"></i></a>';
                    }else{
                        echo '<i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_bila_multi.'" data-placement="bottom"></i>';
                    }
                    

                ?>
            </td>
            <td class="text-center"> <!-- verifikasi sisdur -->
                <?php 
                
                    //$link       	= "'".base_url()."proyek-efektif-verify/".$row->id_proyek."/".$tahun_tw."/".$id_tw."/verifysisdur'";
                    $link = "";
                    $tmpModal       = "'tmpModal'";
                    $modalVerify	= "'modalVerify'";

                    if($row->verify_sisdur == "0"){
                        $verify_class = "icon-checkbox-unchecked text-grey position-left";
                    }elseif($row->verify_sisdur == "1"){
                        $verify_class = "icon-cancel-square2 position-left text-danger";
                    }elseif($row->verify_sisdur == "2"){
                        $verify_class = "icon-checkbox-checked2 position-left text-success";
                    }

                    if (($row->status == "submit-add" OR $row->status == "being-verified" OR $row->status == "recom-to-del" OR $row->status == "submit-edit") AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "sisdur")){

                        echo '<a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalVerify.')" ><i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_sisdur.'" data-placement="bottom"></i></a>';
                    }else{
                        echo '<i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_sisdur.'" data-placement="bottom"></i>';
                    }
                    
                ?>
            </td>
            <td class="text-center">
                <?php 
                    if($row->tw_aktif == "1"){
                ?>
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <?php 
                            
                                if($row->status == "pending-add"){
                                    
                                    echo '<li><a href="#" onclick="submit_add('.$row->id_proyek.','.$id_tw.')"><i class="icon-upload"></i> Submit </a></li>';
                                    //$link       = "'proyek-efektif-edit/".$row->id_proyek."/".$tahun_tw."/".$id_tw."'";
                                    $link = "";
                                    $tmpModal   = "'tmpModal'";
                                    $modalEdit  = "'modalEdit'";
                                    echo '<li><a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalEdit.')" data-popup="tooltip" title="Edit" data-placement="bottom"><i class="icon-pencil7"></i> Edit</a></li>';

                                    $link           = "'".base_url()."proyek-efektif-delete/".$row->id_proyek."'";
                                    $refresh_link   = ""; //"'".base_url()."proyek-efektif-load/".$tahun_tw."/".$id_tw."'";
                                    $nama_div       = "'data'";
                                    echo '<li><a href="#" onclick="deleteData('.$link.', '.$refresh_link.', '.$nama_div.')" data-popup="tooltip" title="Delete" data-placement="bottom"><i class="icon-trash"></i> <span class="text-danger">Hapus</span></a></li>';
                                
                                }elseif($row->status == "ready"){

                                    echo '<li><a href="#" onclick="request_to_edit('.$row->id_proyek.','.$id_tw.', 1)" data-popup="tooltip" title="" data-placement="bottom"><i class="icon-pencil7"></i>Request to Edit</a></li>';

                                }elseif($row->status == "edit" AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "operator")){
                                    
                                    echo '<li><a href="#" onclick="submit_data('.$row->id_proyek.', '.$id_tw.', 2)" data-popup="tooltip" title="" data-placement="bottom"><i class="icon-pencil7"></i>Submit</a></li>';
                                    
                                    $link       = "'proyek-efektif-edit/".$row->id_proyek."/".$id_tw."'";
                                    $tmpModal   = "'tmpModal'";
                                    $modalEdit  = "'modalEdit'";
                                    echo '<li><a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalEdit.')" data-popup="tooltip" title="Edit" data-placement="bottom"><i class="icon-pencil7"></i> Edit</a></li>';

                                }elseif ($row->status == "req-edit" AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "sisdur" OR $this->session->userdata('level') == "bila-multi")){
                                    
                                    echo '<li><a href="#" onclick="request_to_edit('.$row->id_proyek.', '.$id_tw.', 2)" data-popup="tooltip" title="" data-placement="bottom"><i class="icon-pencil7"></i>Approve to Edit</a></li>';

                                }elseif($row->status == "edit" AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "operator")){
                                    
                                    echo '<li><a href="#" onclick="submit_data('.$row->id_proyek.', '.$id_tw.', 2)" data-popup="tooltip" title="" data-placement="bottom"><i class="icon-pencil7"></i>Submit</a></li>';
                                    
                                    $link       = "'proyek-efektif-edit/".$row->id_proyek."/".$id_tw."'";
                                    $tmpModal   = "'tmpModal'";
                                    $modalEdit  = "'modalEdit'";
                                    echo '<li><a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalEdit.')" data-popup="tooltip" title="Edit" data-placement="bottom"><i class="icon-pencil7"></i> Edit</a></li>';

                                }
                                
                            ?>
                            <li><div class="hr"></div></li>
                            <li><a href="#"><i class="icon-table2"></i> Log</a></li>
                        </ul>
                    </li>
                </ul>
                <?php 
                    }else{
                        echo '<i class="icon-lock"></i>';
                    }
                ?>
            </td>
        </tr>
            <?php endforeach; ?>
    </tbody>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    function request_to_edit(id_proyek, id_tw, kat){
        if(kat == "1"){
            var kategori = "request-to-edit";
        }else if (kat == "2"){
            var kategori = "approve-to-edit";
        }
        swal({
            title: "Do you want to request to edit this data?",
            text: "-",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url(); ?>proyek-efektif-realisasi-edit/"+id_proyek+"/"+id_tw+"/"+kategori,
                        success:function(response){
                            swal("Data has been saved", {
                                icon: "success",
                            });
                            //loadData("<?php echo base_url(); ?>proyek-efektif-load/<?php //echo $tahun_tw; ?>/<?php //echo $id_tw; ?>", "data");
                            loadData("<?php echo base_url().'proyek-efektif-kumulatif/'.$id_proyek.'/'.$id_tw; ?>", "kumulatif");
                        },
                        error: function(){
                            alert('Error Updating!');
                        },
                        dataType:"html"
                    });

                    
                    
                }
            });
    }

    function submit_data(id_proyek, id_tw, kat){
        if(kat == "1"){
            var kategori = "submit-add";
        }else if (kat == "2"){
            var kategori = "submit-edit";
        }
        swal({
            title: "Do you want to submit this data?",
            text: "-",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url(); ?>proyek-efektif-realisasi-submit/"+id_proyek+"/"+id_tw+"/"+kategori,
                        success:function(response){
                            swal("Data has been saved", {
                                icon: "success",
                            });
                            //loadData("<?php echo base_url(); ?>proyek-efektif-load/<?php //echo $tahun_tw; ?>/<?php //echo $id_tw; ?>", "data");
                            loadData("<?php echo base_url().'proyek-efektif-kumulatif/'.$id_proyek.'/'.$id_tw; ?>", "kumulatif");
                        },
                        error: function(){
                            alert('Error Updating!');
                        },
                        dataType:"html"
                    });

                    
                    
                }
            });
    }

    $(document).ready(function(){
        showLoading();

        $('.select').select2();

        $('#tabel').DataTable();
        
        $('[data-popup="tooltip"]').tooltip();

        $('#btnTambah').click(function(){
            CallPage("<?php echo base_url(); ?>proyek-efektif-realisasi-add/<?php echo $id_proyek; ?>/<?php echo $id_tw; ?>", "tmpModal", "modalAdd");
        });

    });

</script>