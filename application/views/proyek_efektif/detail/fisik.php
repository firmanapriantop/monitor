<div class="panel-heading">
    <h6 class="panel-title">
        <i class="icon-git-commit position-left"></i> 
            Kemajuan Proyek / Progres Fisik
    </h6>
    <div class="heading-elements">
        <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
            <li><a data-action="reload" id="reload_fisik"></a></li>
            <li><a data-action="close"></a></li>
            <li>
                <form class="heading-form" action="#">
                    <div class="form-group">
                        <select class="select form-control">
                            <option>2018</option>
                        </select>
                    </div>
                </form>
            </li>
        </ul><!--
        <form class="heading-form" action="#">
            <div class="form-group">
                <select class="select form-control">
                    <option>2018</option>
                </select>
            </div>
        </form>-->
    </div>
</div>
<table class="table datatable-responsive">
    <thead>
        <tr>
            <th>#asdds</th>
            <th>Triwulan</th>
            <th>Masa Laku</th>
            <th>Nilai <br /> Kurs</th>
            <th>Nilai <br />Pinjaman</th>
            <th>Penarikan <br /> Kumulatif</th>
            <th>%</th>
            <th>Pinjaman <br /> Belum Ditarik</th>
            <th>PV <br />PPN</th>
            <th>PV <br />KEU</th>
            <th>Target</th>
            <th>Realisasi</th>
            <th>%</th>
            <th>Realisasi <br /> Triwulan</th>
            <th>Status</th>
            <th>Bila - Multi</th>
            <th>Sisdur</th>
            <th>Aksi</th>
        </tr>
    </thead>
</table>

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        

        

        $('#reload_fisik').on('click', function() {
            //showLoading2("fisik");
            loadData("<?php echo base_url().'proyek-efektif-fisik'; ?>", "fisik");
        });

    });

</script>