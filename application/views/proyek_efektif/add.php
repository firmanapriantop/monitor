<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="jenis">Jenis Proyek</label>
                                <select class="select" data-placeholder="Pilih jenis proyek" id="jenis" name="jenis">
                                    <option></option>
                                    <option value="pinjaman">Pinjaman Luar Negeri</option>
                                    <option value="hibah">Hibah Luar Negeri</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="id_mitra">Mitra Pembangunan</label>
                                <select class="select" data-placeholder="Pilih mitra pembangunan" id="id_mitra" name="id_mitra">
                                    <option></option>
                                    <?php 
                                        foreach($mitra->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="id_instansi">Instansi Penanggung Jawab</label>
                                <select class="select" data-placeholder="Pilih instansi penanggung jawab" id="id_instansi" name="id_instansi">
                                    <option></option>
                                    <?php 
                                        foreach($instansi->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="id_sektor">Sektor/Bidang</label>
                                <select class="select" data-placeholder="Pilih sektor/bidang" id="id_sektor" name="id_sektor">
                                    <option></option>
                                    <?php 
                                        foreach($sektor->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="kode">Kode Proyek</label>
                                <input type="text" name="kode" id="kode" class="form-control" placeholder="Masukkan kode proyek">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="nama_en">Judul Proyek (EN)</label>
                                <input type="text" name="nama_en" id="nama_en" class="form-control" placeholder="Masukkan judul proyek dalam bahasa Inggris (EN)">
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="nama_id">Judul Proyek (ID)</label>
                                <input type="text" name="nama_id" id="nama_id" class="form-control" placeholder="Masukkan judul proyek dalam bahasa Indonesia (ID)">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_mulai">Tanggal Mulai</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                    <input type="text" name="tgl_mulai" id="tgl_mulai"  class="form-control daterange-single" placeholder="Masukkan tanggal mulai">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="tgl_selesai">Tanggal Selesai</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                    <input type="text" name="tgl_selesai" id="tgl_selesai"  class="form-control daterange-single" placeholder="Masukkan tanggal selesai">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="id_mata_uang">Mata Uang</label>
                                <select class="select" id="id_mata_uang" name="id_mata_uang" data-placeholder="Pilih mata uang">
                                    <option></option>
                                    <?php 
                                        foreach($mata_uang->result() AS $row):
                                            echo "<option value=".$row->id.">".$row->nama."</option>";
                                        endforeach;
                                    ?>
                                </select> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="nilai_pinjaman">Nilai Pinjaman</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-cash2"></i></span>
                                    <input type="text" name="nilai_pinjaman" id="nilai_pinjaman"  class="form-control" placeholder="Masukkan nilai pinjaman">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for="instansi_pelaksana">Instansi Pelaksana</label>
                                <textarea rows="5" cols="5" name="instansi_pelaksana" id="instansi_pelaksana" class="form-control" placeholder="Masukkan instansi pelaksana"></textarea> 
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="ruang_lingkup">Ruang Lingkup</label>
                                <textarea rows="5" cols="5" name="ruang_lingkup" id="ruang_lingkup" class="form-control" placeholder="Masukkan ruang lingkup"></textarea> 
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>
    $(document).ready(function(){

        $('.select').select2();

        $('.daterange-single').daterangepicker({ 
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var jenis               = $("#jenis").val();
            var id_instansi         = $("#id_instansi").val();
            var id_mitra            = $("#id_mitra").val();
            var id_sektor           = $("#id_sektor ").val();
            var kode                = $("#kode").val();
            var nama_en             = $("#nama_en").val();
            var nama_id             = $("#nama_id").val();
            var tgl_mulai           = $("#tgl_mulai").val();
            var tgl_selesai         = $("#tgl_selesai").val();
            var id_mata_uang        = $("#id_mata_uang").val();
            var nilai_pinjaman      = $("#nilai_pinjaman").val();
            var instansi_pelaksana  = $("#instansi_pelaksana").val();
            var ruang_lingkup       = $("#ruang_lingkup").val();
			
            var form_data 	= new FormData();
            
            form_data.append('jenis', jenis);
            form_data.append('id_instansi', id_instansi);
            form_data.append('id_mitra', id_mitra);
            form_data.append('id_sektor', id_sektor);
            form_data.append('kode', kode);
            form_data.append('nama_en', nama_en);
            form_data.append('nama_id', nama_id);
            form_data.append('tgl_mulai', tgl_mulai);
            form_data.append('tgl_selesai', tgl_selesai);
            form_data.append('id_mata_uang', id_mata_uang);
            form_data.append('nilai_pinjaman', nilai_pinjaman);
            form_data.append('instansi_pelaksana', instansi_pelaksana);
            form_data.append('ruang_lingkup', ruang_lingkup);
    

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-efektif-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
						loadData("<?php echo base_url(); ?>proyek-efektif-load/"+<?php echo $id_tw; ?>, "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
        
	});
</script>
