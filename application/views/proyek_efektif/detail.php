<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat" id="profil">
        
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-flat" id="">
            
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-watch position-left"></i> Grafik</h6>
                <div class="heading-elements">
                    <form class="heading-form" action="#">
						<div class="form-group">
                            <select class="select form-control">
                                <option>2018</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div class="row">

    <div class="col-md-12">
        
    </div>

</div>
-->
<div class="panel panel-flat" id="realisasi">

</div>

<div class="panel panel-flat" id="fisik">
    
</div>




 <!--
        
   

<div class="panel panel-flat">
    <div class="panel-heading">
        <h6><i class="icon-brain position-left"></i> <span class="text-semibold">Permasalahan &amp; Tindak Lanjut</span></h6>
    </div>
    <table class="table" id="tabel">
        <thead>
            <tr>
                <th>Kategori</th>
                <th>Permasalahan</th>
                <th>Tindak <br />Lanjut</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody> 
        </tbody>
    </table>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h6><span class="text-semibold">Sebaran Lokasi</span></h6>
    </div>
</div>
 -->
<!-- Revisions -->

<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-git-commit position-left"></i> Documents</h6>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <ul class="media-list">
            <li class="media">
                <div class="media-left"><a href="#" class="btn border-primary text-primary btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-pull-request"></i></a></div>
                <div class="media-body">
                    Drop the IE <a href="#">specific hacks</a> for temporal inputs
                    <div class="media-annotation">4 minutes ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-warning text-warning btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-commit"></i></a></div>
                <div class="media-body">
                    Add full font overrides for popovers and tooltips
                    <div class="media-annotation">36 minutes ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-info text-info btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-branch"></i></a></div>
                <div class="media-body">
                    <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                    <div class="media-annotation">2 hours ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-success text-success btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-merge"></i></a></div>
                <div class="media-body">
                    <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                    <div class="media-annotation">Dec 18, 18:36</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-primary text-primary btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-pull-request"></i></a></div>
                <div class="media-body">
                    Have Carousel ignore keyboard events
                    <div class="media-annotation">Dec 12, 05:46</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /revisions -->

<!-- Kemajuan -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-git-commit position-left"></i> Kemajuan</h6>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <ul class="media-list">
            <li class="media">
                <div class="media-body">
                    Drop the IE <a href="#">specific hacks</a> for temporal inputs
                    <div class="media-annotation">4 minutes ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-warning text-warning btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-commit"></i></a></div>
                <div class="media-body">
                    Add full font overrides for popovers and tooltips
                    <div class="media-annotation">36 minutes ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-info text-info btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-branch"></i></a></div>
                <div class="media-body">
                    <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                    <div class="media-annotation">2 hours ago</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-success text-success btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-merge"></i></a></div>
                <div class="media-body">
                    <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                    <div class="media-annotation">Dec 18, 18:36</div>
                </div>
            </li>

            <li class="media">
                <div class="media-left"><a href="#" class="btn border-primary text-primary btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-pull-request"></i></a></div>
                <div class="media-body">
                    Have Carousel ignore keyboard events
                    <div class="media-annotation">Dec 12, 05:46</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /kemajuan -->

 <!-- foto -->
 <div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-git-commit position-left"></i> Foto</h6>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <div class="thumbnail">
            <div class="thumb">
                <img src="assets/images/demo/flat/3.png" alt="">
                <div class="caption-overflow">
                    <span>
                        <a href="assets/images/demo/flat/3.png" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                        <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /revisions -->


<!--<script src="<?php //echo base_url(); ?>assets/js/core/app.js"></script>
 <script src="<?php //echo base_url(); ?>assets/muds/muds.js"></script> -->

<style>
    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){
        showLoading();

        loadData("<?php echo base_url().'proyek-efektif-profil/'.$id_proyek.'/'.$id_triwulan; ?>", "profil");

        //loadData("<?php echo base_url().'proyek-efektif-kumulatif/'.$id_proyek.'/'.$id_triwulan; ?>", "kumulatif");

        loadData("<?php echo base_url().'proyek-efektif-realisasi/'.$id_proyek.'/'.$id_triwulan; ?>", "realisasi");

        //loadData("<?php echo base_url().'proyek-efektif-fisik/'.$id_proyek.'/'.$id_triwulan; ?>", "fisik");

        loadData("<?php echo base_url().'proyek-efektif-fisik'; ?>", "fisik");
        

        $('.table-togglable').footable();

        $('[data-popup="lightbox"]').fancybox({
            padding: 3
        });

        // ========================================
        //
        // Heading elements
        //
        // ========================================


        // Heading elements toggler
        // -------------------------

        // Add control button toggler to page and panel headers if have heading elements
        $('.panel-footer').has('> .heading-elements:not(.not-collapsible)').prepend('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');
        $('.page-title, .panel-title').parent().has('> .heading-elements:not(.not-collapsible)').children('.page-title, .panel-title').append('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');


        // Toggle visible state of heading elements
        $('.page-title .heading-elements-toggle, .panel-title .heading-elements-toggle').on('click', function() {
            $(this).parent().parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });
        $('.panel-footer .heading-elements-toggle').on('click', function() {
            $(this).parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });

        // Breadcrumb elements toggler
        // -------------------------

        // Add control button toggler to breadcrumbs if has elements
        $('.breadcrumb-line').has('.breadcrumb-elements').prepend('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>');


        // Toggle visible state of breadcrumb elements
        $('.breadcrumb-elements-toggle').on('click', function() {
            $(this).parent().children('.breadcrumb-elements').toggleClass('visible-elements');
        });

        $('.select').select2();
    /*
        $('#tabel').DataTable({
            "paging":   false,
            //"ordering": false,
            "info":     false,
            "searching": false
        });

        $('#tabel_2').DataTable({
            "paging":   false,
            //"ordering": false,
            "info":     false,
            "searching": false
        });
        
      */  
        $('#btnTambah').click(function(){
            CallPage("<?php echo base_url(); ?>proyek-efektif-add", "tmpModal", "modalAdd");
        });
        
        $('[data-popup="tooltip"]').tooltip();



    });
</script>

