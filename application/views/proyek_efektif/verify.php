<!-- Verify modal -->
<div class="modal inmodal fade" id="modalVerify" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title"><?php echo $judul; ?></h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php 
                                    if($verify == "0"){
                                        
                                        $no_action_checked = set_radio('verify', '0', TRUE);
                                        $reject_checked = "";
                                        $approve_checked = "";

                                    }elseif($verify == "1"){
                                        
                                        $reject_checked = set_radio('verify', '1', TRUE);
                                        $no_action_checked = "";
                                        $approve_checked = "";

                                    }elseif($verify == "2"){
                                        
                                        $approve_checked = set_radio('verify', '2', TRUE);
                                        $reject_checked = "";
                                        $no_action_checked = "";

                                    }else{

                                        $no_action_checked = "";
                                        $reject_checked = "";
                                        $approve_checked = "";

                                    }
                                ?>

                                <label class="control-label" for="verify">Hasil verifikasi</label>
                                <div class="radio">
                                    <label >
                                        <input type="radio" name="verify" class="control-custom" <?php echo $no_action_checked; ?> value="0" >
                                        Tidak ada aksi
                                    </label>
                                </div>
                                
                                <div class="radio">
                                    <label class="text-danger">
                                        <input type="radio" name="verify" class="control-danger" <?php echo $reject_checked; ?> value="1">
                                        Ditolak
                                    </label>
                                </div>

                                <div class="radio" >
                                    <label class="text-success">
                                        <input type="radio" name="verify" class="control-success" <?php echo $approve_checked; ?> value="2">
                                        Diterima
                                    </label>
                                </div>
                                <br>
                                <textarea rows="5" cols="5" name="cat" id="cat" class="form-control" placeholder="Catatan"><?php echo $cat; ?></textarea> 
                            </div>
                            
                        </div>
                    </div>
                    <input type="text" name="id" id="id"  class="form-control" value="<?php echo $id_proyek; ?>">
                    <input type="text" name="id_tw" id="id_tw"  class="form-control" value="<?php echo $id_tw; ?>">
                    <input type="text" name="verified_by" id="verified_by"  class="form-control" value="<?php echo $verified_by; ?>">
                    <input type="text" name="jenis" id="jenis"  class="form-control" value="<?php echo $jenis; ?>">
                </div>
                <hr>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>
    $(document).ready(function(){
        $(".control-custom").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-grey-600 text-grey-800'
        });

        // Primary
        $(".control-primary").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $(".control-danger").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $(".control-success").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Info
        $(".control-info").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-info-600 text-info-800'
        });

        $('#htmlForm').submit(function(e) {

            var verify_val = $('input:radio[name=verify]:checked').val();

			e.preventDefault();

			var me 			    = $(this);
            
            var id                  = $("#id").val();
            var verify              = verify_val;
            var cat                 = $("#cat").val();
            var id_tw               = $("#id_tw").val();
            var verified_by         = $("#verified_by").val();
            var jenis               = $("#jenis").val(); //usulan or realisasi
            var sub_jenis           = $("#sub_jenis").val(); //usulan or realisasi
			
            var form_data 	= new FormData();
            
            form_data.append('id', id);
            form_data.append('verify', verify);
            form_data.append('cat', cat);
            form_data.append('id_tw', id_tw);
            form_data.append('verified_by', verified_by);
            form_data.append('jenis', jenis);
            form_data.append('sub_jenis', sub_jenis);
    

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-efektif-verify-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalVerify').modal('hide');
						loadData("<?php echo $link_to_refresh; ?>", "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

    });

/*
    $(document).ready(function(){

        $('.select').select2();

        $('.daterange-single').daterangepicker({ 
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        
		
        
	});
    */
</script>

