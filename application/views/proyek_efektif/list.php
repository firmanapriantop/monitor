
<div class="row">
    <div class="col-md-12">
        <!-- Title with left icon -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">Data</h6>
                <div class="heading-elements">
                    <div class="heading-btn">
                        <button type="button" class="btn btn-success btn-xs" id="btnTambah"><i class="icon-plus-circle2 position-left"></i> Tambah Data</button>
                    </div>
                </div>
            </div>
            
            <div class="panel-body">
            </div>
                <table class="table datatable-responsive table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Proyek</th>
                            <th>Nilai <br> Pinjaman</th>
                            <th>Penarikan <br>Kumulatif</th>
                            <th>%</th>
                            <th>Waktu <br>Pakai</th>
                            <th>PV <br/> PPN</th>
                            <th>Target <br> <?php echo $tahun_tw; ?></th>
                            <th>Realisasi <br> <?php echo $tahun_tw; ?></th>
                            <!-- <th>%<br> <?php //echo $tahun_tw; ?></th> -->
                            <th>Status</th>
                            <th>Bila - Multi</th>
                            <th>Sisdur</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=0;
                            foreach($data->result() AS $row):
                                $no++;
                            
                        ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->mitra; ?> - <?php echo $row->kode; ?><br>
                                    <a hre="#" onclick="loadData('<?php echo base_url()."proyek-efektif-detail/".$row->id_proyek; ?>', 'data')"><?php echo $row->nama_en; ?></a> 
                                    <?php
                                        if ($row->jlh_ext <> "0"){
                                            echo '<span class="badge badge-primary">'.$row->jlh_ext.'</span>';
                                        }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if($row->mata_uang <> "USD"){
                                            echo "<small class='pull-left text-muted'> ".$row->mata_uang."</small> ".number_format($row->nilai_pinjaman_ASLI/1000000, 2); 
                                            echo "<br />";
                                        }
                                        echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->nilai_pinjaman_USD/1000000, 2);
                                        //echo $row->nilai_kurs;
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if($row->mata_uang <> "USD"){
                                            echo "<small class='pull-left text-muted'> ".$row->mata_uang."</small> ".number_format($row->realisasi_kum_ASLI/1000000, 2); 
                                            echo "<br />";
                                        }
                                        echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->realisasi_kum_USD/1000000, 2);
                                    ?>
                                </td>
                                <td class="text-right"><?php echo number_format($row->persen_kum, 2); ?></td>
                                <td class="text-right"><?php echo number_format($row->waktu_terpakai, 2); ?></td>
                                <td class="text-right">
                                    <?php 
                                        $persen_kum = $row->persen_kum; $waktu_terpakai = $row->waktu_terpakai;
                                        $pv = $persen_kum - $waktu_terpakai;

                                        if($pv <= -30){
                                            echo '<span class="label label-danger">'.number_format($pv, 2).'</span>';
                                        }else{
                                            echo '<span class="label label-success">'.number_format($pv, 2).'</span>';
                                        }

                                    ?>
                                <td class="text-right">
                                    <?php 
                                        echo "<small class='pull-left text-muted'>Rp </small> ".number_format($row->nilai_target_rp/1000000, 2); 
                                            echo "<br />";
                                        
                                        echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->nilai_target_USD/1000000, 2);
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if($row->mata_uang <> "USD"){
                                            echo "<small class='pull-left text-muted'> ".$row->mata_uang."</small> ".number_format($row->realisasi_thn_ASLI/1000000, 2); 
                                            echo "<br />";
                                        }
                                        echo "<small class='pull-left text-muted'>USD</small> ".number_format($row->realisasi_thn_USD/1000000, 2);
                                    ?>
                                </td>
                                <!-- 
                                <td class="text-right">
                                    <?php
                                    /*
                                        $realisasi_thn_USD = $row->realisasi_thn_USD;
                                        $target_USD = $row->nilai_target_USD;
                                        if($target_USD <> 0){
                                            echo number_format((($realisasi_thn_USD / $target_USD) * 100), 2);
                                        }else{
                                            echo number_format(0.00, 2);
                                        }
                                        */
                                    ?>
                                </td>
                                    -->
                                <td class="text-center"> <!-- satatus -->
                                    <?php 
                                        if ($row->status == "pending-add"){
                                            echo '<span class="label label-warning">'.$row->status.'</span>';
                                        }elseif($row->status == "ready"){
                                            echo '<span class="label label-success">'.$row->status.'</span>';
                                        }elseif($row->status == "submit-add"){
                                            echo '<span class="label label-danger">'.$row->status.'</span>';
                                        }else{
                                            echo '<span class="label label-info">'.$row->status.'</span>';
                                        }
                                    ?>
                                </td>
                                <td class="text-center"> <!-- verifikasi bila-multi -->
                                    <?php 
                                        if ($status = "submit-add"){
                                            $sub_jenis = "add";
                                        }elseif ($status = "submit-edit"){
                                            $sub_jenis = "edit";
                                        }elseif ($status = "submit-extend"){
                                            $sub_jenis = "extend";
                                        }

                                        $link       	= "'".base_url()."proyek-efektif-verify/".$row->id_proyek."/".$id_tw."/usulan/add/verify-bila-multi'";
                                        $tmpModal       = "'tmpModal'";
                                        $modalVerify	= "'modalVerify'";

                                        if($row->verify_bila_multi == "0"){
                                            $verify_class = "icon-checkbox-unchecked text-grey position-left";
                                        }elseif($row->verify_bila_multi == "1"){
                                            $verify_class = "icon-cancel-square2 position-left text-danger";
                                        }elseif($row->verify_bila_multi == "2"){
                                            $verify_class = "icon-checkbox-checked2 position-left text-success";
                                        }

                                        if (($row->status == "submit-add" OR $row->status == "being-verified" OR $row->status == "recom-to-del" OR $row->status == "submit-edit") AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "bila-multi")){

                                            echo '<a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalVerify.')" ><i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_bila_multi.'" data-placement="bottom"></i></a>';
                                        }else{
                                            echo '<i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_bila_multi.'" data-placement="bottom"></i>';
                                        }
                                    ?>
                                </td>
                                <td class="text-center"> <!-- verifikasi sisdur -->
                                    <?php 
                                        if ($status = "submit-add"){
                                            $sub_jenis = "add";
                                        }elseif ($status = "submit-edit"){
                                            $sub_jenis = "edit";
                                        }elseif ($status = "submit-extend"){
                                            $sub_jenis = "extend";
                                        }

                                        //$link       	= "'".base_url()."proyek-efektif-verify/".$row->id_proyek."/".$id_tw."/usulan/".$sub_jenis."/verify-sisdur'";
                                        $link       	= "'".base_url()."proyek-efektif-verify/".$row->id_proyek."/".$id_tw."/usulan/add/verify-sisdur'";
                                        $tmpModal       = "'tmpModal'";
                                        $modalVerify	= "'modalVerify'";

                                        if($row->verify_sisdur == "0"){
                                            $verify_class = "icon-checkbox-unchecked text-grey position-left";
                                        }elseif($row->verify_sisdur == "1"){
                                            $verify_class = "icon-cancel-square2 position-left text-danger";
                                        }elseif($row->verify_sisdur == "2"){
                                            $verify_class = "icon-checkbox-checked2 position-left text-success";
                                        }

                                        if (($row->status == "submit-add" OR $row->status == "being-verified" OR $row->status == "recom-to-del" OR $row->status == "submit-edit"OR $row->status == "submit-ext") AND ($this->session->userdata('level') == "admin" OR $this->session->userdata('level') == "sisdur")){

                                            echo '<a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalVerify.')" ><i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_sisdur.'" data-placement="bottom"></i></a>';
                                        }else{
                                            echo '<i class="'.$verify_class.'" data-popup="tooltip" title="'.$row->cat_sisdur.'" data-placement="bottom"></i>';
                                        }
                                    ?>
                                </td>
                                <td class="text-center"><!-- aksi -->
                                    <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
                                                    <?php 
                                                        if($row->status == "pending-add"){
                                                            $page ="'".base_url()."proyek-efektif-submit/".$row->id_proyek."/usulan/add'";
                                                            $refresh_link   = "'".base_url()."proyek-efektif-load/".$id_tw."'";
                                                            echo '<li><a href="#" onclick="submit_data('.$page.','.$refresh_link.')"><i class="icon-upload"></i> Submit </a></li>';

                                                            $link       = "'proyek-efektif-edit/".$row->id_proyek."/".$tahun_tw."/".$id_tw."'";
                                                            $tmpModal   = "'tmpModal'";
                                                            $modalEdit  = "'modalEdit'";
                                                            echo '<li><a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalEdit.')" data-popup="tooltip" title="Edit" data-placement="bottom"><i class="icon-pencil7"></i> Edit</a></li>';

                                                            $link           = "'".base_url()."proyek-efektif-delete/".$row->id_proyek."'";
                                                            $refresh_link   = "'".base_url()."proyek-efektif-load/".$id_tw."'";
                                                            $nama_div       = "'data'";
                                                            echo '<li><a href="#" onclick="deleteData('.$link.', '.$refresh_link.', '.$nama_div.')" data-popup="tooltip" title="Delete" data-placement="bottom"><i class="icon-trash"></i> <span class="text-danger">Hapus</span></a></li>';
                                                        
                                                        }elseif($row->status == "ready"){
                                                            $page ="'".base_url()."proyek-efektif-submit/".$row->id_proyek."/usulan/request-extend'";
                                                            $refresh_link   = "'".base_url()."proyek-efektif-load/".$id_tw."'";

                                                            echo '<li><a href="#" onclick="submit_data('.$page.','.$refresh_link.')" data-popup="tooltip" title="Perpanjangan Pinjaman/Hibah" data-placement="bottom"><i class="icon-aid-kit"></i>Request Extend</a></li>';

                                                        }elseif ($row->status == "recom-to-edit" || $row->status == "ext-edit"){

                                                            $page ="'".base_url()."proyek-efektif-submit/".$row->id_proyek."/usulan/edit'";
                                                            $refresh_link   = "'".base_url()."proyek-efektif-load/".$id_tw."'";
                                                            echo '<li><a href="#" onclick="submit_data('.$page.','.$refresh_link.')"><i class="icon-upload"></i> Submit </a></li>';
                                                            
                                                            $link       = "'proyek-efektif-edit/".$row->id_proyek."/".$tahun_tw."/".$id_tw."'";
                                                            $tmpModal   = "'tmpModal'";
                                                            $modalEdit  = "'modalEdit'";
                                                            echo '<li><a href="#" onclick="CallPage('.$link.', '.$tmpModal.', '.$modalEdit.')" data-popup="tooltip" title="Edit" data-placement="bottom"><i class="icon-pencil7"></i> Edit</a></li>';

                                                        }elseif($row->status == "req-ext"){

                                                            echo '<li><a href="#" onclick="permission_to_ext('.$row->id_proyek.','.$id_tw.')" data-popup="tooltip" title="" data-placement="bottom"><i class="icon-aid-kit"></i>Permission to Extend</a></li>';

                                                        }
                                                    ?>
                                                    <li><div class="hr"></div></li>
                                                    <li><a href="#"><i class="icon-table2"></i> Log</a></li>
												</ul>
											</li>
										</ul>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            
        </div>
        <!-- /title with left icon -->
    </div>
</div>
<!--
<script src="<?php //echo base_url(); ?>assets/js/core/app.js"></script>-->
<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    

    function request_to_ext(id_proyek, id_tw){
        swal({
            title: "Do you want to extend this project?",
            text: "-",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url(); ?>proyek-efektif-ext/"+id_proyek,
                        success:function(response){
                            swal("Data has been saved", {
                                icon: "success",
                            });
                            loadData("<?php echo base_url(); ?>proyek-efektif-load/<?php echo $id_tw; ?>", "data");
                        },
                        error: function(){
                            alert('Error Updating!');
                        },
                        dataType:"html"
                    });
                }
            });
    }

    function permission_to_ext(id_proyek, id_tw){
        swal({
            title: "Do you want to extend this project?",
            text: "-",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url(); ?>proyek-efektif-ext/"+id_proyek,
                        success:function(response){
                            swal("Data has been saved", {
                                icon: "success",
                            });
                            loadData("<?php echo base_url(); ?>proyek-efektif-load/<?php echo $tahun_tw; ?>/<?php echo $id_tw; ?>", "data");
                        },
                        error: function(){
                            alert('Error Updating!');
                        },
                        dataType:"html"
                    });
                }else{
                    $.ajax({
                        url: "<?php echo base_url(); ?>proyek-efektif-ext/"+id_proyek,
                        success:function(response){
                            swal("Data has been saved", {
                                icon: "success",
                            });
                            loadData("<?php echo base_url(); ?>proyek-efektif-load/<?php echo $tahun_tw; ?>/<?php echo $id_tw; ?>", "data");
                        },
                        error: function(){
                            alert('Error Updating!');
                        },
                        dataType:"html"
                    });
                }
            });
    }

    $(document).ready(function(){
        showLoading();

       // $('.select-border-color').select2();

        $('#btnTambah').click(function(){
            CallPage("<?php echo base_url(); ?>proyek-efektif-add/"+<?php echo $tahun_tw; ?>+"/"+<?php echo $id_tw; ?>, "tmpModal", "modalAdd");
        });
        
        $('[data-popup="tooltip"]').tooltip();

    });
</script>

