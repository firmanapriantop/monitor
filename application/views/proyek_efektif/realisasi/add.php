<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="tw">Triwulan</label>
                                <input type="text" name="tw" id="tw" class="form-control" disabled value="<?php echo $tw; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="nilai_kurs">Nilai Kurs </label>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="nilai_kurs" id="nilai_kurs" class="form-control" disabled value="<?php echo number_format($nilai_kurs, 2); ?>">
                                    <div class="form-control-feedback">
                                        <i class="icon-coin-dollar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="realisasi_uang">Realisasi Triwulan</label>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="realisasi_uang" id="realisasi_uang" class="form-control" >
                                    <div class="form-control-feedback">
                                        <?php echo $mata_uang; ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="realisasi_usd" id="realisasi_usd" class="form-control" readonly="readonly">
                                    <div class="form-control-feedback">USD
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label" for="ket">Keterangan</label>
                                <textarea rows="5" cols="5" name="ket" id="ket" class="form-control" placeholder="Masukkan keterangan"></textarea>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id_proyek" id="id_proyek"  class="form-control" value="<?php echo $id_proyek; ?>">
                    <input type="hidden" name="id_triwulan" id="id_triwulan"  class="form-control" value="<?php echo $id_triwulan; ?>">
                    <input type="hidden" name="id_nilai_kurs" id="id_nilai_kurs"  class="form-control" value="<?php echo $id_nilai_kurs; ?>">

                    <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->

<style>
    .daterangepicker{
        z-index: 10000 !important;
    }

    .project-meta{
        border:0;
        margin:-7px 0 0;
    }
    
    .panel-body .project-meta tr:last-child td{
        border-bottom:0;
        padding-bottom:0
    }
</style>

<script>

    $(document).ready(function(){

        $('#realisasi_uang').keyup(function(){
            var nilai_kurs      = parseFloat($("#nilai_kurs").val());
            var realisasi_uang  = parseFloat($("#realisasi_uang").val());
            var hasil = nilai_kurs * realisasi_uang;

            $("#realisasi_usd").val(addCommas(parseFloat(hasil.toFixed(2))));
        });

        $('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id_proyek           = $("#id_proyek").val();
            var id_triwulan         = $("#id_triwulan").val();
            var id_nilai_kurs       = $("#id_nilai_kurs").val();
            var realisasi_uang      = $("#realisasi_uang").val();
            var ket                 = $("#ket").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id_proyek', id_proyek);
            form_data.append('id_triwulan', id_triwulan);
            form_data.append('id_nilai_kurs', id_nilai_kurs);
            form_data.append('realisasi_uang', realisasi_uang);
            form_data.append('ket', ket);

            $.ajax({
                url: '<?php echo base_url(); ?>proyek-efektif-realisasi-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
                        loadData("<?php echo base_url().'proyek-efektif-realisasi/'.$id_proyek.'/'.$id_triwulan; ?>", "realisasi");
						notif("Informasi", "Data berhasil disimpan.");
                        
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        
	});
</script>
