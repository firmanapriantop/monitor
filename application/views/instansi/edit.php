<!-- Edit modal -->
<div class="modal inmodal fade" id="modalEdit" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Edit Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                
                    <fieldset class="content-group">
                        <div class="form-group"  for="level">
                            <label class="control-label" for="level">Tipe</label>
                            <select class="select" data-placeholder="Pilih tipe" id="level" name="level">
                                <option value="<?php echo $data->level; ?>"><?php echo $data->level; ?></option>
                                <option value="Kementerian/Lembaga">Kementerian/Lembaga</option>
                                <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                <option value="BUMN">BUMN</option>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="nama">Nama</label>
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Instansi" value="<?php echo $data->nama; ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="singkatan">Singkatan</label>
                            <input type="text" name="singkatan" id="singkatan" class="form-control" placeholder="Masukkan Singkatan dari Nama Instansi" value="<?php echo $data->singkatan; ?>">
                        </div>
                        <div class="form-group"  for="is_active">
                            <label class="control-label" for="is_active">Status</label>
                            <select class="select" data-placeholder="Pilih Status" id="is_active" name="is_active">
                                <option value="<?php echo $data->is_active; ?>"><?php echo $data->status; ?></option>
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            </select> 
                        </div>
                        <input type="hidden" name="id_parent" id="id_parent" value="0">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">
                    </fieldset>
                </div>
                <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->


<script>
    $(document).ready(function(){

         $('.select').select2();
        
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id_parent   = $("#id_parent").val();
            var level       = $("#level").val();
            var nama        = $("#nama").val();
            var singkatan   = $("#singkatan").val();
            var is_active   = $("#is_active").val();
            var id          = $("#id").val();
			
            var form_data 	= new FormData();
            
            form_data.append('id_parent', id_parent);
            form_data.append('level', level);
            form_data.append('nama', nama);
            form_data.append('singkatan', singkatan);
            form_data.append('is_active', is_active);
            form_data.append('id', id);
    

            $.ajax({
                url: '<?php echo base_url(); ?>instansi-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalEdit').modal('hide');
						loadData("<?php echo base_url(); ?>instansi-load", "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
        
	});
</script>
