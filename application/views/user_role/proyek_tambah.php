<div id="modalTambah" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Tambah Proyek</h5>
			</div>
			<form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">
			<div class="modal-body">
				
				<fieldset class="content-group">
					<!-- <legend class="text-bold">Form A</legend> -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="sumber_dana" class="col-sm-3 control-label">Sumber</label>
								<div class="col-md-12">
									<select id="sumber_dana" class="select-search" >
										<option value=""></option>
										<option value="pinjaman">Pinjaman Luar Negeri</option>
										<option value="hibah">Hibah Luar Negeri</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_mitra" class="col-sm-3 control-label">Mitra</label>
								<div class="col-md-12">
									<select id="id_mitra" name="id_mitra" class="select-search" >
										<option value=""></option>
										<?php foreach($mitra->result() as $row){ ?>
											<option value="<?php echo $row->id; ?>"><?php echo $row->nama." (".$row->singkatan.")"; ?></option>';
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_instansi" class="col-sm-3 control-label">Instansi</label>
								<div class="col-md-12">
									<select id="id_instansi" class="select-search" >
										<option value="">Pilih Instansi</option>
										<?php foreach($instansi->result() as $row){ ?>
											<option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>';
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_instansi_eselon_satu" class="col-sm-3 control-label">Unit Eselon I</label>
								<div class="col-md-12">
									<select id="id_instansi_eselon_satu" name="id_instansi_eselon_satu" class="select-search" >
										<option value=""></option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="kode" class="col-sm-3 control-label">Kode</label>
								<div class="col-md-12">
									<input type="text" name="kode" id="kode" class="form-control" placeholder="Kode Proyek">
								</div>
							</div>

							<div class="form-group">
								<label for="no_register" class="col-sm-3 control-label">No.Register </label>
								<div class="col-md-12">
									<input type="text" name="no_register" id="no_register" class="form-control" placeholder="Nomor Register">
								</div>
							</div>


							<div class="form-group">
								<label for="nama" class="col-sm-3 control-label">Nama </label>
								<div class="col-md-12">
									<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Proyek">
								</div>
							</div>

							<div class="form-group">
								<label for="id_mata_uang" class="col-sm-3 control-label">Mata Uang </label>
								<div class="col-md-12">
									<select id="id_mata_uang" name="id_mata_uang" class="select-search" >
										<option value=""></option>
										<?php foreach($mata_uang->result() as $row){ ?>
											<option value="<?php echo $row->id; ?>"><?php echo $row->nama." (".$row->singkatan.")"; ?></option>';
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="nilai_original" class="col-sm-3 control-label">Nilai (Original) </label>
								<div class="col-md-12">
									<input type="text" name="nilai_original" id="nilai_original" class="form-control" placeholder="Nilai Pinjaman Proyek (Original)">
								</div>
							</div>

							<div class="form-group">
								<label for="nilai_revisi" class="col-sm-3 control-label">Nilai (Revisi) </label>
								<div class="col-md-12">
									<input type="text" name="nilai_revisi" id="nilai_revisi" class="form-control" placeholder="Nilai Pinjaman Proyek (Revisi)">
								</div>
							</div>

						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="tgl_efektif_tentatif" class="col-sm-12 control-label">Tanggal Efektif NPPHLN (Tentatif) </label>
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span><input type="text" name="tgl_efektif_tentatif" id="tgl_efektif_tentatif" class="form-control daterange-single"  placeholder="Tanggal Penandatanganan NPPHLN" >
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="tgl_efektif_ril" class="col-sm-12 control-label">Tanggal Efektif NPPHLN (Riil) </label>
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span><input type="text" name="tgl_efektif_ril" id="tgl_efektif_ril" class="form-control" >
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="	" class="col-sm-12 control-label">Tanggal Penandatanganan NPPHLN </label>
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span><input type="text" name="tgl_ttd_npphln" id="tgl_ttd_npphln" class="form-control" placeholder="Tanggal Penandatanganan NPPHLN">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="tgl_tutup_original" class="col-sm-12 control-label">Tanggal Penutupan NPPHLN (Original)</label>
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span><input type="text" name="tgl_tutup_original" id="tgl_tutup_original" class="form-control" placeholder="Tanggal Penandatanganan NPPHLN">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="tgl_tutup_actual" class="col-sm-12 control-label">Tanggal Penutupan NPPHLN (Actual/Revisi)</label>
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span><input type="text" name="tgl_tutup_actual" id="tgl_tutup_actual" class="form-control" placeholder="Tanggal Penandatanganan NPPHLN">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="tujuan" class="col-sm-12 control-label">Tujuan</label>
								<div class="col-md-12">
									<textarea name="tujuan" id="tujuan" rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="sasaran" class="col-sm-12 control-label">Sasaran</label>
								<div class="col-md-12">
									<textarea name="sasaran" id="sasaran" rows="5" cols="5" class="form-control" placeholder="Sasaran Proyek"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="ruang_lingkup" class="col-sm-12 control-label">Ruang Lingkup</label>
								<div class="col-md-12">
									<textarea name="ruang_lingkup" id="ruang_lingkup" rows="5" cols="5" class="form-control" placeholder="Ruang Lingkup Proyek"></textarea>
								</div>
							</div>

						</div>
					</div>
				</fieldset>
			
			</div>
			<legend class="text-bold">&nbsp;</legend>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><b>Tutup</b></button>
				<button type="submit" class="btn btn-success"><i class="icon-floppy-disk position-left"></i> <b>Simpan</b></button>
			</div>
			</form>
		</div>
	</div>
</div>

<style>
	.daterangepicker{z-index:99999 !important;}
</style>

<script>
    $(document).ready(function(){
		$("#id_instansi").select2({
			placeholder: "Pilih Instansi",
			width: "100%"
		});

		$("#id_instansi_eselon_satu").select2({
			placeholder: "Pilih Instansi Eselon I",
			width: "100%"
		});

		$("#sumber_dana").select2({
			placeholder: "Pilih Sumber Pendanaan",
			width: "100%"
		});

		$("#id_mitra").select2({
			placeholder: "Pilih Mitra Pembangunan",
			width: "100%"
		});

		$("#sumber_dana").select2({
			placeholder: "Pilih Sumber Pendanaan",
			width: "100%"
		});
		
		/*
		$('#id_sumber').change(function () {
            var id_sumber = $(this).val();
            $.ajax({
                url: "<?php //echo base_url(); ?>proyek/ambil_data_mitra/",
                dataType: "json",
                success: function(data) {
                    $("#id_mitra").empty();
                    $("#id_mitra").append('<option value="">Pilih Mitra Pembangunan</option>');
                    $(data).each(function(){
                        var option = $('<option />');
                        option.attr('value', this.id).text(this.nama+" ("+this.singkatan+")");
                        $('#id_mitra').append(option);
                    });
                }
            })
		});
		*/

		$('#id_instansi').change(function () {
            var id_instansi = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>proyek/ambil_data_instansi_eselon_satu/"+id_instansi,
                dataType: "json",
                success: function(data) {
                    $("#id_instansi_eselon_satu").empty();
                    $("#id_instansi_eselon_satu").append('<option value="">Pilih Instansi Eselon I</option>');
                    $(data).each(function(){
                        var option = $('<option />');
                        option.attr('value', this.id).text(this.nama);
                        $('#id_instansi_eselon_satu').append(option);
                    });
                }
            })
		});
		
		$("#id_mata_uang").select2({
			placeholder: "Pilih Mata Uang",
			width: "100%"
		});


		$("#tgl_efektif_ril").pickadate({
			selectMonths: true,
			selectYears: true,
			format: 'yyyy-mm-dd',
			formatSubmit: 'yyyy-mm-dd'
		});

		$("#tgl_efektif_tentatif").pickadate({
			selectMonths: true,
			selectYears: true,
			format: 'yyyy-mm-dd',
			formatSubmit: 'yyyy-mm-dd'
		});

		$("#tgl_tutup_original").pickadate({
			selectMonths: true,
			selectYears: true,
			format: 'yyyy-mm-dd',
			formatSubmit: 'yyyy-mm-dd'
		});

		$("#tgl_tutup_actual").pickadate({
			selectMonths: true,
			selectYears: true,
			format: 'yyyy-mm-dd',
			formatSubmit: 'yyyy-mm-dd'
		});

		$("#tgl_ttd_npphln").pickadate({
			selectMonths: true,
			selectYears: true,
			format: 'yyyy-mm-dd',
			formatSubmit: 'yyyy-mm-dd'
		});
		
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			= $(this);

			var id_instansi_eselon_satu 	= $("#id_instansi_eselon_satu").val();
			var id_mitra 					= $("#id_mitra").val();
			var sumber_dana					= $("#sumber_dana").val();
			var kode 						= $("#kode").val();
			var no_register					= $("#no_register").val();
			var nama 						= $("#nama").val();
			var id_mata_uang 				= $("#id_mata_uangnama").val();
			var nilai_original				= $("#nilai_original").val();
			var nilai_revisi 				= $("#nilai_revisi").val();
			var tgl_ttd_npphln 				= $("#tgl_ttd_npphln").val();
			var tgl_efektif_tentatif 		= $("#tgl_efektif_tentatif").val();
			var tgl_efektif_ril			 	= $("#tgl_efektif_ril").val();
			var tgl_tutup_original 			= $("#tgl_tutup_original").val();
			var tgl_tutup_actual 			= $("#tgl_tutup_actual").val();
			var id_mata_uang	 			= $("#id_mata_uang").val();
			var nilai_original		 		= $("#nilai_original").val();
			var nilai_revisi		 		= $("#nilai_revisi").val();
			var tujuan		 				= $("#tujuan").val();
			var sasaran		 				= $("#sasaran").val();
			var ruang_lingkup		 		= $("#ruang_lingkup").val();
            
			
            var form_data 	= new FormData();
			
			form_data.append('id_instansi_eselon_satu', id_instansi_eselon_satu);
            form_data.append('id_mitra', id_mitra);
			form_data.append('sumber_dana', sumber_dana);
			form_data.append('kode', kode);
			form_data.append('no_register', no_register);
			form_data.append('nama', nama);
			form_data.append('id_mata_uang', id_mata_uang);
			form_data.append('nilai_original', nilai_original);
			form_data.append('nilai_revisi', nilai_revisi);
			form_data.append('tgl_ttd_npphln', tgl_ttd_npphln);
			form_data.append('tgl_efektif_tentatif', tgl_efektif_tentatif);
			form_data.append('tgl_efektif_ril', tgl_efektif_ril);
			form_data.append('tgl_tutup_original', tgl_tutup_original);
			form_data.append('tgl_tutup_actual', tgl_tutup_actual);
			form_data.append('no_register', no_register);
			form_data.append('id_mata_uang', id_mata_uang);
			form_data.append('nilai_original', nilai_original);
			form_data.append('nilai_revisi',nilai_revisi);
			form_data.append('tujuan', tujuan);
			form_data.append('sasaran', sasaran);
			form_data.append('ruang_lingkup', ruang_lingkup);


            $.ajax({
                url: '<?php echo base_url(); ?>proyek/simpan',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalTambah').modal('hide');
						loadData("<?php echo base_url(); ?>proyek/proyek_list", "list");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error has-feedback' : 'has-success')
							.find('.text-danger')
							.append("<div class='form-control-feedback'><i class='icon-cancel-circle2'></i></div>")
							.remove();
							
							element.after(value);
						});
					}
                }
            });
		});
		


	});
</script>

