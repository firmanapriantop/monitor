<!-- Page header -->
<div class="page-header">
	

	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2 position-left"></i> <span class="text-bold">User</span> Role</h4>
		</div>

		<div class="heading-elements col-md-6">
			<div class="form-group">
				<div class="col-sm-10">
				</div>
				<button class="btn btn-danger col-sm-2 text-bold" id="btnCari">Tampilkan</button>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="general_embeds.html">General pages</a></li>
			<li class="active">Responsive embeds</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content" >
	
	<!-- Form horizontal -->
	<div class="panel panel-flat tabel" id="detail">
		<div class="panel-heading">
			<h5 class="panel-title">Data</h5>
			<div class="heading-elements">
				<!-- 
				<button type="button" id="btnTambah" class="btn btn-primary"><i class=" icon-plus-circle2 position-left"></i> Tambah</button>
				<button type="button" id="btnTambah2" class="btn btn-primary"><i class=" icon-plus-circle2 position-left"></i> Tambah</button>
						-->
						<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
        	</div>
		</div>
		<div class="panel-body">
		<p class="content-group-lg">Extend form controls by adding text or buttons before, after, or on both sides of any text-based <code>&lt;input></code>. Use <code>.input-group</code> with an <code>.input-group-addon</code> to prepend or append elements to a single <code>.form-control</code>. Place one add-on or button on either side of an input. You may also place one on both sides of an input. Multiple add-ons on a single side and multiple form-controls in a single input group aren't supported.</p>
		<legend class="text-bold">&nbsp;</legend>
			<span  id="list">

			</span>
		</div>

	</div>

</div>
<!-- /Content area -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>


<script>
	
	$(document).ready(function(){

		var id_instansi = $('#id_instansi').val();

		$('.select-custom-colors').select2({
			containerCssClass: 'bg-danger',
			placeholder: "Pilih Instansi Penanggung Jawab Proyek"
		});
		
		$('#btnCari').on('click', function() {
			var id_instansi2 = $('#id_instansi').val();
			loadData("<?php echo base_url(); ?>proyek/proyek_list/"+id_instansi2, "list");
		});

		$('#example').DataTable({
			responsive: true,
			"dom": 'T<"clear">lfrtip',
			"order": [[ 0, "desc" ]]
		});
		
		loadData("<?php echo base_url(); ?>user_role/user_role_list", "list");
		
		$('#btnTambah2').on('click', function() {
			//var dark = $(this).parent();
			var dark = $(this).parent();
			$('.panel-flat').block({
				message: '<i class="icon-spinner spinner"></i>',
				overlayCSS: {
					backgroundColor: '#1B2024',
					opacity: 0.85,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'none',
					color: '#fff'
				}
			});
			window.setTimeout(function () {
				$('.panel-flat').unblock();
			}, 2000);
		});

		$('#btnTambah').click(function(){
			CallPage("<?php echo base_url(); ?>proyek/proyek_tambah", "tmpModal", "modalTambah");
			//notif("Informasi", "Data berhasil disimpan.");
			//showLoading();
		});

	});
</script> 

