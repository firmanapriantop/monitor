
<table class="table  table-hover" id="tabel">
 <!--<table class="table datatable-responsive-column-controlled table-bordered table-striped table-hover" id="tabel"> -->
	<thead>
		<tr>
			<th>User</th>
			<th>Role</th>
			<th class="text-center">Actions</th>
		</tr>
	</thead>
	<tbody >

		<?php foreach ($user_role->result() AS $row) { ?>
			<tr>
				<td><?php echo $row->nama_depan.' '.$row->nama_belakang; ?>
				<td>
					<?php 
						$rray = explode(',', $row->role);
						$count = count($rray);
						for ($i = 0; $i < $count; $i++) {
							echo '<span class="label label-primary">'.$rray[$i].'</span> <br />';
						}
						
					?><br>
				</td>
				<td class="text-center">
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
								<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
								<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
							</ul>
						</li>
					</ul>
				</td>
			</tr>
		<?php } ?>
	</tbody>

</table>


<script>
	
	$(document).ready(function(){
		var names = [{"id":"1","name":"Adair,James"}
             , {"id":"2","name":"Anderson,Peter"}
             , {"id":"3","name":"Armstrong,Ryan"}]

		$(".pilih_proyek").select2({
			placeholder: "Pilih Mata Uang",
			width: "100%"
		});



		showLoading();

		$('#tabel').DataTable({
			"responsive": true,
			"dom": 'T<"clear">lfrtip',
			language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
			
		});

		$('.dataTables_filter input[type=search]').attr('placeholder','Cari di sini...');
		$('.dataTables_length select').select2({
			minimumResultsForSearch: "-1"
		});
	});
</script> 
