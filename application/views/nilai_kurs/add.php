<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                
                    <fieldset class="content-group">
                        <div class="form-group">
                            <label class="control-label" for="id_triwulan">Triwulan</label>
                            <select class="select" data-placeholder="Pilih Triwulan" id="id_triwulan" name="id_triwulan">
                                <option></option>
                                <?php 
                                    foreach($triwulan->result() AS $row):
                                        echo '<option value="'.$row->id.'">'.$row->triwulan.'</option>';
                                    endforeach;
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="id_mata_uang">Mata Uang</label>
                            <select class="select" data-placeholder="Pilih Mata Uang" id="id_mata_uang" name="id_mata_uang">
                                <option></option>
                                <?php 
                                    foreach($mata_uang->result() AS $row):
                                        echo '<option value="'.$row->id.'">'.$row->mata_uang.'</option>';
                                    endforeach;
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="nilai">Nilai Kurs</label> <i class="icon-info22 position-left" data-popup="tooltip" title="Gunakan 2 angka di belakang koma" data-placement="bottom"></i></label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input id="nilai" name="nilai" type="text" class="form-control" placeholder="Masukkan nilai kurs terhadap USD">
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="cat">Catatan</label>
                            <input type="text" name="cat" id="cat" class="form-control" placeholder="Masukkan Catatan">
                        </div>
                        
                    </fieldset>
                </div>
                <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->


<script>
    $(document).ready(function(){
        $('[data-popup="tooltip"]').tooltip();
        $('.select').select2();
        
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var id_mata_uang    = $("#id_mata_uang").val();
            var id_triwulan     = $("#id_triwulan").val();
            var nilai           = $("#nilai").val();
            var cat             = $("#cat").val();
			
            var form_data 	= new FormData();
        
            form_data.append('id_mata_uang', id_mata_uang);
            form_data.append('id_triwulan', id_triwulan);
            form_data.append('nilai', nilai);
            form_data.append('cat', cat);
    

            $.ajax({
                url: '<?php echo base_url(); ?>nilai-kurs-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
						loadData("<?php echo base_url(); ?>nilai-kurs-load", "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
        
	});
</script>
