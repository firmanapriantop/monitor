<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-office position-left"></i> <span class="text-semibold">Instansi Penanggung Jawab :</span> UKE Eselon - I</h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>" class="active"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Data Referensi</li>
            <li class="active">Instansi Penanggung Jawab</li>
            <li class="active">UKE Eselon I</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content" id="data">

</div>
<!-- /content area -->

<script>

    $(document).ready(function(){
        loadData("<?php echo base_url(); ?>unit-kerja-load", "data");
    });

</script> 