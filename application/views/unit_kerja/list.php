<!-- Title with left icon -->
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title">Data</h6>
        <div class="heading-elements">
            <button type="button" class="btn btn-success btn-xs" id="btnTambah"><i class="icon-plus-circle2 position-left"></i> Tambah Data</button>
        </div>
    </div>
    
    <div class="panel-body">
    </div>
        <table class="table datatable-responsive table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Instansi</th>
                    <th>Nama</th>
                    <th>Singkatan</th>
                    <th>Status</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no=0;
                    foreach($data->result() AS $row):
                        $no++;
                ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->instansi_singkatan; ?></td>
                    <td><?php echo $row->nama; ?></td>
                    <td><?php echo $row->singkatan; ?></td>
                    <td><?php echo $row->status; ?></td>
                    <td class="text-center"> 
                        <a href="#" onclick='CallPage("<?php echo base_url(); ?>unit-kerja-edit/<?php echo $row->id; ?>", "tmpModal", "modalEdit")' class="text-default" data-popup="tooltip" title="Edit" data-placement="bottom"><i class=" icon-pencil5"></i></a></li>
                        <a href="#" onclick='deleteData("<?php echo base_url(); ?>unit-kerja-delete/<?php echo $row->id; ?>", "<?php echo base_url(); ?>unit-kerja-load", "data")' class="text-danger" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-trash"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    
</div>
<!-- /title with left icon -->

<script src="<?php echo base_url(); ?>assets/muds/muds.js"></script>

<script>

    $(document).ready(function(){
        showLoading();

        $('#btnTambah').click(function(){
            CallPage("<?php echo base_url(); ?>unit-kerja-add", "tmpModal", "modalAdd");
        });
        
        $('[data-popup="tooltip"]').tooltip();

    });
</script>

