<!-- Add modal -->
<div class="modal inmodal fade" id="modalAdd" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Tambah Data</h6>
            </div>

            <form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">

                <div class="modal-body">
                
                    <fieldset class="content-group">
                        <div class="form-group">
                            <label class="control-label" for="tahun">Tahun</label>
                            <input type="text" name="tahun" id="tahun" class="form-control" placeholder="Silahkan masukkan tahun">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tw">Triwulan</label>
                            <select class="select-tw" data-placeholder="Pilih Triwulan" id="tw" name="tw">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="form-group"  for="is_active">
                            <label class="control-label" for="is_active">Status</label>
                            <select class="select-status" data-placeholder="Pilih Status" id="is_active" name="is_active">
                                <option></option>
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                        
                    </fieldset>
                </div>
                <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success btn-xs">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- /Add modal -->


<script>
    $(document).ready(function(){
        $('.select-tw').select2();
        $('.select-status').select2();

         $(".control-success").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-success-600 text-success-800'
        });

        $(".control-danger").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-danger-600 text-danger-800'
        });



		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var tahun       = $("#tahun").val();
            var tw 	        = $("#tw").val();
            var is_active   = $("#is_active").val();
			
            var form_data 	= new FormData();
        
            form_data.append('tahun', tahun);
            form_data.append('tw', tw);
            form_data.append('is_active', is_active);
    

            $.ajax({
                url: '<?php echo base_url(); ?>triwulan-save',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
						loadData("<?php echo base_url(); ?>triwulan-load", "data");
						notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
		});
	});
</script>
