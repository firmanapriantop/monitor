<ul class="navigation navigation-main navigation-accordion">
    <!-- Main -->
    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
    <li <?php if($isActive == 'dashboard' || $isActive == "") echo "class='active'"; ?>><a href="<?php echo base_url(); ?>"><i class="icon-home2"></i> <span>Dashboard</span></a></li>
    <li <?php if($isActive == 'proyek') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>proyek"><i class="icon-stack2"></i> <span>Proyek</span></a></li>
    <!--
    <li>
        <a href="#"><i class="icon-stack2"></i> <span>Proyek</span></a>
        <ul>
            <li 
                <?php 
                    //if($isActive == 'proyek-efektif') echo "class='active'"; 
                ?>
            >
                <a href="<?php //echo base_url(); ?>proyek-efektif">Efektif</a></li>
            <li <?php //if($isActive == 'proyek-tutup') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>proyek-tutup.html">Tutup</a></li>
        </ul>
    </li>
    -->
    <li>
        <a href="#"><i class="icon-shredder"></i> <span>Laporan</span></a>
        <ul>
            <li><a href="#" id="layout1">Rekapitulasi</a></li>
            <li><a href="#" id="layout5">LPK-PHLN</a></li>
        </ul>
    </li>
    <li class="navigation-header"><span>Dokumentasi</span> <i class="icon-menu" title="Main pages"></i></li>
    <li <?php if($isActive == 'pengumuman') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>pengumuman"><i class="icon-bubbles10"></i> <span>Pengumuman</span></a></li>
    <li <?php if($isActive == 'dokumentasi') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>dokumentasi"><i class="icon-notebook"></i> <span>Dokumentasi</span></a></li>
    <li class="navigation-header"><span>Pengguna</span> <i class="icon-menu" title="Main pages"></i></li>
    <li <?php if($isActive == 'pengguna') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>pengguna"><i class="icon-user"></i> <span>Manajemen Pengguna</span></a></li>
    <li class="navigation-header"><span>Data Referensi</span> <i class="icon-menu" title="Main pages"></i></li>
    <li>
        <a href="#"><i class="icon-office"></i> <span>Instansi Penanggung Jawab</span></a>
        <ul>
            <li <?php if($isActive == 'instansi') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>instansi">Instansi</a></li>
            <li <?php if($isActive == 'unit-kerja') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>unit-kerja">Unit Kerja Eselon I</a></li>
        </ul>
    </li>
    <!--
    <li><a href="#"><i class="icon-location3"></i> <span>Wilayah</span></a>
        <ul>
            <li <?php //if($isActive == 'provinsi') echo "class='active'"; ?>><a href="<?php //echo base_url(); ?>provinsi"></i>Provinsi</a></li>
            <li <?php //if($isActive == 'kab-kota') echo "class='active'"; ?>><a href="<?php //echo base_url(); ?>kab-kota"></i>Kabupaten/Kota</a></li>
        </ul>
    </li>
    -->
    <li <?php if($isActive == 'mitra') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>mitra"><i class="icon-user-tie"></i> <span>Mitra Pembangunan</span></a></li>
    <li <?php if($isActive == 'triwulan') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>triwulan"><i class="icon-diff-removed"></i> <span>Triwulan</span></a></li>
    <li <?php if($isActive == 'mata-uang') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>mata-uang"><i class="icon-cash"></i> <span>Mata Uang</span></a></li>
    <li <?php if($isActive == 'nilai-kurs') echo "class='active'"; ?>><a href="<?php echo base_url(); ?>nilai-kurs"><i class="icon-wallet"></i> <span>Nilai Kurs</span></a></li>
    
    <!-- /page kits -->
</ul>