<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MONEV PHLN | User Resgistration</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					<form class="form-horizontal" action="#" id="htmlForm" method="post" enctype="multipart/form-data">
						<div class="panel panel-body login-form">

							<div class="text-center">
								<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
								<h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
							</div>

							<div class="content-divider text-muted form-group"><span>Your credentials</span></div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="email" class="form-control" placeholder="Email" id="email" name="email" required>
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Nama Depan" id="nama_depan" name="nama_depan">
								<div class="form-control-feedback">
									<i class="icon-user-check text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Nama Belakang" id="nama_belakang" name="nama_belakang">
								<div class="form-control-feedback">
									<i class="icon-user-check text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="No. Handphone" id="no_hp" name="no_hp">
								<div class="form-control-feedback">
									<i class="icon-phone2 text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" id="password" name="password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password Confirmation" id="password_conf" name="password_conf">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<button type="submit" class="btn bg-teal btn-block btn-lg">Register <i class="icon-circle-right2 position-right"></i></button>
						</div>
					</form>
					<!-- /advanced login -->

					<div class="login-form">
						<div class="text-center">
						Already have an account? <a href="<?php  echo base_url().'sign-in.html'; ?>">Sign in</a>
						</div>
					</div>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script>
		$(document).ready(function(){
			
			$('#htmlForm').submit(function(e) {
				e.preventDefault();

				var me 			    = $(this);
				
				var email			= $("#email").val();
				var nama_depan      = $("#nama_depan").val();
				var nama_belakang   = $("#nama_belakang ").val();
				var password        = $("#password").val();
				var password_conf   = $("#password_conf").val();
				var no_hp           = $("#no_hp").val();
				
				var form_data 	= new FormData();
				
				form_data.append('email', email);
				form_data.append('nama_depan', nama_depan);
				form_data.append('nama_belakang', nama_belakang);
				form_data.append('password', password);
				form_data.append('password_conf', password_conf);
				form_data.append('no_hp', no_hp);

				$.ajax({
					url: '<?php echo base_url(); ?>register-save',
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function(response){
						if (response.success == true) {
							//$('#modalAdd').modal('hide');
							//loadData("<?php echo base_url(); ?>proyek-efektif-load", "data");
							notif("Informasi", "Data berhasil disimpan.");
						}
						else {
							$.each(response.messages, function(key, value) {
								var element = $('#' + key);
								
								element.closest('div.form-group')
								.removeClass('has-error')
								.addClass(value.length > 0 ? 'has-error' : 'has-success')
								.find('.text-danger')
								.remove();

								element.after(value);
							});
						}
					}
				});
			});
			
		});
	</script>


</body>
</html>
