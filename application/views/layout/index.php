<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MONITOR | Sistem Informasi Pemantauan dan Evaluasi Proyek Pinjaman dan/atau Hibah Luar Negeri</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <?php 
    
		echo $css_js;
		
        //$this->load->view('layout/css_js',array('isActive' => $isActive)); ?>
	<!-- /global stylesheets -->

	<script src="<?php //echo base_url(); ?>assets/muds/muds.js"></script>
	
	<!-- /theme JS files class="sidebar-xs"-->

</head>

<body class="pace-done sidebar-xs">
	<div id="tmpModal"></div>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><!--<img src="assets/images/logo_light.png" alt="">--><strong>MONITOR PHLN</strong></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">


				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<span><?= $this->session->userdata('nama_depan').' '.$this->session->userdata('nama_belakang') ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="<?php echo base_url().'sign-out.html'; ?>"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="
				background-image: url(<?php echo base_url(); ?>assets/images/bg.png);
				background-repeat: repeat; 
				background-color: #37474F; 
				
				border-color: #37474F; 
				position: relative;
				">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?= $this->session->userdata('nama_depan').' '.$this->session->userdata('nama_belakang') ?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<?php $this->load->view('layout/menu'); ?>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
                    
					<?php $this->load->view($content); ?>
					

					<!-- Content area -->
					<div class="content">
						<!-- Footer -->
						<div class="footer text-muted">
							Dirancang, dianalisis dan dikembangkan oleh <a href="#">Firman Perangin-angin</a> Mahasiswa S2 Teknik Informatika <a href="http://themeforest.net/user/Kopyov" target="_blank">Universitas Bina Nusantara</a>
						</div>
						<!-- /footer -->
					</div>
					<!-- /content area -->
                    
                </div>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
