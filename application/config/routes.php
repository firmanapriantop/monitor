<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['triwulan']                  = 'triwulan';
$route['triwulan-add']              = 'triwulan/add_data';
$route['triwulan-load']             = 'triwulan/show_data';
$route['triwulan-edit/(:any)']      = 'triwulan/edit_data/$1';
$route['triwulan-delete/(:any)']    = 'triwulan/delete_data/$1';
$route['triwulan-save']             = 'triwulan/save_data';

$route['mata-uang']                  = 'mata_uang';
$route['mata-uang-add']              = 'mata_uang/add_data';
$route['mata-uang-load']             = 'mata_uang/show_data';
$route['mata-uang-edit/(:any)']      = 'mata_uang/edit_data/$1';
$route['mata-uang-delete/(:any)']    = 'mata_uang/delete_data/$1';
$route['mata-uang-save']             = 'mata_uang/save_data';

$route['nilai-kurs']                  = 'nilai_kurs';
$route['nilai-kurs-add']              = 'nilai_kurs/add_data';
$route['nilai-kurs-load']             = 'nilai_kurs/show_data';
$route['nilai-kurs-edit/(:any)']      = 'nilai_kurs/edit_data/$1';
$route['nilai-kurs-delete/(:any)']    = 'nilai_kurs/delete_data/$1';
$route['nilai-kurs-save']             = 'nilai_kurs/save_data';

$route['mitra']                  = 'mitra';
$route['mitra-add']              = 'mitra/add_data';
$route['mitra-load']             = 'mitra/show_data';
$route['mitra-edit/(:any)']      = 'mitra/edit_data/$1';
$route['mitra-delete/(:any)']    = 'mitra/delete_data/$1';
$route['mitra-save']             = 'mitra/save_data';

$route['instansi']                  = 'instansi';
$route['instansi-add']              = 'instansi/add_data';
$route['instansi-load']             = 'instansi/show_data';
$route['instansi-edit/(:any)']      = 'instansi/edit_data/$1';
$route['instansi-delete/(:any)']    = 'instansi/delete_data/$1';
$route['instansi-save']             = 'instansi/save_data';

$route['unit-kerja']                  = 'unit_kerja';
$route['unit-kerja-add']              = 'unit_kerja/add_data';
$route['unit-kerja-load']             = 'unit_kerja/show_data';
$route['unit-kerja-edit/(:any)']      = 'unit_kerja/edit_data/$1';
$route['unit-kerja-delete/(:any)']    = 'unit_kerja/delete_data/$1';
$route['unit-kerja-save']             = 'unit_kerja/save_data';


$route['proyek-efektif']                                        = 'proyek_efektif';
$route['proyek-efektif-add/(:any)/(:any)']                      = 'proyek_efektif/add_data/$1/$1';
$route['proyek-efektif-load/(:any)']                            = 'proyek_efektif/show_data/$1';
$route['proyek-efektif-edit/(:any)/(:any)/(:any)']              = 'proyek_efektif/edit_data/$1/$1/$1';
$route['proyek-efektif-delete/(:any)']                          = 'proyek_efektif/delete_data/$1';
$route['proyek-efektif-save']                                   = 'proyek_efektif/save_data';
$route['proyek-efektif-detail/(:any)']                          = 'proyek_efektif/detail/$1';
$route['proyek-efektif-profil/(:any)/(:any)']                   = 'proyek_efektif/profil/$1/$1';
$route['proyek-efektif-kumulatif/(:any)/(:any)']                = 'proyek_efektif/kumulatif/$1/$1';

$route['proyek-efektif-ext/(:any)']                             = 'proyek_efektif/ext/$1';
$route['proyek-efektif-submit/(:any)/(:any)/(:any)']            = 'proyek_efektif/submit_data/$1/$1/$1';
$route['proyek-efektif-verify/(:any)/(:any)/(:any)/(:any)/(:any)']     = 'proyek_efektif/verify_data/$1/$1/$1/$1/$1';
$route['proyek-efektif-verify-save']                            = 'proyek_efektif/save_verify_data';

$route['proyek-efektif-realisasi/(:any)/(:any)']                = 'proyek_efektif/realisasi/$1/$1';
$route['proyek-efektif-realisasi-add/(:any)/(:any)']            = 'proyek_efektif/realisasi_add/$1/$1';
$route['proyek-efektif-realisasi-edit/(:any)/(:any)/(:any)']    = 'proyek_efektif/realisasi_edit/$1/$1/$1';
$route['proyek-efektif-realisasi-delete/(:any)/(:any)/(:any)']  = 'proyek_efektif/realisasi_delete/$1/$1/$1';
$route['proyek-efektif-realisasi-submit/(:any)/(:any)/(:any)']  = 'proyek_efektif/realisasi_submit/$1/$1/$1';

//$route['proyek-efektif-fisik/(:any)/(:any)'] = 'proyek_efektif/fisik/$1/$1';
$route['proyek-efektif-fisik'] = 'proyek_efektif/fisik';
$route['proyek-efektif-fisik-add/(:any)/(:any)'] = 'proyek_efektif/fisik_add/$1/$1';
$route['proyek-efektif-fisik-edit/(:any)/(:any)/(:any)'] = 'proyek_efektif/fisik_edit/$1/$1/$1';
$route['proyek-efektif-fisik-delete/(:any)/(:any)/(:any)']  = 'proyek_efektif/fisik_delete/$1/$1/$1';
$route['proyek-efektif-fisik-submit/(:any)/(:any)/(:any)']  = 'proyek_efektif/fisik_submit/$1/$1/$1';

$route['proyek-efektif-realisasi-save']                         = 'proyek_efektif/realisasi_save';



$route['dashboard']         = 'dashboard';
$route['sign-up']           = 'dashboard/register';
$route['register-save']     = 'dashboard/save_register';
$route['sign-in']           = 'dashboard/login';
$route['sign-out']          = 'dashboard/logout';


$route['proyek'] = 'proyek';
$route['proyek-load/(:any)'] = 'proyek/load_data/$1';
$route['proyek-add/(:any)/(:any)'] = 'proyek/add_data/$1/$1';
$route['proyek-save'] = 'proyek/save_data';
$route['proyek-edit/(:any)/(:any)'] = 'proyek/edit_data/$1/$1';
$route['proyek-delete/(:any)'] = 'proyek/delete_data/$1';
$route['proyek-detail/(:any)'] = 'proyek/detail_data/$1';
$route['proyek-profil/(:any)'] = 'proyek/profil/$1';

$route['proyek-kategori/(:any)'] = 'proyek/kat/$1';
$route['proyek-kategori-add/(:any)'] = 'proyek/kat_add/$1';
$route['proyek-kategori-save'] = 'proyek/kat_save';
$route['proyek-kategori-edit/(:any)'] = 'proyek/kat_edit/$1';
$route['proyek-kategori-delete/(:any)'] = 'proyek/kat_delete/$1';

$route['proyek-kategori-instansi/(:any)'] = 'proyek/kat_instansi/$1';
$route['proyek-kategori-instansi-add/(:any)'] = 'proyek/kat_instansi_add/$1';
$route['proyek-kategori-instansi-save'] = 'proyek/kat_instansi_save';
$route['proyek-kategori-instansi-edit/(:any)'] = 'proyek/kat_instansi_edit/$1';
$route['proyek-kategori-instansi-delete/(:any)'] = 'proyek/kat_instansi_delete/$1';

$route['proyek-kategori-wilayah/(:any)'] = 'proyek/kat_wilayah/$1';
$route['proyek-kategori-wilayah-add/(:any)'] = 'proyek/kat_wilayah_add/$1';
$route['proyek-kategori-wilayah-save'] = 'proyek/kat_wilayah_save';
$route['proyek-kategori-wilayah-edit/(:any)'] = 'proyek/kat_wilayah_edit/$1';
$route['proyek-kategori-wilayah-delete/(:any)'] = 'proyek/kat_wilayah_delete/$1';

$route['proyek-paket/(:any)'] = 'proyek/paket/$1';
$route['proyek-paket-add/(:any)'] = 'proyek/paket_add/$1';
$route['proyek-paket-save'] = 'proyek/paket_save';
$route['proyek-paket-edit/(:any)'] = 'proyek/paket_edit/$1';
$route['proyek-paket-delete/(:any)'] = 'proyek/paket_delete/$1';
$route['proyek-paket-detail/(:any)'] = 'proyek/paket_detail/$1';

$route['proyek-paket-target-realisasi/(:any)'] = 'proyek/paket_target_realisasi/$1';

$route['proyek-paket-target/(:any)'] = 'proyek/paket_target/$1';
$route['proyek-paket-target-add/(:any)'] = 'proyek/paket_target_add/$1';
$route['proyek-paket-target-save'] = 'proyek/paket_target_save';
$route['proyek-paket-target-edit/(:any)'] = 'proyek/paket_target_edit/$1';
$route['proyek-paket-target-delete/(:any)'] = 'proyek/paket_target_delete/$1';

$route['proyek-paket-realisasi/(:any)'] = 'proyek/paket_realisasi/$1'; //parent_id_paket
$route['proyek-paket-realisasi-add/(:any)'] = 'proyek/paket_realisasi_add/$1'; //parent_id_paket
$route['proyek-paket-realisasi-save'] = 'proyek/paket_realisasi_save';
$route['proyek-paket-realisasi-edit/(:any)'] = 'proyek/paket_realisasi_edit/$1'; //id
$route['proyek-paket-realisasi-delete/(:any)'] = 'proyek/paket_realisasi_delete/$1'; //id

$route['proyek-masalah/(:any)'] = 'proyek/masalah/$1'; //parent_id_proyek
$route['proyek-masalah-add/(:any)'] = 'proyek/masalah_add/$1'; //parent_id_paket
$route['proyek-masalah-save'] = 'proyek/masalah_save';
$route['proyek-masalah-edit/(:any)'] = 'proyek/masalah_edit/$1'; //id
$route['proyek-masalah-delete/(:any)'] = 'proyek/masalah_delete/$1'; //id

$route['proyek-realisasi/(:any)'] = 'proyek/realisasi/$1'; //parent_id proyek