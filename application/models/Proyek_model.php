<?php
/**
	* Model : Proyek_m
	* Tabel terkait : monitor_proyek
	@firman.apriantop@gmail.com
**/

	class Proyek_model extends CI_Model 
	{
		
		function ambil_data_proyek($id_instansi)
		{
			$this->db->select('	monitor_proyek.id 			AS id, 
								monitor_instansi.id 		AS id_instansi,
								monitor_instansi.singkatan	AS instansi_singkatan,
								monitor_proyek.nama			AS nama,
								monitor_mitra.singkatan 	AS mitra_singkatan,
								monitor_proyek.kode 		AS kode
								');
			$this->db->from('monitor_proyek');
			$this->db->join('monitor_instansi_eselon_satu', 'monitor_instansi_eselon_satu.id = monitor_proyek.id_instansi_eselon_satu', 'inner');
			$this->db->join('monitor_instansi', 'monitor_instansi.id = monitor_instansi_eselon_satu.id_instansi', 'inner');
			$this->db->join('monitor_mitra', 'monitor_mitra.id = monitor_proyek.id_mitra', 'inner');
			//if($id_instansi <> "0"){
				$this->db->where('monitor_instansi.id', $id_instansi);
			//}
			return $this->db->get();
		}

		function ambil_data_proyek_per_id($id)
		{
			$this->db->select('	monitor_proyek.id 			AS id, 
								monitor_instansi.singkatan	AS instansi_singkatan,
								monitor_proyek.nama			AS nama,
								monitor_mitra.singkatan 	AS mitra_singkatan,
								monitor_proyek.kode 		AS kode

								');
			$this->db->from('monitor_proyek');
			$this->db->join('monitor_instansi_eselon_satu', 'monitor_instansi_eselon_satu.id = monitor_proyek.id_instansi_eselon_satu', 'inner');
			$this->db->join('monitor_instansi', 'monitor_instansi.id = monitor_instansi_eselon_satu.id_instansi', 'inner');
			$this->db->join('monitor_mitra', 'monitor_mitra.id = monitor_proyek.id_mitra', 'inner');
			$this->db->where('monitor_proyek.id', $id);
			return $this->db->get();
		}

		function ambil_data($tabel, $order, $sortir)
		{
			$this->db->select('*');
			$this->db->from($tabel);
			$this->db->order_by($order, $sortir);
			return $this->db->get();
		}

		function ambil_data_instansi()
		{
			$this->db->select('*');
			$this->db->from('monitor_instansi');
			return $this->db->get();
		}
		
	}

?>