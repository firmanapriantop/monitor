<?php
/**
	* Model : General_model
	* Tabel terkait : monitor_proyek
**/

	class General_model extends CI_Model 
	{

		function get_data_qry($qry)
		{
			return $this->db->query($qry);
		}

		function check_if_duplicate($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$sql = $this->db->get();

			if($sql->num_rows() > 0){
				return TRUE;
			}else{
				return FALSE;
			}
		}

		function save_data($tabel, $data)
		{
			if(array_key_exists('id', $data))
			{
				$id = $data['id'];
				unset($data['id']);
				$this->db->where('id', $id);
				$this->db->update($tabel, $data);
			}
			else
			{
				$this->db->insert($tabel, $data);
			}		
		}

		function delete_data($nama_tabel, $id)
		{
			$this->db->where('id', $id);
			return $this->db->delete($nama_tabel);
		}




		function ambil_data($tabel, $order, $sortir)
		{
			$this->db->select('*');
			$this->db->from($tabel);
			$this->db->order_by($order, $sortir);
			return $this->db->get();
		}

		function ambil_data_by($tabel, $field, $field_value, $order, $sortir)
		{
			
			$this->db->select('*');
			$this->db->from($tabel);
			$this->db->where($field, $field_value);
			$this->db->order_by($order, $sortir);

			//var_dump($this->db);

			return $this->db->get();
			
/*
			$query = "SELECT * FROM monitor_mitra WHERE id_sumber = '$field_value' ORDER BY nama ASC";
       		return $this->db->query($query)->result();
*/
		}

		function select_data($tabel, $where, $order, $sortir)
		{
			$this->db->select('*');
			$this->db->from($tabel);
			if($where <> "0"){
				$this->db->where($where);
			}
			$this->db->order_by($order, $sortir);
			return $this->db->get();
		}


		

		

		
	}

?>