<?php
/**
	* Model : User_role_model
	* Tabel terkait : monitor_user, monitor_user_role
	@firman.apriantop@gmail.com
**/

	class User_role_model extends CI_Model 
	{
		
		function ambil_data_proyek()
		{
			$this->db->select('	monitor_user_role.id_user   AS id_user, 
                                monitor_user.nama_depan     AS nama_depan,
								monitor_user.nama_belakang  AS nama_belakang,
								monitor_user_role.role      AS role

								');
			$this->db->from('monitor_user_role');
			$this->db->join('monitor_user', 'monitor_user.id = monitor_user_role.id_user', 'inner');
			
			return $this->db->get();
		}

		function ambil_data_proyek_per_id($id)
		{
			$this->db->select('	monitor_proyek.id 			AS id, 
								monitor_instansi.singkatan	AS instansi_singkatan,
								monitor_proyek.nama			AS nama,
								monitor_mitra.singkatan 	AS mitra_singkatan,
								monitor_proyek.kode 		AS kode

								');
			$this->db->from('monitor_proyek');
			$this->db->join('monitor_instansi_eselon_satu', 'monitor_instansi_eselon_satu.id = monitor_proyek.id_instansi_eselon_satu', 'inner');
			$this->db->join('monitor_instansi', 'monitor_instansi.id = monitor_instansi_eselon_satu.id_instansi', 'inner');
			$this->db->join('monitor_mitra', 'monitor_mitra.id = monitor_proyek.id_mitra', 'inner');
			$this->db->where('monitor_proyek.id', $id);
			return $this->db->get();
		}

		function ambil_data($tabel, $order, $sortir)
		{
			$this->db->select('*');
			$this->db->from($tabel);
			$this->db->order_by($order, $sortir);
			return $this->db->get();
		}

		function ambil_data_instansi()
		{
			$this->db->select('*');
			$this->db->from('monitor_instansi');
			return $this->db->get();
		}

		function ambil_data_proyek
		
	}

?>