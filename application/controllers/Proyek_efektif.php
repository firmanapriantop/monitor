<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek_efektif extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        $this->load->model(array('general_model' => 'gm', 'proyek_model'));
	}
	
	public function index()
	{
		if(is_logged_in()) {

			$data["content"]    = 'proyek_efektif/index';
            $data["isActive"]   = 'proyek-efektif';
            $data['css_js'] 	= asset_url('proyek-efektif');
			//echo $css_js;
			
			$qry = $this->gm->get_data_qry("SELECT CONCAT(tahun, tw) as tahun_tw, CONCAT('Triwulan ', tw, ' Tahun ', tahun) as full_tw, tgl, id FROM triwulan WHERE is_active = 1");
			
			$data['tahun_tw'] 	= $qry->row('tahun_tw');
			$data['full_tw'] 	= $qry->row('full_tw');
			$data['tgl_tw']		= $qry->row('tgl');
			$data['id_tw']		= $qry->row('id');
 
            $this->load->view('layout/index', $data);
			

		}else{

			$data['current_link'] = uri_string();

			$this->load->view('template/login', $data); 		
		}

    }

    function show_data()
	{
		//cek user berdasarkan rolenya 

		//$tw 			= $this->uri->segment(2);
		$id_triwulan 	= $this->uri->segment(2);

		//get tahun triwulan
		$data['tahun_tw']  = $this->gm->get_data_qry("SELECT tahun FROM triwulan WHERE id = '$id_triwulan'")->row('tahun');
		
		$data['data'] 	= $this->gm->get_data_qry('SELECT *, (nilai_kurs * nilai_pinjaman_ASLI) AS nilai_pinjaman_USD, (nilai_kurs * realisasi_kum_ASLI) AS realisasi_kum_USD, ((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) AS persen_kum, (nilai_target_rp / kurs_USD) AS nilai_target_USD, (nilai_kurs * realisasi_thn_ASLI) AS realisasi_thn_USD, (((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) - waktu_terpakai) AS pv FROM v_proyek_dashboard');

		//send id_tw 
		$data['id_tw'] = $id_triwulan;
	
		$this->load->view('proyek_efektif/list', $data);
		
	}
	
	function get_tw()
	{
		$id_triwulan = $this->uri->segment(3);
		$result = $this->gm->get_data_qry("SELECT * FROM triwulan WHERE id = '$id_triwulan'")->row();
		if($result->is_active == 1){
			echo "aa";
		}
	}

	function add_data()
    {
		$data['mitra']   	= $this->gm->get_data_qry("SELECT * FROM mitra WHERE is_active = 1");
		$data['instansi']   = $this->gm->get_data_qry("SELECT * FROM instansi WHERE is_active = 1 AND id_parent = 0");
		$data['sektor']    	= $this->gm->get_data_qry("SELECT * FROM sektor WHERE is_active = 1");
		$data['mata_uang']	= $this->gm->get_data_qry("SELECT * FROM mata_uang WHERE is_active = 1");

		$data['tahun_tw']  	= $this->uri->segment(2);
		$data['id_tw']		= $this->uri->segment(3);

		$this->load->view('proyek_efektif/add', $data);
	}
	
	function save_data()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("jenis", "Jenis Proyek", "trim|required");
		$this->form_validation->set_rules("id_instansi", "Instansi Penanggung Jawab", "trim|required");
		$this->form_validation->set_rules("id_mitra", "Mitra Pembangunan", "trim|required");
		$this->form_validation->set_rules("id_sektor", "Sektor/Bidang", "trim|required");
		$this->form_validation->set_rules("kode", "Kode Proyek", "trim|required");
		$this->form_validation->set_rules("nama_en", "Judul Proyek (EN)", "trim|required");
		$this->form_validation->set_rules("nama_id", "Judul Proyek (ID)", "trim|required");
		$this->form_validation->set_rules("tgl_mulai", "Tanggal Mulai", "trim|required");
		$this->form_validation->set_rules("tgl_selesai", "Tanggal Selesai", "trim|required");
		$this->form_validation->set_rules("id_mata_uang", "Mata Uang", "trim|required");
		$this->form_validation->set_rules("nilai_pinjaman", "Nilai Pinjaman", "trim|required|numeric");
		$this->form_validation->set_rules("instansi_pelaksana", "Instansi Pelaksana", "trim|required");
		$this->form_validation->set_rules("ruang_lingkup", "Ruang Lingkup", "trim|required");
		
        //$this->form_validation->set_rules("file", "File", "required");
        
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			//cek kode proyek -> unique

			if(empty($this->input->post('id'))){
			
				$data 						= $_POST;

				$data['jlh_ext']			= 0;
				$data['status']				= "pending-add";
				$data['status_act']			= "add.new";
				$data['submit_operator']	= "0";
				$data['verify_bila_multi']	= "0";
				$data['verify_sisdur']		= "0";
				$data['is_active']			= "1";
				$data['input_by']			= $this->session->userdata('id');
				$data['input_date']			= date('Y-m-d H:i:s');


				$result				= $this->gm->save_data('proyek', $data);
				$status['success']  = true;
				
			}else{

				$data 				= $_POST;

				$result				= $this->gm->save_data('proyek', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 
	
	function edit_data()
	{
		$id     		= $this->uri->segment(2);
		$sql			= "SELECT * FROM v_proyek WHERE id = '$id'";

		$qry    		= $this->gm->get_data_qry($sql);
		$data['data']	= $qry->row();

		$data['mitra']   	= $this->gm->get_data_qry("SELECT * FROM mitra WHERE is_active = 1");
		$data['instansi']   = $this->gm->get_data_qry("SELECT * FROM instansi WHERE is_active = 1 AND id_parent = 0");
		$data['sektor']    	= $this->gm->get_data_qry("SELECT * FROM sektor WHERE is_active = 1");
		$data['mata_uang']	= $this->gm->get_data_qry("SELECT * FROM mata_uang WHERE is_active = 1");

		$data['tahun_tw']  	= $this->uri->segment(3);
		$data['id_tw']		= $this->uri->segment(4);

		$this->load->view('proyek_efektif/edit', $data);
	}

	function delete_data()
	{	
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('proyek', $id);
	}

	function detail()
	{
		/*
		$id = $this->uri->segment(2);
		$data['data'] = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'");
		$data['id_proyek'] = $id;
		$this->load->view('proyek_efektif/detail', $data);
		*/
		/*
		$data['id_proyek'] = '2';
		$data['id_triwulan'] = '1';
		$data['tw'] = $this->gm->get_data_qry("SELECT * FROM triwulan WHERE id = '1'")->row();
		$this->load->view('proyek_efektif/detail', $data);
		*/
		

		$id = $this->uri->segment(2);

		$data['data'] = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'");
		$data['id_proyek'] = $id;
		$data['id_triwulan'] = '1';
		$this->load->view('proyek_efektif/detail', $data);

	}

	function profil()
	{
		$id = $this->uri->segment(2);
		$data['proyek'] = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'")->row();
		$this->load->view('proyek_efektif/detail/profil', $data);
	}
	/*
	function kumulatif()
	{
		$id 				= $this->uri->segment(2);
		$data['id_proyek'] 	= $id;

		$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		$data['realisasi'] = $this->gm->get_data_qry("SELECT *, ((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) AS persen_kum, (((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) - waktu_terpakai) AS pv, (nilai_pinjaman_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, (nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, (nilai_target_rp / kurs_USD) AS nilai_target_USD, ((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  FROM v_proyek_realisasi WHERE kode = '$kode'");

		//get tw actie
		$data['id_tw']		= $this->gm->get_data_qry("SELECT id FROM triwulan WHERE is_active = '1'")->row('id');
		
		$this->load->view('proyek_efektif/detail/kum', $data);
	}
	*/

	function submit_data(){

		$id 		= $this->uri->segment(2);
		$jenis		= $this->uri->segment(3); //usulan or realisasi
		$sub_jenis	= $this->uri->segment(4); //add/edit/extend

		$id_user	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		switch ($jenis) {
			case "usulan":
				$status = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'")->row('status');
				switch ($sub_jenis) {
					case "add":
						$status 	= "submit-add";
						$status_act	= "submit.add.to.verify";
					break;

					case "edit":
						$status 			= "submit-edit";
						$status_act			= "submit.edit.to.verify";
						
					break;

					case "request-extend":
						$status 	= "req-ext";
						$status_act	= "request.to.extend";
					break;

					case "submit-extend":
						$status 	= "submit-request-extend";
						$status_act	= "submit.request.extend.to.verify";
					break;
				}

				$query = 
					"	UPDATE 
							proyek 
						SET 
							submit_operator = '1', 
							status = '$status', 
							status_act = '$status_act', 
							verify_bila_multi = '0', 
							verify_sisdur = '0', 
							input_by = '$id_user', 
							input_date= '$timestamp' 
						WHERE 
							id = '$id'
					";

				$result 	= $this->gm->get_data_qry($query);

			break;

			case "realisasi":
				$status = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'")->row('status');
				switch ($sub_jenis) {
					case "add":
						$status 	= "submit-add";
						$status_act	= "submit.add.to.verify";
					break;

					case "edit":
						$status 	= "submit-edit";
						$status_act	= "submit.edit.to.verify";
					break;

					case "extend":
						$status 	= "submit-extend";
						$status_act	= "submit.extend.to.verify";
					break;
				}

				$result 	= $this->gm->get_data_qry("UPDATE proyek SET submit_operator = '1', status = '$status', status_act = '$status_act',input_by = '$id_user', input_date= '$timestamp' WHERE id = '$id'");

			break;
		}

	}

	function verify_data(){
		$id_proyek	= $this->uri->segment(2);
		//$tahun_tw	= $this->uri->segment(3);
		$id_tw		= $this->uri->segment(3);
		$jenis		= $this->uri->segment(4); //usulan or realisasi etc
		$sub_jenis		= $this->uri->segment(5); //add or edit or extend
		$oleh 		= $this->uri->segment(6); //bila-multi atau sisdur

		$data['id_proyek']		= $id_proyek;
		$data['id_tw']			= $id_tw;
		$data['verified_by']	= $oleh;
		$data['jenis']			= $jenis;


		switch ($jenis){
			case "usulan":
				$qry = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id_proyek'");
				
				$data['status']				= $qry->row('status');
				$data['verify_bila_multi']	= $qry->row('verify_bila_multi');
				$data['verify_sisdur']		= $qry->row('verify_bila_multi');
				$data['link_to_refresh']	= base_url()."proyek-efektif-load/".$id_tw;

				switch ($oleh){
					case "verify-bila-multi":
						$data['judul']	= "Verifikasi Dit. Bilateral atau Dit. Multilateral";
						$data['verify']	= $qry->row('verify_bila_multi');
						$data['cat']	= $qry->row('cat_bila_multi');
					break;

					case "verify-sisdur":
						$data['judul']	= "Verifikasi Dit. Sisdur";
						$data['verify']	= $qry->row('verify_sisdur');
						$data['cat']	= $qry->row('cat_sisdur');
					break;
				}
			break;

			case "realisasi":
			break;
		}
		

		$this->load->view('proyek_efektif/verify', $data);
		
	}

	function save_verify_data(){
		$status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("verify", "Hasil Verifikasi", "trim|required");
		$this->form_validation->set_rules("cat", "Catatan", "trim|required");
        
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

	

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			$verified_by	= $this->input->post('verified_by'); //verifikasi bila-multi or sisdur
			$jenis			= $this->input->post('jenis'); //usulan or realisasi
			$sub_jenis		= $this->input->post('sub_jenis'); //usulan or realisasi

			$id = $this->input->post('id');

			switch ($jenis){
				case "usulan":
					$query = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id'");

					switch ($verified_by){
						case "verify-bila-multi":
							$data['verify_bila_multi']	= $this->input->post('verify');
							$data['cat_bila_multi']		= $this->input->post('cat');
							$data['status_act']			= "verified.by.bila-multi";

							//cek hasil verifikasi
							if($query->row('verify_sisdur') == "0" || $this->input->post('verify') == "0"){
								$data['status']		= "being-verified";
							}elseif ($query->row('verify_sisdur') == "1" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-del";
							}elseif ($query->row('verify_sisdur') == "1" && $this->input->post('verify') == "2"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_sisdur') == "2" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_sisdur') == "2" && $this->input->post('verify') == "2"){
								$data['status']		= "ready";
							}

						break;
						
						case "verify-sisdur":
							$data['verify_sisdur']	= $this->input->post('verify');
							$data['cat_sisdur']		= $this->input->post('cat');
							$data['status_act']		= "verified.by.sisdur";

							if($query->row('verify_bila_multi') == "0" || $this->input->post('verify') == "0"){
								$data['status']		= "being-verified";
							}elseif ($query->row('verify_bila_multi') == "1" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-del";
							}elseif ($query->row('verify_bila_multi') == "1" && $this->input->post('verify') == "2"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_bila_multi') == "2" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_bila_multi') == "2" && $this->input->post('verify') == "2"){
								$data['status']		= "ready";
							}
						break;
					}

					$data['id']			= $this->input->post('id');
					$result				= $this->gm->save_data('proyek', $data);
					$status['success']  = true;
				break;
			}

			
				
		}
		echo json_encode($status);
	}

	function realisasi()
	{
		//$id = $this->uri->segment(2);
		//$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		//$data['realisasi'] = $this->gm->get_data_qry("SELECT *, ((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) AS persen_kum, (((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) - waktu_terpakai) AS pv, (nilai_pinjaman_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, (nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, (nilai_target_rp / kurs_USD) AS nilai_target_USD, ((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  FROM v_proyek_realisasi WHERE kode = '$kode'");
		//$this->load->view('proyek_efektif/detail/realisasi');
		$id 				= $this->uri->segment(2); //id_proyek
		$data['id_proyek'] 	= $id;

		$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		$query = "SELECT *, 
						((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) AS persen_kum, 
						(((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) - waktu_terpakai) AS pv, 
						(nilai_pinjaman_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, 
						(nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, 
						(nilai_target_rp / kurs_USD) AS nilai_target_USD, 
						((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  
					FROM 
						v_proyek_realisasi 
					WHERE kode = '$kode'
				";
		$data['realisasi'] = $this->gm->get_data_qry($query);

		//get tw actie
		$data['id_tw']		= $this->gm->get_data_qry("SELECT id FROM triwulan WHERE is_active = '1'")->row('id');
		
		$this->load->view('proyek_efektif/detail/realisasi', $data);
	}

	function ext()
	{	
		$id     	= $this->uri->segment(2);

		$row = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id'");
		$submit_operator = $row->row('submit_operator');
		$jlh_ext = $row->row('jlh_ext');
		
		if ($submit_operator == "1"){

			$status 	= "req-ext";
			$status_act = "request.to.extend";
			$input_by 	= $this->session->userdata('id');
			$timestamp 	= date('Y-m-d H:i:s');

			$query	= $this->gm->get_data_qry("UPDATE proyek SET status = '$status', status_act = '$status_act', submit_operator = '2', input_by = '$input_by', input_date= '$timestamp' WHERE id = '$id'");

		}elseif ($submit_operator == "2"){ //dizinkan untuk melakukan perpanjangan

			$status 	= "ext-edit";
			$status_act = "accept.to.extend";
			$input_by 	= $this->session->userdata('id');
			$timestamp 	= date('Y-m-d H:i:s');
			$jlh_ext 	= $jlh_ext+1;

			$query	= $this->gm->get_data_qry("UPDATE proyek SET status = '$status', status_act = '$status_act', is_active = '0', input_by = '$input_by',  input_date= '$timestamp' WHERE id = '$id'");

			$jenis			= $row->row('jenis');
			$id_instansi	= $row->row('id_instansi');
			$id_mitra		= $row->row('id_mitra');
			$id_sektor		= $row->row('id_sektor');
			$kode			= $row->row('kode');
			$nama_en		= $row->row('nama_en');
			$nama_id		= $row->row('nama_id');
			$tgl_mulai		= $row->row('tgl_mulai');
			$tgl_selesai	= $row->row('tgl_selesai');
			$id_mata_uang	= $row->row('id_mata_uang');
			$nilai_pinjaman	= $row->row('nilai_pinjaman');
			$instansi_pelaksana	= $row->row('instansi_pelaksana');
			$ruang_lingkup		= $row->row('ruang_lingkup');
			$cat_bila_multi		= $row->row('cat_bila_multi');
			$cat_sisdur			= $row->row('cat_sisdur');
			//create tambah baru
			$query = $this->gm->get_data_qry("INSERT INTO proyek (jenis, id_instansi, id_mitra, id_sektor, kode, nama_en, nama_id, tgl_mulai, tgl_selesai, id_mata_uang, nilai_pinjaman, jlh_ext, instansi_pelaksana, ruang_lingkup, status, status_act, is_active, submit_operator, verify_bila_multi, cat_bila_multi, verify_sisdur, cat_sisdur, input_by, input_date) VALUES ('$jenis', '$id_instansi', '$id_mitra', '$id_sektor', '$kode', '$nama_en', '$nama_id', '$tgl_mulai', '$tgl_selesai', '$id_mata_uang', '$nilai_pinjaman', '$jlh_ext', '$instansi_pelaksana', '$ruang_lingkup', '$status', '$status_act', '1', '1', '0', '$cat_bila_multi', '0', '$cat_sisdur', '$input_by', '$timestamp')");


		}
		
	}

	function realisasi_add()
	{
		$id 	= $this->uri->segment(2);
		$id_tw 	= $this->uri->segment(3);

		$query	= "	SELECT 
						CONCAT('Triwulan ', triwulan.tw, ' Tahun ', triwulan.tahun) AS tw,
						nilai_kurs.nilai AS nilai_kurs,
						nilai_kurs.id AS id_nilai_kurs,
						mata_uang.singkatan AS mata_uang
					FROM 
						nilai_kurs 
					INNER JOIN 
						proyek ON proyek.id_mata_uang = nilai_kurs.id_mata_uang 
					INNER JOIN
						triwulan ON triwulan.id	= nilai_kurs.id_triwulan
					INNER JOIN
						mata_uang ON mata_uang.id	= nilai_kurs.id_mata_uang
					WHERE 
						nilai_kurs.id_triwulan = '$id_tw' AND proyek.id = '$id'
					";

		$row	= $this->gm->get_data_qry($query);

		$data['nilai_kurs']		= $row->row('nilai_kurs');
		$data['tw']				= $row->row('tw');
		$data['mata_uang']		= $row->row('mata_uang');
		$data['id_nilai_kurs']	= $row->row('id_nilai_kurs');
		$data['id_triwulan']	= $id_tw;
		$data['id_proyek']		= $id;

		$this->load->view('proyek_efektif/realisasi/add', $data);
	}

	function realisasi_edit()
	{
		$id 		= $this->uri->segment(2); 
		$id_proyek 	= $this->uri->segment(3); 
		$id_tw 		= $this->uri->segment(4);

		$query 	= 
			"SELECT
				realisasi.*,
				CONCAT('Triwulan ', triwulan.tw, ' Tahun ', triwulan.tahun) AS tw,
				nilai_kurs.nilai AS nilai_kurs,
				mata_uang.singkatan AS mata_uang
			FROM
				realisasi
				INNER JOIN nilai_kurs ON nilai_kurs.id = realisasi.id_nilai_kurs
				INNER JOIN triwulan ON triwulan.id = realisasi.id_triwulan
				INNER JOIN mata_uang ON mata_uang.id = nilai_kurs.id_mata_uang
			WHERE
				realisasi.id = '$id'
			";

		$row	= $this->gm->get_data_qry($query);

		$data['data']	= $row->row();

		$nilai_kurs = $row->row('nilai_kurs');
		$realisasi_uang_asli = $row->row('realisasi_uang');
		$data['realisasi_uang_usd'] = $nilai_kurs * $realisasi_uang_asli;
		/*
		$realisasi_uang = $row->row('realisasi_uang');
		$nilai_kurs		= $row->row('nilai_kurs');

		$data['realisasi_uang_usd'] = $realisasi_uang * $nilai_kurs;
		$data['nilai_kurs']			= $row->row('nilai_kurs');
		$data['tw']					= $row->row('tw');
		$data['mata_uang']			= $row->row('mata_uang');
		$data['id_nilai_kurs']		= $row->row('id_nilai_kurs');
		$data['id_triwulan']		= $id_tw;
		$data['id_proyek']			= $id;
			*/
		$this->load->view('proyek_efektif/realisasi/edit', $data);


		/*

		$input_by 	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		if ($kat == "req-to-edit"){
			
			
			$query	= "	UPDATE 
							realisasi 
						SET 
							status 		= 'req-edit', 
							status_act 	= 'req-to-edit', 
							input_by 	= '$input_by',  
							input_date	= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}elseif ($kat == "approve-to-edit"){

			$query	= "	UPDATE 
							realisasi 
						SET 
							status 				= 'edit', 
							status_act 			= 'approved-to-edit', 
							verify_bila_multi	= 0,
							cat_bila_multi		= '',
							verify_sisdur		= 0,
							cat_sisdur			= '',
							input_by 			= '$input_by',  
							input_date			= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}*/
		
	}

	function realisasi_delete()
	{
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('realisasi', $id);
	}

	function realisasi_submit()
	{
		$id 	= $this->uri->segment(2);
		$id_tw 	= $this->uri->segment(3);
		$kat 	= $this->uri->segment(4);

		$input_by 	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		if ($kat == "submit-add"){
			
			
			$query	= "	UPDATE 
							realisasi 
						SET 
							status 		= 'req-edit', 
							status_act 	= 'req-to-edit', 
							input_by 	= '$input_by',  
							input_date	= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}elseif ($kat == "submit-edit"){

			$query	= "	UPDATE 
							realisasi 
						SET 
							status 				= 'submit-edit', 
							status_act 			= 'submit-edit', 
							input_by 			= '$input_by',  
							input_date			= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}
		
	}

	function realisasi_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("id_proyek", "ID Proyek", "trim|required");
		$this->form_validation->set_rules("id_triwulan", "ID Triwulan", "trim|required");
		$this->form_validation->set_rules("id_nilai_kurs", "ID Nilai Kurs", "trim|required");
		$this->form_validation->set_rules("realisasi_uang", "Realisasi Triwulan", "trim|required|numeric");
		$this->form_validation->set_rules("ket", "Keterangan", "trim|required");
        
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			//cek kode proyek -> unique

			if(empty($this->input->post('id'))){
			
				$data 						= $_POST;

				$data['status']				= "pending-add";
				$data['status_act']			= "add.new";
				$data['submit_operator']	= "0";
				$data['verify_bila_multi']	= "0";
				$data['verify_sisdur']		= "0";
				$data['input_by']			= $this->session->userdata('id');
				$data['input_date']			= date('Y-m-d H:i:s');


				$result				= $this->gm->save_data('realisasi', $data);
				$status['success']  = true;
				
			}else{

				$data 				= $_POST;

				$result				= $this->gm->save_data('realisasi', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function fisik()
	{
		/*
		$id	= $this->uri->segment(2); //id_proyek
		$data['id_proyek'] = $id;

		$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		$query = "SELECT *, 
						((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) AS persen_kum, 
						(((realisasi_kum_ASLI / nilai_pinjaman_ASLI) * 100) - waktu_terpakai) AS pv, 
						(nilai_pinjaman_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, 
						(nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, 
						(nilai_target_rp / kurs_USD) AS nilai_target_USD, 
						((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  
					FROM 
						v_proyek_realisasi 
					WHERE kode = '$kode'
				";
		$data['realisasi'] = $this->gm->get_data_qry($query);

		//get tw actie
		$data['id_tw']		= $this->gm->get_data_qry("SELECT id FROM triwulan WHERE is_active = '1'")->row('id');
		
		$this->load->view('proyek_efektif/detail/fisik', $data);
		*/
		$this->load->view('proyek_efektif/detail/fisik');
	}

}
