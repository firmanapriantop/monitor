<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instansi extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->library('session');
		$this->load->helper('cookie');
		$this->load->model(array('general_model' => 'gm'));
		//$this->load->helper('app_helper');
    }

	public function index()
	{
		if(is_logged_in()) {


			$data["content"]    = 'instansi/index';
            $data["isActive"]   = 'instansi';
            $data['css_js'] = asset_url('instansi');
			//echo $css_js;
            $this->load->view('layout/index', $data);
			

		}else{
			$this->load->view('template/login'); 		
		}

    }

    function show_data()
	{
		$qry = 'SELECT * FROM v_instansi';

		$data['data'] = $this->gm->get_data_qry($qry);
		$this->load->view('instansi/list', $data);
    }

    function add_data()
    {
		 $this->load->view('instansi/add');
	}
	
	function save_data()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("level", "Tipe Instansi", "trim|required");
		$this->form_validation->set_rules("nama", "Nama Instansi", "trim|required");
		$this->form_validation->set_rules("singkatan", "Singkatan Instansi", "trim|required");
		$this->form_validation->set_rules("is_active", "Status", "trim|required");
        //$this->form_validation->set_rules("file", "File", "required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			if(empty($this->input->post('id'))){
			
				$data 				= $_POST;
				$result				= $this->gm->save_data('instansi', $data);
				$status['success']  = true;
				
			}else{

				$data 				= $_POST;
				$result				= $this->gm->save_data('instansi', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 
	
	function edit_data()
	{
		$id     		= $this->uri->segment(2);
		$sql			= "SELECT * FROM v_instansi WHERE id = '$id'";

		$qry    		= $this->gm->get_data_qry($sql);
		$data['data']	= $qry->row();

		$this->load->view('instansi/edit', $data);
	}

	function delete_data()
	{	
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('instansi', $id);
	}
		
		
}
