<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->library('session');
		$this->load->helper(array('url'));
		$this->load->model(array('general_model' => 'gm'));
		//$this->load->helper('app_helper');
    }

	public function index()
	{
		
		
		if (is_logged_in()) {

			$data["content"]    = 'dashboard/index';
            $data["isActive"]   = 'dashboard';
            $data['css_js'] 	= asset_url('dashboard');
			//echo $css_js;
            $this->load->view('layout/index', $data);


		} else {

			$data['current_link'] = uri_string();
			
			$this->load->view('template/login', $data); 	
			/*	
			$data["content"]    = 'user/login';
            $data["isActive"]   = 'login';
			$data['css_js'] = asset_url('login');
			*/
			//echo $css_js;
            //$this->load->view('layout/index', $data);
		}
		
	}

	function info(){
		$tgl_triwulan = $this->gm->get_data_qry("select * from triwulan where is_active = '1'")->row('tgl_triwulan');
		$data['on_going'] = $this->gm->get_data_qry("select count(*) as jlh, sum(nilai_pinjaman_USD) as nilai, sum(realisasi_kum_USD) as realisasi from z_proyek_rekap where pv >= '1' and (status != 'closed' or status != 'canceled')")->row();
		$data['behind'] = $this->gm->get_data_qry("select count(*) as jlh, sum(nilai_pinjaman_USD) as nilai, sum(realisasi_kum_USD) as realisasi from z_proyek_rekap where (pv > '0.3' and pv < '1') and (status != 'closed' or status != 'canceled')")->row();
		$data['risk'] = $this->gm->get_data_qry("select count(*) as jlh, sum(nilai_pinjaman_USD) as nilai, sum(realisasi_kum_USD) as realisasi from z_proyek_rekap where pv <='0.3' and (status != 'closed' or status != 'canceled')")->row();
		$data['closed'] = $this->gm->get_data_qry("select count(*) as jlh, sum(nilai_pinjaman_USD) as nilai, sum(realisasi_kum_USD) as realisasi from z_proyek_rekap where status = 'closed'")->row();
		$data['canceled'] = $this->gm->get_data_qry("select count(*) as jlh, sum(nilai_pinjaman_USD) as nilai, sum(realisasi_kum_USD) as realisasi from z_proyek_rekap where status = 'canceled'")->row();
		
		$this->load->view('dashboard/info', $data);

	}
	
	function grafik()
	{
		$query="select 
					group_concat(realisasi) as penyerapan
				from z_paket_realisasi_per_tahun
				";
		$data['penyerapan'] = $this->gm->get_data_qry($query)->row('penyerapan');

		$query="select 
					sektor, 
					sum(nilai) as nilai
				from 
					z_proyek
				where is_active = 1
				group by sektor
				";
		$data['sektor'] = $this->gm->get_data_qry($query);

		$query="select 
					mitra, 
					sum(nilai) as nilai
				from 
					z_proyek
				where is_active = 1
				group by mitra
				";
		$data['mitra'] = $this->gm->get_data_qry($query);
		$this->load->view('dashboard/grafik', $data);
	}

	function login()
	{
		$data['current_link'] = uri_string();
		$this->load->view('template/login', $data);
	}

	function do_login()
	{

		$this->auth->restrict(true);
		$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean');
		
		$current_link = $this->input->post('current_link');

		if ($this->form_validation->run() == FALSE)
		{
			echo warning('Maaf, validasi gagal','');
		}
		else
		{
			$login = array(
							'email'		=> $this->input->post('email'),
							'password'	=> MD5($this->input->post('password'))
						);
						
			if($this->auth->do_login($login))
			{
				//echo goToPage('awal/index');
				echo goToPage('./'.$current_link);
			}
			else
			{
				//echo warning('Maaf, user name dan password yang Anda masukkan salah!','#');
				echo goToPage('./'.$current_link);
			}
		}
	}
	
	function logout()
	{
		$this->auth->logout();
		echo goToPage('sign-in.html');
        
	}

	function register()
	{
		$this->load->view('layout/register');
	}

	function save_register()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("email", "Email", "trim|required|xss_clean|valid_email|is_unique[user.email]");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		$this->form_validation->set_rules("password_conf", "Password Confirmation", "trim|required|matches[password]|min_length[5]");
		$this->form_validation->set_rules("nama_depan", "Nama Depan", "trim|required");
		$this->form_validation->set_rules("nama_belakang", "Nama Belakang", "trim|required");
		$this->form_validation->set_rules("no_hp", "No. Handphone", "trim|required");
		
        //$this->form_validation->set_rules("file", "File", "required");
        $this->form_validation->set_message('is_unique', 'This %s already exists.');
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			$data 				= $_POST;
			$result				= $this->gm->save_data('user', $data);
			$status['success']  = true;

		}
		echo json_encode($status);
	}

	function sendEmail()
    {
		$to_email = 'f.manangin@gmail.com';
		$from_email = 'no-reply@mudscode.com'; //change this to yours
        $subject = 'Verify Your Email Address';
        $message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br /> http://www.mydomain.com/user/verify/' . md5($to_email) . '<br /><br /><br />Thanks<br />Mydomain Team';
        
        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://mail.mudscode.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'Bogor123'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
		
		//$this->email->initialize($config);
		$this->load->library('email', $config);  
        
        //send mail
        $this->email->from($from_email, 'Team Monev PHLN');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
}
