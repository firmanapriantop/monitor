<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        $this->load->model(array('general_model' => 'gm', 'proyek_model'));
	}
	
	public function index()
	{
		if(is_logged_in()) {

			$data["content"]    = 'proyek/index';
            $data["isActive"]   = 'proyek';
            $data['css_js'] 	= asset_url('proyek');
			
			$query = "	SELECT 
							CONCAT(tahun, tw) as tahun_tw, 
							CONCAT('Triwulan ', tw, ' Tahun ', tahun) as full_tw, 
							tgl, 
							id 
						FROM 
							triwulan 
						WHERE 
							is_active = 1";

			$baris = $this->gm->get_data_qry($query);
			
			$data['tahun_tw'] 	= $baris->row('tahun_tw');
			$data['full_tw'] 	= $baris->row('full_tw');
			$data['tgl_tw']		= $baris->row('tgl');
			$data['id_tw']		= $baris->row('id');
 
            $this->load->view('layout/index', $data);
			

		}else{

			$data['current_link'] = uri_string();

			$this->load->view('template/login', $data); 		
		}

    }

    function load_data()
	{
		//cek user berdasarkan rolenya 

		//$tw 			= $this->uri->segment(2);
		$id_triwulan 	= $this->uri->segment(2);

		//get tahun triwulan
		$data['tahun_tw']  = $this->gm->get_data_qry("SELECT tahun FROM triwulan WHERE id = '$id_triwulan'")->row('tahun');

		$tgl_triwulan = $this->gm->get_data_qry("select tgl from triwulan where is_active = 1")->row('tgl');

		$query = "	SELECT 
						*, 
						(((tgl_triwulan - tgl_efektif_riil) / (tgl_tutup_actual - tgl_efektif_riil))*100) as waktu_terpakai1,
						((datediff(tgl_triwulan, tgl_efektif_riil) / datediff(tgl_tutup_actual, tgl_efektif_riil))*100) as waktu_terpakai,
						(nilai_kurs * nilai_ASLI) AS nilai_pinjaman_USD, 
						(nilai_kurs * realisasi_kum_ASLI) AS realisasi_kum_USD, 
						((realisasi_kum_ASLI / nilai_ASLI) * 100) AS persen_kum, 
						(nilai_target_rp / kurs_USD) AS nilai_target_USD, 
						(nilai_kurs * realisasi_thn_ASLI) AS realisasi_thn_USD, 
						(((realisasi_kum_ASLI / nilai_ASLI) * 100)) AS pv 
				FROM z_proyek
				";
		
		$data['data'] 	= $this->gm->get_data_qry($query);

		//send id_tw 
		$data['id_tw'] = $id_triwulan;
	
		$this->load->view('proyek/list', $data);
		
	}
	
	function get_tw()
	{
		$id_triwulan = $this->uri->segment(3);
		$result = $this->gm->get_data_qry("SELECT * FROM triwulan WHERE id = '$id_triwulan'")->row();
		if($result->is_active == 1){
			echo "aa";
		}
	}

	function add_data()
    {
		$data['mitra']   	= $this->gm->get_data_qry("SELECT * FROM mitra WHERE is_active = 1");
		$data['instansi']   = $this->gm->get_data_qry("SELECT * FROM instansi WHERE is_active = 1 AND id_parent = 0");
		$data['sektor']    	= $this->gm->get_data_qry("SELECT * FROM sektor WHERE is_active = 1");
		$data['mata_uang']	= $this->gm->get_data_qry("SELECT * FROM mata_uang WHERE is_active = 1");

		$data['tahun_tw']  	= $this->uri->segment(2);
		$data['id_tw']		= $this->uri->segment(3);

		$this->load->view('proyek/add', $data);
	}
	
	function save_data()
    {
		$status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("jenis", "Jenis Proyek", "trim|required");
		$this->form_validation->set_rules("id_instansi", "Instansi Penanggung Jawab", "trim|required");
		$this->form_validation->set_rules("id_mitra", "Mitra Pembangunan", "trim|required");
		$this->form_validation->set_rules("id_sektor", "Sektor/Bidang", "trim|required");
		$this->form_validation->set_rules("kode", "Kode Pinjaman/Hibah Luar Negeri", "trim|required");
		$this->form_validation->set_rules("no_npphln", "No. NPPHLN", "trim|required");
		$this->form_validation->set_rules("no_register", "No. Register", "trim|required");
		$this->form_validation->set_rules("nama_en", "Judul Proyek (EN)", "trim|required");
		$this->form_validation->set_rules("nama_id", "Judul Proyek (ID)", "trim|required");
		$this->form_validation->set_rules("tgl_ttd", "Tanggal Penandatanganan NPPHLN", "trim|required");
		$this->form_validation->set_rules("tgl_efektif_tentatif", "Tanggal Efektif Tentatif", "trim|required");
		$this->form_validation->set_rules("tgl_efektif_riil", "Tanggal Efektif Riil", "trim|required");
		$this->form_validation->set_rules("tgl_tutup_original", "Tanggal Tutup (Original)", "trim|required");
		$this->form_validation->set_rules("tgl_tutup_actual", "Tanggal Tutup (Actual)", "trim|required");
		$this->form_validation->set_rules("id_mata_uang", "Mata Uang", "trim|required");
		$this->form_validation->set_rules("nilai", "Nilai Pinjaman", "trim|required|numeric");
		$this->form_validation->set_rules("instansi_pelaksana", "Instansi Pelaksana", "trim|required");
		$this->form_validation->set_rules("ruang_lingkup", "Ruang Lingkup", "trim|required");
		$this->form_validation->set_rules("tujuan", "Tujuan", "trim|required");
		$this->form_validation->set_rules("sasaran", "Sasaran", "trim|required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		//$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			//cek kode proyek -> unique
			if (empty($this->input->post('id'))) {

				//cek kode tidak boleh sama
				$kode = $this->input->post('kode');
				$query = "SELECT * FROM proyek WHERE kode = '$kode'";
				$baris = $this->db->query($query)->num_rows();
				if ($baris > 0) {
					$status['messages']['kode'] = '<p class="text-danger">Kode ini sudah terdaftar.</p>';
				} else {
					$data 						= $_POST;

					$data['parent_id'] 			= strtotime(date("Y-m-d h:i:sa"));
					$data['is_ext']				= 0;
					$data['status']				= "pending-add";
					$data['status_act']			= "add.new";
					$data['submit_operator']	= "0";
					$data['verify_bila_multi']	= "0";
					$data['verify_sisdur']		= "0";
					$data['is_active']			= "1";
					$data['input_by']			= $this->session->userdata('id');
					$data['input_date']			= date('Y-m-d H:i:s');

					$result				= $this->gm->save_data('proyek', $data);
					$status['success']  = true;
				}
				
			}else{

				$data = $_POST;
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');
				$result = $this->gm->save_data('proyek', $data);
				$status['success']  = true;
				
			}	
			
		}
		
		echo json_encode($status);
	
	} 
	
	function edit_data()
	{
		
		$id_triwulan	= $this->uri->segment(2); 
		$id_proyek 		= $this->uri->segment(3);
		$sql			= "SELECT * FROM z_proyek WHERE id = '$id_proyek'";

		$qry    		= $this->gm->get_data_qry($sql);
		$data['data']	= $qry->row();

		$data['mitra']   	= $this->gm->get_data_qry("SELECT * FROM mitra WHERE is_active = 1");
		$data['instansi']   = $this->gm->get_data_qry("SELECT * FROM instansi WHERE is_active = 1 AND id_parent = 0");
		$data['sektor']    	= $this->gm->get_data_qry("SELECT * FROM sektor WHERE is_active = 1");
		$data['mata_uang']	= $this->gm->get_data_qry("SELECT * FROM mata_uang WHERE is_active = 1");

		//$data['tahun_tw']  	= $this->uri->segment(3);
		$data['id_tw']		= $this->uri->segment(2);

		$this->load->view('proyek/edit', $data);
	}

	function delete_data()
	{	
		/*
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('proyek', $id);
		*/
		

	}

	function detail_data()
	{
		$parent_id = $this->uri->segment(2);
		$query=
			"SELECT 
				*
			FROM 
				z_proyek
			WHERE
				parent_id = '$parent_id'
			";
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id;
		$this->load->view('proyek/detail', $data);
	}

	function profil()
	{
		$parent_id = $this->uri->segment(2);
		$query=
			"SELECT 
				*
			FROM 
				z_proyek
			WHERE
				parent_id = '$parent_id'
			";
		$data['proyek'] = $this->gm->get_data_qry($query)->row();
		$this->load->view('proyek/profil', $data);
	}

	//-------- Kategori PHLN---
	//-------------------------
	//----------------------

	function kat()
	{
		$parent_id_proyek = $this->uri->segment(2);
		$query = 
			"SELECT
				k.*,
				p.mata_uang AS mata_uang,
				(SELECT SUM(nilai) FROM kat_instansi
				WHERE id_kat = k.id) AS nilai_instansi,
				(SELECT SUM(nilai) FROM kat_wilayah
				WHERE id_kat = k.id) AS nilai_wilayah
			FROM 
				kat k
			LEFT JOIN 
				z_proyek p ON p.parent_id = k.parent_id_proyek
			WHERE
				k.parent_id_proyek = '$parent_id_proyek'
			";
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/kat', $data);
	}

	function kat_add()
    {
		$data['parent_id_proyek'] = $this->uri->segment(2);
		$this->load->view('proyek/kat/add', $data);
	}

	function kat_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("nama", "Nama", "trim|required");
		$this->form_validation->set_rules("nilai", "Nilai", "trim|required|numeric");
		//$this->form_validation->set_rules("ket", "Keterangan", "trim|required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data 						= $_POST;
				$data['status'] = 'add';
				$data['input_by']			= $this->session->userdata('id');
				$data['input_date']			= date('Y-m-d H:i:s');


				$result				= $this->gm->save_data('kat', $data);
				$status['success']  = true;
				
			}else{

				$data 				= $_POST;

				$data['status'] = 'edit';
				$data['input_by']	= $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result				= $this->gm->save_data('kat', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function kat_edit()
	{
		$id = $this->uri->segment(2);
		$query = 
			"SELECT
				k.*,
				p.mata_uang AS mata_uang
			FROM 
				kat k
			LEFT JOIN 
				z_proyek p ON p.parent_id = k.parent_id_proyek
			WHERE
				k.id = '$id'
			";
		$qry = $this->gm->get_data_qry($query);
		$data['data'] = $qry->row();
		$this->load->view('proyek/kat/edit', $data);
	}

	function kat_delete()
	{	
		$status = array('success' => false, 'messages' => array());


		$id = $this->uri->segment(2);
		//cek id di instansi atau lokasi
		$query = "SELECT * FROM kat_instansi WHERE id_kat = '$id'";
		$instansi = $this->db->query($query)->num_rows();
	

		$query = "SELECT * FROM kat_wilayah WHERE id_kat = '$id'";
		$wilayah = $this->db->query($query)->num_rows();

		if (($instansi > 0) OR ($wilayah > 0)) {
			$status['messages'] = 'Error! Kategori ini masih tercantum pada Kategori per Instansi Pelaksana dan/atau Kategori per Lokasi';

		} else {

			$query	= $this->gm->delete_data('kat', $id);
			$status['success']  = true;
			
		}

		echo json_encode($status);
	}

	function kat_instansi()
	{
		$parent_id_proyek = $this->uri->segment(2); 
		$query = "SELECT 
						k.*,
						a.nama AS kat,
						a.parent_id_proyek AS parent_id_proyek,
						p.mata_uang AS mata_uang
					FROM
						kat_instansi k
					LEFT JOIN
						kat a ON a.id = k.id_kat
					LEFT JOIN
						z_proyek p ON p.parent_id = a.parent_id_proyek
					WHERE
						a.parent_id_proyek = '$parent_id_proyek'
				";
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/kat_instansi', $data);
	}

	function kat_instansi_add()
    {
		$parent_id_proyek = $this->uri->segment(2);
		$query = "SELECT 
					* 
				FROM 
					kat 
				WHERE 
					parent_id_proyek = '$parent_id_proyek'";
		$data['kat'] = $this->gm->get_data_qry($query);
		$query = "SELECT 
					p.mata_uang AS mata_uang 
				FROM 
					kat k
				LEFT JOIN 
					z_proyek p ON p.parent_id = k.parent_id_proyek
				WHERE 
					p.parent_id = '$parent_id_proyek'";
		$data['mata_uang'] = $this->gm->get_data_qry($query)->row('mata_uang');
		$data['parent_id_proyek'] = $parent_id_proyek;

		$this->load->view('proyek/kat_instansi/add', $data);
	}

	function kat_instansi_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('id_kat', 'Kategori', 'required');
		$this->form_validation->set_rules('instansi', 'Instansi', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai Alokasi', 'required|numeric');
		//$this->form_validation->set_rules('ket', 'Keterangan', 'trim|required');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data = $_POST;

				$data['status']= 'add';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');

				$result = $this->gm->save_data('kat_instansi', $data);
				$status['success']  = true;
				
			}else{

				$data = $_POST;

				$data['status']= 'edit';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result = $this->gm->save_data('kat_instansi', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function kat_instansi_edit()
	{
		$id = $this->uri->segment(2);
		$sql = 
			"SELECT 
				a.* ,
				b.nama AS kat,
				c.mata_uang AS mata_uang,
				c.id AS id_proyek
			FROM 
				kat_instansi a 
			LEFT JOIN
				kat b ON b.id = a.id_kat
			LEFT JOIN
				z_proyek c ON c.parent_id = b.parent_id_proyek
			WHERE 
				a.id = '$id'
			";
		$qry = $this->gm->get_data_qry($sql);
		$data['data'] = $qry->row();
		$parent_id_proyek = $qry->row('parent_id_proyek');
		$query = 
			"SELECT 
				* 
			FROM 
				kat 
			WHERE 
				parent_id_proyek = '$parent_id_proyek'
			";
		$data['kat'] = $this->gm->get_data_qry($query);	
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/kat_instansi/edit', $data);
	}

	function kat_instansi_delete()
	{
		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('kat_instansi', $id);

		$status = array('success' => true);
		echo json_encode($status);
	}

	// ------------------------------------- KAT WILAYAH
	// ------------------------------------------------------
	function kat_wilayah()
	{
		$parent_id_proyek = $this->uri->segment(2); 
		$query = "SELECT 
						k.*,
						a.nama AS kat,
						a.parent_id_proyek AS parent_id_proyek,
						p.mata_uang AS mata_uang,
						CONCAT(w.nama, ', ', w1.nama) AS nama_wilayah,
                        w1.nama AS provinsi,
                        w.nama AS kabupaten
					FROM
						kat_wilayah k
					LEFT JOIN
						kat a ON a.id = k.id_kat
					LEFT JOIN
						wilayah w ON w.id = k.id_wilayah
					LEFT JOIN
						wilayah w1 ON w1.id = w.id_parent AND w1.level = 'provinsi'
					LEFT JOIN
						z_proyek p ON p.parent_id = a.parent_id_proyek
					WHERE
						a.parent_id_proyek = '$parent_id_proyek'
				";
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/kat_wilayah', $data);
	}

	function kat_wilayah_add()
    {
		$parent_id_proyek = $this->uri->segment(2);
		$query = "SELECT 
					* 
				FROM 
					kat 
				WHERE 
				parent_id_proyek = '$parent_id_proyek'";
		$data['kat'] = $this->gm->get_data_qry($query);
		$query = "SELECT 
					p.mata_uang AS mata_uang 
				FROM 
					kat_wilayah kw
				LEFT JOIN
					kat k ON k.id = kw.id_kat
				LEFT JOIN 
					z_proyek p ON p.parent_id = k.parent_id_proyek
				WHERE 
					k.parent_id_proyek = '$parent_id_proyek'";
		$data['mata_uang'] = $this->gm->get_data_qry($query)->row('mata_uang');
		$query=
			"SELECT 
				*
			FROM 
				wilayah
			WHERE 
				level = 'kabupaten'
			";
		$data['wilayah'] = $this->gm->get_data_qry($query);
		
		$data['parent_id_proyek'] = $parent_id_proyek;

		$this->load->view('proyek/kat_wilayah/add', $data);
	}

	function kat_wilayah_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('id_kat', 'Kategori', 'required');
		$this->form_validation->set_rules('id_wilayah', 'Lokasi', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai Alokasi', 'required|numeric');
		//$this->form_validation->set_rules('ket', 'Keterangan', 'trim|required');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data = $_POST;

				$data['status'] = 'add';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');


				$result = $this->gm->save_data('kat_wilayah', $data);
				$status['success']  = true;
				
			}else{

				$data = $_POST;

				$data['status'] = 'edit';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result = $this->gm->save_data('kat_wilayah', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function kat_wilayah_edit()
	{
		$id = $this->uri->segment(2);
		$sql = 
			"SELECT 
				a.* ,
				b.nama AS kat,
				c.mata_uang AS mata_uang,
				w.nama AS wilayah,
				b.parent_id_proyek AS parent_id_proyek
			FROM 
				kat_wilayah a 
			LEFT JOIN
				wilayah w ON w.id = a.id_wilayah
			LEFT JOIN
				kat b ON b.id = a.id_kat
			LEFT JOIN
				z_proyek c ON c.parent_id = b.parent_id_proyek
			WHERE 
				a.id = '$id'
			";
		$data['data'] = $this->gm->get_data_qry($sql)->row();
		$parent_id_proyek = $this->gm->get_data_qry($sql)->row('parent_id_proyek');
		$query = "SELECT 
					* 
				FROM 
					kat 
				WHERE 
					parent_id_proyek = '$parent_id_proyek'";
		$data['kat'] = $this->gm->get_data_qry($query);	
		$query=
			"SELECT 
				*
			FROM 
				wilayah
			WHERE 
				level = 'kabupaten'
			";
		$data['wilayah'] = $this->gm->get_data_qry($query);

		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/kat_wilayah/edit', $data);
	}

	function kat_wilayah_delete()
	{
		$status = array('success' => true, 'messages' => '');

		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('kat_wilayah', $id);

		echo json_encode($status);
	}

	// ------------------------------------- Paket
	// -------------------------------------------
	// ---

	function paket()
	{
		$parent_id_proyek = $this->uri->segment(2);
		$query	=	
			"SELECT
				pa.*,
				m.singkatan AS mata_uang,
				k.nama as kat,
				(SELECT SUM(uang) FROM paket_realisasi 
				WHERE parent_id_paket = pa.parent_id) AS penyerapan_kum,
				(SELECT SUM(nilai) FROM paket_target 
				WHERE parent_id_paket = pa.parent_id) AS target,
                (((SELECT SUM(uang) FROM paket_realisasi 
				WHERE parent_id_paket = pa.parent_id) / pa.nilai)*100) AS persen
			FROM
				paket pa
			LEFT JOIN
				proyek p ON p.parent_id = pa.parent_id_proyek
			LEFT JOIN kat k ON k.id = pa.id_kat
			LEFT JOIN
				mata_uang m ON m.id = p.id_mata_uang 
			WHERE pa.parent_id_proyek = '$parent_id_proyek' 
			group by pa.id, m.id
			";
		$data['data'] = $this->gm->get_data_qry($query);
		
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/paket', $data);
	}

	function paket_add()
    {
		$parent_id_proyek = $this->uri->segment(2);
		$query = "SELECT 
					ma.singkatan AS mata_uang 
				FROM 
					proyek p
				LEFT JOIN 
					mata_uang ma ON ma.id = p.id_mata_uang
				WHERE 
					p.parent_id = '$parent_id_proyek'";
		$data['mata_uang'] = $this->gm->get_data_qry($query)->row('mata_uang');
		$data['kat'] = $this->gm->get_data_qry("SELECT * FROM kat WHERE parent_id_proyek = '$parent_id_proyek'");
		$data['parent_id_proyek'] = $parent_id_proyek;

		$this->load->view('proyek/paket/add', $data);
	}

	function paket_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('parent_id_proyek', 'parent_id_proyek', 'required');
		$this->form_validation->set_rules('nama', 'Nama Kontrak', 'required');
		$this->form_validation->set_rules('no', 'Nomor Kontrak', 'required');
		$this->form_validation->set_rules('tgl', 'Tanggal Kontrak', 'required');
		$this->form_validation->set_rules('tgl_setuju_peminjam', 'Tanggal Persetujuan Peminjam', 'required');
		$this->form_validation->set_rules('periode', 'Periode Kontrak', 'required');
		$this->form_validation->set_rules('tgl_akhir', 'Tanggal Akhir Kontrak', 'required');
		$this->form_validation->set_rules('kontraktor', 'Nama Kontraktor', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai Alokasi', 'required|numeric');
		$this->form_validation->set_rules('status_terakhir', 'Status Terakhir', 'required');
		$this->form_validation->set_rules('id_kat', 'Kategori', 'required');
		//$this->form_validation->set_rules('ket', 'Keterangan', 'trim|required');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data = $_POST;
				$data['parent_id'] = strtotime(date("Y-m-d h:i:sa")); 
				$data['status'] = 'add';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');


				$result = $this->gm->save_data('paket', $data);
				$status['success']  = true;
				
			}else{

				$data = $_POST;
				$data['status'] = 'edit';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result = $this->gm->save_data('paket', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function paket_edit()
	{
		$id = $this->uri->segment(2);
		$query=
			"SELECT
				pa.*,
				m.singkatan AS mata_uang
			FROM
				paket pa
			LEFT JOIN
				proyek p ON p.parent_id = pa.parent_id_proyek
			LEFT JOIN
				mata_uang m ON m.id = p.id_mata_uang
			WHERE 
				pa.id = '$id'
			";
		/*$sql = 
			"SELECT 
				a.* ,
				b.nama AS kat,
				c.mata_uang AS mata_uang,
				c.id AS id_proyek
			FROM 
				kat_instansi a 
			LEFT JOIN
				kat b ON b.id = a.id_kat
			LEFT JOIN
				z_proyek c ON c.id = b.id_proyek
			WHERE a.id = '$id'";
		*/
		$qry = $this->gm->get_data_qry($query);
		$data['data'] = $qry->row();
		$parent_id_proyek = $qry->row('parent_id_proyek');
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/paket/edit', $data);
	}

	function paket_delete()
	{
		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('paket', $id);
		$status['success'] = true;
		echo json_encode($status);
	}

	// ------------------------------------- Paket Target
	// --------------------------
	//-------------------
	//----------------------
	//---------
	//--------
	function paket_target_realisasi()
	{
		$parent_id_proyek = $this->uri->segment(2);
		$query = 
			"SELECT * 
			FROM 
				paket
			WHERE 
				parent_id_proyek = '$parent_id_proyek'
			";

		$data['paket'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/paket_target_realisasi', $data);

	}

	//------------------------------------
	//------------------------------------
	//------------------------------------

	function paket_target()
	{
		$parent_id_paket = $this->uri->segment(2);
		$query	=	
			"SELECT
				a.*,
				d.singkatan AS mata_uang,
				(select n.nilai from nilai_kurs n where n.id_triwulan = a.id_triwulan and n.id_mata_uang = c.id_mata_uang) as nilai_kurs,
				CONCAT('Triwulan ', t.tw, ' Tahun ', t.tahun) AS tw
			FROM
				paket_target a
			LEFT JOIN
				paket b ON b.parent_id = a.parent_id_paket
			LEFT JOIN
				triwulan t ON t.id = a.id_triwulan
			LEFT JOIN proyek c ON c.id = b.id_proyek
			LEFT JOIN
				mata_uang d ON d.id = c.id_mata_uang
			WHERE 
				a.parent_id_paket = '$parent_id_paket'
			ORDER BY
				t.tahun, t.tw DESC
			";
		$data['data'] = $this->gm->get_data_qry($query);
		
		//$data['id_proyek'] = $id_proyek;
		$data['parent_id_paket'] = $parent_id_paket;
		$this->load->view('proyek/paket_target', $data);
	}

	function paket_target_add()
    {
		$parent_id_paket = $this->uri->segment(2);
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan
			ORDER BY
				tahun DESC, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$query = "SELECT 
					ma.singkatan AS mata_uang,
					a.nama AS paket
				FROM 
					paket a
				LEFT JOIN
					proyek p ON p.parent_id = a.parent_id_proyek
				LEFT JOIN 
					mata_uang ma ON ma.id = p.id_mata_uang
				WHERE 
					a.parent_id = '$parent_id_paket'
				";
			
		$data['mata_uang'] = $this->gm->get_data_qry($query)->row('mata_uang');
		$data['parent_id_paket'] = $parent_id_paket;

		$this->load->view('proyek/paket_target/add', $data);
	}

	function paket_target_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('parent_id_paket', 'Paket', 'required');
		$this->form_validation->set_rules('id_triwulan', 'Triwulan', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai Target', 'required|numeric');
		//$this->form_validation->set_rules('ket', 'Keterangan', 'trim|required');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){

				//check tw is exist
				$id_triwulan = $this->input->post('id_triwulan');
				$parent_id_paket = $this->input->post('parent_id_paket');
				$query = "select * from paket_target where id_triwulan = '$id_triwulan' and parent_id_paket = '$parent_id_paket'";
				$hasil = $this->gm->get_data_qry($query)->num_rows();
				if ( $hasil != 0 ) {
					$status['messages']['id_triwulan'] = '<p class="text-danger">Triwulan ini sudah ada</p>';
				} else {
			
					$data = $_POST;
					$data['status'] = 'add';
					$data['input_by'] = $this->session->userdata('id');
					$data['input_date'] = date('Y-m-d H:i:s');

					$result = $this->gm->save_data('paket_target', $data);
					$status['success']  = true;
				}
				
			}else{

					$data = $_POST;
					$data['status'] = 'edit';
					$data['input_by'] = $this->session->userdata('id');
					$data['input_date']	= date('Y-m-d H:i:s');
					$result = $this->gm->save_data('paket_target', $data);
					$status['success']  = true;
			
				

			}	
		}
		echo json_encode($status);
	} 

	function paket_target_edit()
	{
		$id = $this->uri->segment(2);
		$query=
			"SELECT
				pt.*,
				m.singkatan AS mata_uang,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS tw 
			FROM
				paket_target pt 
			LEFT JOIN
				triwulan t ON t.id = pt.id_triwulan
			LEFT JOIN
				paket pa ON pa.parent_id = pt.parent_id_paket
			LEFT JOIN
				proyek p ON p.parent_id = pa.parent_id_proyek
			LEFT JOIN
				mata_uang m ON m.id = p.id_mata_uang
			WHERE 
				pt.id = '$id'
			";
		$data['data']  = $this->gm->get_data_qry($query)->row();
		$data['parent_id_paket'] = $this->gm->get_data_qry($query)->row('parent_id_paket');
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan 
			WHERE 
				is_active = 1
			ORDER BY
				tahun, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$this->load->view('proyek/paket_target/edit', $data);
	}

	function paket_target_delete()
	{
		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('paket_target', $id);
		$status['success']  = true;
		echo json_encode($status);
	}



	//------------------------------------
	//------------------------------------
	//------------------------------------
	//-- Paket Realisasi

	function paket_realisasi()
	{
		$parent_id_paket = $this->uri->segment(2);
		$query	=	
			"SELECT
				a.*,
				d.singkatan AS mata_uang,
				CONCAT('Triwulan ', t.tw, ' Tahun ', t.tahun) AS tw
			FROM
				paket_realisasi a
			LEFT JOIN
				paket b ON b.parent_id = a.parent_id_paket
			LEFT JOIN
				triwulan t ON t.id = a.id_triwulan
			LEFT JOIN
				proyek c ON c.parent_id = b.parent_id_proyek
			LEFT JOIN
				mata_uang d ON d.id = c.id_mata_uang
			WHERE 
				a.parent_id_paket = '$parent_id_paket'
			ORDER BY
				t.tahun, t.tw DESC
			";
		$data['data'] = $this->gm->get_data_qry($query);
		
		//$data['id_proyek'] = $id_proyek;
		$data['parent_id_paket'] = $parent_id_paket;
		$this->load->view('proyek/paket_realisasi', $data);
	}

	function paket_realisasi_add()
    {
		$parent_id_paket = $this->uri->segment(2);
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan 
			ORDER BY
				tahun DESC, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$query = "SELECT 
					ma.singkatan AS mata_uang,
					a.nama AS paket
				FROM 
					paket a
				LEFT JOIN
					proyek p ON p.parent_id = a.parent_id_proyek
				LEFT JOIN 
					mata_uang ma ON ma.id = p.id_mata_uang
				WHERE 
					a.parent_id = '$parent_id_paket'
				";
			
		$data['mata_uang'] = $this->gm->get_data_qry($query)->row('mata_uang');
		$data['parent_id_paket'] = $parent_id_paket;

		$this->load->view('proyek/paket_realisasi/add', $data);
	}

	function paket_realisasi_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('parent_id_paket', 'Paket', 'required');
		$this->form_validation->set_rules('id_triwulan', 'Triwulan', 'required');
		$this->form_validation->set_rules('uang', 'Realisasi Keuangan', 'required|numeric');
		$this->form_validation->set_rules('fisik', 'Progres Fisik', 'required|numeric');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data = $_POST;
				$data['status'] = 'add';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');


				$result = $this->gm->save_data('paket_realisasi', $data);
				$status['success']  = true;
				
			}else{

				$data = $_POST;
				$data['status'] = 'edit';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result = $this->gm->save_data('paket_realisasi', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function paket_realisasi_edit()
	{
		$id = $this->uri->segment(2);
		$query=
			"SELECT
				pr.*,
				m.singkatan AS mata_uang,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS tw 
			FROM
				paket_realisasi pr
			LEFT JOIN
				triwulan t ON t.id = pr.id_triwulan
			LEFT JOIN
				paket pa ON pa.parent_id = pr.parent_id_paket
			LEFT JOIN
				proyek p ON p.parent_id = pa.parent_id_proyek
			LEFT JOIN
				mata_uang m ON m.id = p.id_mata_uang
			WHERE 
			pr.id = '$id'
			";
		$data['data']  = $this->gm->get_data_qry($query)->row();
		$data['parent_id_paket'] = $this->gm->get_data_qry($query)->row('parent_id_paket');
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan 
			ORDER BY
				tahun DESC, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$this->load->view('proyek/paket_realisasi/edit', $data);
	}

	function paket_realisasi_delete()
	{
		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('paket_realisasi', $id);
		$status['success']  = true;
		echo json_encode($status);
	}

//------------------------------------
	//------------------------------------
	//------------------------------------
	//-- Paket Masalah

	function masalah()
	{
		$parent_id_proyek = $this->uri->segment(2);
		$query=
			"
				select 
					m.*,
					(select concat('TW ', t.tw, ' Tahun ', t.tahun) from triwulan t where t.id = m.id_triwulan_mulai) as tw_mulai,
					(select concat('TW', t.tw, ' Tahun ', t.tahun) from triwulan t where t.id = m.id_triwulan_selesai) as tw_selesai,
					mk.nama as kat_masalah
				from 
					masalah m
				left join masalah_kat mk on mk.id = m.id_masalah_kat
				where m.parent_id_proyek = '$parent_id_proyek'
			";
		
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $parent_id_proyek;
		$this->load->view('proyek/masalah', $data);
	}

	function masalah_add()
    {
		$parent_id_proyek = $this->uri->segment(2);
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan 
			ORDER BY
				tahun, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$data['kat_masalah'] = $this->gm->get_data_qry("SELECT * FROM masalah_kat WHERE is_active = '1'");
		
		$data['parent_id_proyek'] = $parent_id_proyek;

		$this->load->view('proyek/masalah/add', $data);
	}

	function masalah_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('parent_id_proyek', 'Proyek', 'required');
		$this->form_validation->set_rules('id_triwulan_mulai', 'Triwulan', 'required');
		$this->form_validation->set_rules('id_masalah_kat', 'Kategori masalah', 'required|numeric');
		$this->form_validation->set_rules('masalah', 'Masalah', 'required');
		$this->form_validation->set_rules('tindak_lanjut', 'Tindak Lanjut', 'required');
		$this->form_validation->set_rules('progres', 'Progres ', 'required|numeric');
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			//cek kode proyek -> unique
			if(empty($this->input->post('id'))){
			
				$data = $_POST;
				$data['status'] = 'add';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date'] = date('Y-m-d H:i:s');

				$result = $this->gm->save_data('masalah', $data);
				$status['success']  = true;
				
			}else{

				$data = $_POST;
				$data['status'] = 'edit';
				$data['input_by'] = $this->session->userdata('id');
				$data['input_date']	= date('Y-m-d H:i:s');
				$result = $this->gm->save_data('masalah', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function masalah_edit()
	{
		$id = $this->uri->segment(2);
		$query=
			"select 
				m.*,
				(select concat('Triwulan ', t.tw, ' Tahun ', t.tahun) from triwulan t where t.id = m.id_triwulan_mulai) as tw_mulai,
				(select concat('Triwulan ', t.tw, ' Tahun ', t.tahun) from triwulan t where t.id = m.id_triwulan_selesai) as tw_selesai,
				mk.nama as kat_masalah
			from 
				masalah m
			left join masalah_kat mk on mk.id = m.id_masalah_kat
			where m.id = '$id'
			";
		$data['data']  = $this->gm->get_data_qry($query)->row();
		$query = 
			"SELECT 
				*,
				CONCAT('Triwulan ', tw, ' Tahun ', tahun) AS nama 
			FROM 
				triwulan 
			ORDER BY
				tahun, tw DESC
			";
		$data['triwulan'] = $this->gm->get_data_qry($query);
		$data['kat_masalah'] = $this->gm->get_data_qry("SELECT * FROM masalah_kat WHERE is_active = '1'");
		$this->load->view('proyek/masalah/edit', $data);
	}

	function masalah_delete()
	{
		$id = $this->uri->segment(2);
		$query	= $this->gm->delete_data('masalah', $id);
		$status['success']  = true;
		echo json_encode($status);
	}

	function realisasi()
	{
		$parent_id_proyek = $this->uri->segment(2);
		$query = 
			"SELECT 
				concat('Triwulan ', t.tw, ' Tahun ', t.tahun) as tw,
				sum(pr.uang) as uang,
				(
					SELECT
						SUM(pr1.uang)
					FROM
						paket pa1
					LEFT JOIN
						paket_realisasi pr1 ON pr1.parent_id_paket = pa1.parent_id
					WHERE 
						pa1.parent_id_proyek = pa.parent_id_proyek AND
						pr1.id_triwulan <= pr.id_triwulan
					
				) AS kum,
				p.nilai AS nilai,
				(
					SELECT
						SUM(pt.nilai)
					FROM
						paket_target pt 
					LEFT JOIN
						paket pa1 ON pa1.parent_id = pt.parent_id_paket
					WHERE
						pt.parent_id_paket = pa.parent_id AND
						pt.id_triwulan <= pr.id_triwulan
				) AS target_nilai
			FROM 
				paket pa
			LEFT JOIN
				paket_realisasi pr ON pr.parent_id_paket = pa.parent_id 
			LEFT JOIN
				proyek p ON p.parent_id = pa.parent_id_proyek
			LEFT JOIN 
				triwulan t ON t.id = pr.id_triwulan
			WHERE 
				pa.parent_id_proyek = '$parent_id_proyek'
			GROUP BY	
					pr.id_triwulan, pa.parent_id_proyek
			ORDER BY 
					pr.id_triwulan DESC
			";
		$data['data'] = $this->gm->get_data_qry($query);
		$data['parent_id_proyek'] = $this->uri->segment(2);
		$this->load->view('proyek/realisasi', $data);
	}






























































































































































	function submit_data(){

		$id 		= $this->uri->segment(2);
		$jenis		= $this->uri->segment(3); //usulan or realisasi
		$sub_jenis	= $this->uri->segment(4); //add/edit/extend

		$id_user	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		switch ($jenis) {
			case "usulan":
				$status = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'")->row('status');
				switch ($sub_jenis) {
					case "add":
						$status 	= "submit-add";
						$status_act	= "submit.add.to.verify";
					break;

					case "edit":
						$status 			= "submit-edit";
						$status_act			= "submit.edit.to.verify";
						
					break;

					case "request-extend":
						$status 	= "req-ext";
						$status_act	= "request.to.extend";
					break;

					case "submit-extend":
						$status 	= "submit-request-extend";
						$status_act	= "submit.request.extend.to.verify";
					break;
				}

				$query = 
					"	UPDATE 
							proyek 
						SET 
							submit_operator = '1', 
							status = '$status', 
							status_act = '$status_act', 
							verify_bila_multi = '0', 
							verify_sisdur = '0', 
							input_by = '$id_user', 
							input_date= '$timestamp' 
						WHERE 
							id = '$id'
					";

				$result 	= $this->gm->get_data_qry($query);

			break;

			case "realisasi":
				$status = $this->gm->get_data_qry("SELECT * FROM v_proyek WHERE id = '$id'")->row('status');
				switch ($sub_jenis) {
					case "add":
						$status 	= "submit-add";
						$status_act	= "submit.add.to.verify";
					break;

					case "edit":
						$status 	= "submit-edit";
						$status_act	= "submit.edit.to.verify";
					break;

					case "extend":
						$status 	= "submit-extend";
						$status_act	= "submit.extend.to.verify";
					break;
				}

				$result 	= $this->gm->get_data_qry("UPDATE proyek SET submit_operator = '1', status = '$status', status_act = '$status_act',input_by = '$id_user', input_date= '$timestamp' WHERE id = '$id'");

			break;
		}

	}

	function verify_data(){
		$id_proyek	= $this->uri->segment(2);
		//$tahun_tw	= $this->uri->segment(3);
		$id_tw		= $this->uri->segment(3);
		$jenis		= $this->uri->segment(4); //usulan or realisasi etc
		$sub_jenis		= $this->uri->segment(5); //add or edit or extend
		$oleh 		= $this->uri->segment(6); //bila-multi atau sisdur

		$data['id_proyek']		= $id_proyek;
		$data['id_tw']			= $id_tw;
		$data['verified_by']	= $oleh;
		$data['jenis']			= $jenis;


		switch ($jenis){
			case "usulan":
				$qry = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id_proyek'");
				
				$data['status']				= $qry->row('status');
				$data['verify_bila_multi']	= $qry->row('verify_bila_multi');
				$data['verify_sisdur']		= $qry->row('verify_bila_multi');
				$data['link_to_refresh']	= base_url()."proyek-efektif-load/".$id_tw;

				switch ($oleh){
					case "verify-bila-multi":
						$data['judul']	= "Verifikasi Dit. Bilateral atau Dit. Multilateral";
						$data['verify']	= $qry->row('verify_bila_multi');
						$data['cat']	= $qry->row('cat_bila_multi');
					break;

					case "verify-sisdur":
						$data['judul']	= "Verifikasi Dit. Sisdur";
						$data['verify']	= $qry->row('verify_sisdur');
						$data['cat']	= $qry->row('cat_sisdur');
					break;
				}
			break;

			case "realisasi":
			break;
		}
		

		$this->load->view('proyek_efektif/verify', $data);
		
	}

	function save_verify_data(){
		$status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("verify", "Hasil Verifikasi", "trim|required");
		$this->form_validation->set_rules("cat", "Catatan", "trim|required");
        
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

	

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			$verified_by	= $this->input->post('verified_by'); //verifikasi bila-multi or sisdur
			$jenis			= $this->input->post('jenis'); //usulan or realisasi
			$sub_jenis		= $this->input->post('sub_jenis'); //usulan or realisasi

			$id = $this->input->post('id');

			switch ($jenis){
				case "usulan":
					$query = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id'");

					switch ($verified_by){
						case "verify-bila-multi":
							$data['verify_bila_multi']	= $this->input->post('verify');
							$data['cat_bila_multi']		= $this->input->post('cat');
							$data['status_act']			= "verified.by.bila-multi";

							//cek hasil verifikasi
							if($query->row('verify_sisdur') == "0" || $this->input->post('verify') == "0"){
								$data['status']		= "being-verified";
							}elseif ($query->row('verify_sisdur') == "1" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-del";
							}elseif ($query->row('verify_sisdur') == "1" && $this->input->post('verify') == "2"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_sisdur') == "2" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_sisdur') == "2" && $this->input->post('verify') == "2"){
								$data['status']		= "ready";
							}

						break;
						
						case "verify-sisdur":
							$data['verify_sisdur']	= $this->input->post('verify');
							$data['cat_sisdur']		= $this->input->post('cat');
							$data['status_act']		= "verified.by.sisdur";

							if($query->row('verify_bila_multi') == "0" || $this->input->post('verify') == "0"){
								$data['status']		= "being-verified";
							}elseif ($query->row('verify_bila_multi') == "1" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-del";
							}elseif ($query->row('verify_bila_multi') == "1" && $this->input->post('verify') == "2"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_bila_multi') == "2" && $this->input->post('verify') == "1"){
								$data['status']		= "recom-to-edit";
							}elseif ($query->row('verify_bila_multi') == "2" && $this->input->post('verify') == "2"){
								$data['status']		= "ready";
							}
						break;
					}

					$data['id']			= $this->input->post('id');
					$result				= $this->gm->save_data('proyek', $data);
					$status['success']  = true;
				break;
			}

			
				
		}
		echo json_encode($status);
	}

	function realisasi2()
	{
		//$id = $this->uri->segment(2);
		//$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		//$data['realisasi'] = $this->gm->get_data_qry("SELECT *, ((realisasi_kum_ASLI / nilai_ASLI) * 100) AS persen_kum, (((realisasi_kum_ASLI / nilai_ASLI) * 100) - waktu_terpakai) AS pv, (nilai_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, (nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, (nilai_target_rp / kurs_USD) AS nilai_target_USD, ((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  FROM v_proyek_realisasi WHERE kode = '$kode'");
		//$this->load->view('proyek_efektif/detail/realisasi');
		$id 				= $this->uri->segment(2); //id_proyek
		$data['id_proyek'] 	= $id;

		$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		$query = "SELECT *, 
						((realisasi_kum_ASLI / nilai_ASLI) * 100) AS persen_kum, 
						(((realisasi_kum_ASLI / nilai_ASLI) * 100) - waktu_terpakai) AS pv, 
						(nilai_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, 
						(nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, 
						(nilai_target_rp / kurs_USD) AS nilai_target_USD, 
						((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  
					FROM 
						v_proyek_realisasi 
					WHERE kode = '$kode'
				";
		$data['realisasi'] = $this->gm->get_data_qry($query);

		//get tw actie
		$data['id_tw']		= $this->gm->get_data_qry("SELECT id FROM triwulan WHERE is_active = '1'")->row('id');
		
		$this->load->view('proyek_efektif/detail/realisasi', $data);
	}

	function ext()
	{	
		$id     	= $this->uri->segment(2);

		$row = $this->gm->get_data_qry("SELECT * FROM proyek WHERE id = '$id'");
		$submit_operator = $row->row('submit_operator');
		$jlh_ext = $row->row('jlh_ext');
		
		if ($submit_operator == "1"){

			$status 	= "req-ext";
			$status_act = "request.to.extend";
			$input_by 	= $this->session->userdata('id');
			$timestamp 	= date('Y-m-d H:i:s');

			$query	= $this->gm->get_data_qry("UPDATE proyek SET status = '$status', status_act = '$status_act', submit_operator = '2', input_by = '$input_by', input_date= '$timestamp' WHERE id = '$id'");

		}elseif ($submit_operator == "2"){ //dizinkan untuk melakukan perpanjangan

			$status 	= "ext-edit";
			$status_act = "accept.to.extend";
			$input_by 	= $this->session->userdata('id');
			$timestamp 	= date('Y-m-d H:i:s');
			$jlh_ext 	= $jlh_ext+1;

			$query	= $this->gm->get_data_qry("UPDATE proyek SET status = '$status', status_act = '$status_act', is_active = '0', input_by = '$input_by',  input_date= '$timestamp' WHERE id = '$id'");

			$jenis			= $row->row('jenis');
			$id_instansi	= $row->row('id_instansi');
			$id_mitra		= $row->row('id_mitra');
			$id_sektor		= $row->row('id_sektor');
			$kode			= $row->row('kode');
			$nama_en		= $row->row('nama_en');
			$nama_id		= $row->row('nama_id');
			$tgl_mulai		= $row->row('tgl_mulai');
			$tgl_selesai	= $row->row('tgl_selesai');
			$id_mata_uang	= $row->row('id_mata_uang');
			$nilai_pinjaman	= $row->row('nilai_pinjaman');
			$instansi_pelaksana	= $row->row('instansi_pelaksana');
			$ruang_lingkup		= $row->row('ruang_lingkup');
			$cat_bila_multi		= $row->row('cat_bila_multi');
			$cat_sisdur			= $row->row('cat_sisdur');
			//create tambah baru
			$query = $this->gm->get_data_qry("INSERT INTO proyek (jenis, id_instansi, id_mitra, id_sektor, kode, nama_en, nama_id, tgl_mulai, tgl_selesai, id_mata_uang, nilai_pinjaman, jlh_ext, instansi_pelaksana, ruang_lingkup, status, status_act, is_active, submit_operator, verify_bila_multi, cat_bila_multi, verify_sisdur, cat_sisdur, input_by, input_date) VALUES ('$jenis', '$id_instansi', '$id_mitra', '$id_sektor', '$kode', '$nama_en', '$nama_id', '$tgl_mulai', '$tgl_selesai', '$id_mata_uang', '$nilai_pinjaman', '$jlh_ext', '$instansi_pelaksana', '$ruang_lingkup', '$status', '$status_act', '1', '1', '0', '$cat_bila_multi', '0', '$cat_sisdur', '$input_by', '$timestamp')");


		}
		
	}

	function realisasi_add()
	{
		$id 	= $this->uri->segment(2);
		$id_tw 	= $this->uri->segment(3);

		$query	= "	SELECT 
						CONCAT('Triwulan ', triwulan.tw, ' Tahun ', triwulan.tahun) AS tw,
						nilai_kurs.nilai AS nilai_kurs,
						nilai_kurs.id AS id_nilai_kurs,
						mata_uang.singkatan AS mata_uang
					FROM 
						nilai_kurs 
					INNER JOIN 
						proyek ON proyek.id_mata_uang = nilai_kurs.id_mata_uang 
					INNER JOIN
						triwulan ON triwulan.id	= nilai_kurs.id_triwulan
					INNER JOIN
						mata_uang ON mata_uang.id	= nilai_kurs.id_mata_uang
					WHERE 
						nilai_kurs.id_triwulan = '$id_tw' AND proyek.id = '$id'
					";

		$row	= $this->gm->get_data_qry($query);

		$data['nilai_kurs']		= $row->row('nilai_kurs');
		$data['tw']				= $row->row('tw');
		$data['mata_uang']		= $row->row('mata_uang');
		$data['id_nilai_kurs']	= $row->row('id_nilai_kurs');
		$data['id_triwulan']	= $id_tw;
		$data['id_proyek']		= $id;

		$this->load->view('proyek_efektif/realisasi/add', $data);
	}

	function realisasi_edit()
	{
		$id 		= $this->uri->segment(2); 
		$id_proyek 	= $this->uri->segment(3); 
		$id_tw 		= $this->uri->segment(4);

		$query 	= 
			"SELECT
				realisasi.*,
				CONCAT('Triwulan ', triwulan.tw, ' Tahun ', triwulan.tahun) AS tw,
				nilai_kurs.nilai AS nilai_kurs,
				mata_uang.singkatan AS mata_uang
			FROM
				realisasi
				INNER JOIN nilai_kurs ON nilai_kurs.id = realisasi.id_nilai_kurs
				INNER JOIN triwulan ON triwulan.id = realisasi.id_triwulan
				INNER JOIN mata_uang ON mata_uang.id = nilai_kurs.id_mata_uang
			WHERE
				realisasi.id = '$id'
			";

		$row	= $this->gm->get_data_qry($query);

		$data['data']	= $row->row();

		$nilai_kurs = $row->row('nilai_kurs');
		$realisasi_uang_asli = $row->row('realisasi_uang');
		$data['realisasi_uang_usd'] = $nilai_kurs * $realisasi_uang_asli;
		/*
		$realisasi_uang = $row->row('realisasi_uang');
		$nilai_kurs		= $row->row('nilai_kurs');

		$data['realisasi_uang_usd'] = $realisasi_uang * $nilai_kurs;
		$data['nilai_kurs']			= $row->row('nilai_kurs');
		$data['tw']					= $row->row('tw');
		$data['mata_uang']			= $row->row('mata_uang');
		$data['id_nilai_kurs']		= $row->row('id_nilai_kurs');
		$data['id_triwulan']		= $id_tw;
		$data['id_proyek']			= $id;
			*/
		$this->load->view('proyek_efektif/realisasi/edit', $data);


		/*

		$input_by 	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		if ($kat == "req-to-edit"){
			
			
			$query	= "	UPDATE 
							realisasi 
						SET 
							status 		= 'req-edit', 
							status_act 	= 'req-to-edit', 
							input_by 	= '$input_by',  
							input_date	= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}elseif ($kat == "approve-to-edit"){

			$query	= "	UPDATE 
							realisasi 
						SET 
							status 				= 'edit', 
							status_act 			= 'approved-to-edit', 
							verify_bila_multi	= 0,
							cat_bila_multi		= '',
							verify_sisdur		= 0,
							cat_sisdur			= '',
							input_by 			= '$input_by',  
							input_date			= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}*/
		
	}

	function realisasi_delete()
	{
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('realisasi', $id);
	}

	function realisasi_submit()
	{
		$id 	= $this->uri->segment(2);
		$id_tw 	= $this->uri->segment(3);
		$kat 	= $this->uri->segment(4);

		$input_by 	= $this->session->userdata('id');
		$timestamp 	= date('Y-m-d H:i:s');

		if ($kat == "submit-add"){
			
			
			$query	= "	UPDATE 
							realisasi 
						SET 
							status 		= 'req-edit', 
							status_act 	= 'req-to-edit', 
							input_by 	= '$input_by',  
							input_date	= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}elseif ($kat == "submit-edit"){

			$query	= "	UPDATE 
							realisasi 
						SET 
							status 				= 'submit-edit', 
							status_act 			= 'submit-edit', 
							input_by 			= '$input_by',  
							input_date			= '$timestamp' 
						WHERE 
							id_proyek = '$id' AND id_triwulan = '$id_tw'
						";
			$result = $this->gm->get_data_qry($query);

		}
		
	}

	function realisasi_save()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("id_proyek", "ID Proyek", "trim|required");
		$this->form_validation->set_rules("id_triwulan", "ID Triwulan", "trim|required");
		$this->form_validation->set_rules("id_nilai_kurs", "ID Nilai Kurs", "trim|required");
		$this->form_validation->set_rules("realisasi_uang", "Realisasi Triwulan", "trim|required|numeric");
		$this->form_validation->set_rules("ket", "Keterangan", "trim|required");
        
		//$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			//cek kode proyek -> unique

			if(empty($this->input->post('id'))){
			
				$data 						= $_POST;

				$data['status']				= "pending-add";
				$data['status_act']			= "add.new";
				$data['submit_operator']	= "0";
				$data['verify_bila_multi']	= "0";
				$data['verify_sisdur']		= "0";
				$data['input_by']			= $this->session->userdata('id');
				$data['input_date']			= date('Y-m-d H:i:s');


				$result				= $this->gm->save_data('realisasi', $data);
				$status['success']  = true;
				
			}else{

				$data 				= $_POST;

				$result				= $this->gm->save_data('realisasi', $data);
				$status['success']  = true;

			}	
		}
		echo json_encode($status);
	} 

	function fisik()
	{
		/*
		$id	= $this->uri->segment(2); //id_proyek
		$data['id_proyek'] = $id;

		$kode = $this->gm->get_data_qry("SELECT kode FROM proyek WHERE id = '$id'")->row('kode');
		$query = "SELECT *, 
						((realisasi_kum_ASLI / nilai_ASLI) * 100) AS persen_kum, 
						(((realisasi_kum_ASLI / nilai_ASLI) * 100) - waktu_terpakai) AS pv, 
						(nilai_ASLI - realisasi_kum_ASLI) AS sisa_pinjaman_ASLI, 
						(nilai_pinjaman_USD - realisasi_kum_USD) AS sisa_pinjaman_USD, 
						(nilai_target_rp / kurs_USD) AS nilai_target_USD, 
						((realisasi_thn_USD / (nilai_target_rp / kurs_USD))*100) AS persen_realisasi_thn  
					FROM 
						v_proyek_realisasi 
					WHERE kode = '$kode'
				";
		$data['realisasi'] = $this->gm->get_data_qry($query);

		//get tw actie
		$data['id_tw']		= $this->gm->get_data_qry("SELECT id FROM triwulan WHERE is_active = '1'")->row('id');
		
		$this->load->view('proyek_efektif/detail/fisik', $data);
		*/
		$this->load->view('proyek_efektif/detail/fisik');
	}

}
