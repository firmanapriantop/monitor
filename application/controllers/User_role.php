<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_role extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->library('encryption');
		$this->load->model('user_role_model');
        $this->load->model('general_model');
		$this->load->model('proyek_model');
    }

	function index()
	{
		if(is_logged_in()) {
			
			$this->load->view('template/header');
			$this->load->view('user_role/index');
			$this->load->view('template/footer');

		}else{
			$this->load->view('template/login'); 		
		}
	}

	function user_role_list()
	{
		if(is_logged_in()) {

			$id_instansi 	= $this->uri->segment(3);
			$id_user_level 	= $this->session->userdata('id_user_level');
			//$data['proyek']	= $this->proyek_model->ambil_data_proyek();

			$data['user_role']	= $this->user_role_model->ambil_data_proyek();

			$this->load->view('user_role/list', $data);
			 
		}else{
			$this->load->view('template/login'); 		
		}
	}
	


}