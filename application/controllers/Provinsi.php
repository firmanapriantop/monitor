<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model(array('general_model' => 'gm'));
    }

	public function index()
	{
		if(is_logged_in()) {


			$data["content"]    = 'provinsi/index';
            $data["isActive"]   = 'provinsi';
            $data['css_js']     = asset_url('provinsi');
            $this->load->view('layout/index', $data);
			

		}else{
			$this->load->view('template/login'); 		
		}

    }

    function show_data()
	{
		$qry = 'SELECT * FROM v_mitra';

		$data['data'] = $this->gm->get_data_qry($qry);
		$this->load->view('mitra/list', $data);
    }

    function add_data()
    {
		 $this->load->view('mitra/add');
	}
	
	function save_data()
    {
        $status = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("tipe", "Tipe Mitra", "trim|required");
        $this->form_validation->set_rules("nama", "Nama Mitra", "trim|required");
        $this->form_validation->set_rules("singkatan", "Singkatan Mitra", "trim|required");
        $this->form_validation->set_rules("ket", "Keterangan", "trim|required");
		$this->form_validation->set_rules("is_active", "Status", "trim|required");
        //$this->form_validation->set_rules("file", "File", "required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			if(empty($this->input->post('id'))){
    
                $data 				= $_POST;
                $result				= $this->gm->save_data('mitra', $data);
                $status['success']  = true;
		
			}else{

				$data 				= $_POST;
				$result				= $this->gm->save_data('mitra', $data);
				$status['success']  = true;
			}	
		}
		echo json_encode($status);
	} 
	
	function edit_data()
	{
		$id     		= $this->uri->segment(2);
		$sql			= "
							SELECT 
                                id,
                                tipe,
								nama,
                                singkatan,
                                ket,
								is_active,
								IF (
									is_active = 1,
									'Active',
									'Inactive'
								) AS status
							FROM
								mitra
							WHERE 
								id = '$id'";

		$qry    		= $this->gm->get_data_qry($sql);
		$data['data']	= $qry->row();

		$this->load->view('mitra/edit', $data);
	}

	function delete_data()
	{	
		$id     = $this->uri->segment(2);
		$query	= $this->gm->delete_data('mitra', $id);
	}
		
		
}
