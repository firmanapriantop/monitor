<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_kurs extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model(array('general_model' => 'gm'));
    }

	public function index()
	{
		if(is_logged_in()) {


			$data["content"]    = 'nilai_kurs/index';
            $data["isActive"]   = 'nilai-kurs';
            $data['css_js']     = asset_url('nilai-kurs');
            $this->load->view('layout/index', $data);
			

		}else{
			$this->load->view('template/login'); 		
		}

    }

    function show_data()
	{
        $qry = "
                SELECT
                    nk.id AS id,
                    CONCAT(
                        'Triwulan ',
                        t.tw,
                        ' Tahun ',
                        t.tahun
                    ) AS triwulan,
                    CONCAT(
                        mu.nama,
                        ' (',
                        mu.singkatan,
                        ')'
                    ) AS mata_uang,
                    nk.nilai AS nilai
                FROM
                    nilai_kurs nk
                INNER JOIN mata_uang mu ON mu.id = nk.id_mata_uang
                INNER JOIN triwulan t ON t.id = nk.id_triwulan
                ORDER BY 
                    t.tahun DESC, 
                    t.tw DESC
        
                ";

		$data['data'] = $this->gm->get_data_qry($qry);
		$this->load->view('nilai_kurs/list', $data);
    }

    function add_data()
    {
        $qry_tw = "
                SELECT 
                        CONCAT('Tahun ', tahun, ' TW ', tw) AS triwulan, 
                        id  
                FROM 
                    triwulan
                ORDER BY 
                    tahun DESC, 
                    tw DESC
                ";

        $qry_mu = "
                SELECT 
                        CONCAT(nama, ' (', singkatan, ')') AS mata_uang, 
                        id  
                FROM 
                    mata_uang
                WHERE 
                    is_active = 1
                ORDER BY 
                    nama ASC
                ";

        $data['triwulan']   = $this->gm->get_data_qry($qry_tw);
        $data['mata_uang']  = $this->gm->get_data_qry($qry_mu);

		$this->load->view('nilai_kurs/add', $data);
	}
	
	function save_data()
    {
        $status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("id_mata_uang", "Mata Uang", "trim|required");
		$this->form_validation->set_rules("id_triwulan", "Triwulan", "trim|required");
        $this->form_validation->set_rules("nilai", "Nilai Kurs", "trim|required|decimal");
        $this->form_validation->set_rules("cat", "Catatan", "trim|required");
        //$this->form_validation->set_rules("file", "File", "required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			
		}else{ //validasi benar semua

			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = "";
			}

			if(empty($this->input->post('id'))){
    
                $data 				= $_POST;
                $result				= $this->gm->save_data('nilai_kurs', $data);
                $status['success']  = true;
		
			}else{

				$data 				= $_POST;
				$result				= $this->gm->save_data('nilai_kurs', $data);
				$status['success']  = true;
			}	
		}
		echo json_encode($status);
	} 
	
	function edit_data()
	{

        $qry_tw = "
                SELECT 
                        CONCAT('Tahun ', tahun, ' TW ', tw) AS triwulan, 
                        id  
                FROM 
                    triwulan
                ORDER BY 
                    tahun DESC, 
                    tw DESC
                ";

        $qry_mu = "
                SELECT 
                        CONCAT(nama, ' (', singkatan, ')') AS mata_uang, 
                        id  
                FROM 
                    mata_uang
                WHERE 
                    is_active = 1
                ORDER BY 
                    nama ASC
                ";

        $data['triwulan']   = $this->gm->get_data_qry($qry_tw);
        $data['mata_uang']  = $this->gm->get_data_qry($qry_mu);

		$id     		= $this->uri->segment(2);
        $sql            = "
                            SELECT
                                nk.id AS id,
                                nk.id_triwulan AS id_triwulan,
                                nk.id_mata_uang AS id_mata_uang,
                                CONCAT(
                                    'Triwulan ',
                                    t.tw,
                                    ' Tahun ',
                                    t.tahun
                                ) AS triwulan,
                                CONCAT(
                                    mu.nama,
                                    ' (',
                                    mu.singkatan,
                                    ')'
                                ) AS mata_uang,
                                nk.nilai AS nilai,
                                nk.cat  AS cat
                            FROM
                                nilai_kurs nk
                            INNER JOIN mata_uang mu ON mu.id = nk.id_mata_uang
                            INNER JOIN triwulan t ON t.id = nk.id_triwulan
                            WHERE 
                                    nk.id = '$id'
                    
                            ";

		$qry    		= $this->gm->get_data_qry($sql);
		$data['data']	= $qry->row();

		$this->load->view('nilai_kurs/edit', $data);
	}

	function delete_data()
	{	
		$id     = $this->uri->segment(2);
        $query	= $this->gm->delete_data('nilai_kurs', $id);
        $status['success']  = true;
		echo json_encode($status);
	}
		
		
}
