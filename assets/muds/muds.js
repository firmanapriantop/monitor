$(document).ready(function(){

    $('.panel [data-action=reload]').click(function (e) {
        e.preventDefault();
        var block = $(this).parent().parent().parent().parent().parent();
        $(block).block({ 
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

        // For demo purposes
        window.setTimeout(function () {
        $(block).unblock();
        }, 2000); 
    });

   // alert('ok');

   $.extend( $.fn.dataTable.defaults, {
		autoWidth: false,
		responsive: true,
		columnDefs: [{ 
			orderable: false,
			width: '100px'
		}],
		dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
		language: {
			search: '<span>Filter:</span> _INPUT_',
			lengthMenu: '<span>Show:</span> _MENU_',
			paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
		},
		drawCallback: function () {
			$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
		},
		preDrawCallback: function() {
			$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
		}
	});


	// Basic responsive configuration
	$('.datatable-responsive').DataTable();


	// Column controlled child rows
	$('.datatable-responsive-column-controlled').DataTable({
		responsive: {
			details: {
				type: 'column'
			}
		},
		columnDefs: [
			{
				className: 'control',
				orderable: false,
				targets:   0
			},
			{ 
				width: "100px",
				targets: [6]
			},
			{ 
				orderable: false,
				targets: [6]
			}
		],
		order: [1, 'asc']
	});


	// Control position
	$('.datatable-responsive-control-right').DataTable({
		responsive: {
			details: {
				type: 'column',
				target: -1
			}
		},
		columnDefs: [
			{
				className: 'control',
				orderable: false,
				targets: -1
			},
			{ 
				width: "100px",
				targets: [5]
			},
			{ 
				orderable: false,
				targets: [5]
			}
		]
	});


	// Whole row as a control
	$('.datatable-responsive-row-control').DataTable({
		responsive: {
			details: {
				type: 'column',
				target: 'tr'
			}
		},
		columnDefs: [
			{
				className: 'control',
				orderable: false,
				targets:   0
			},
			{ 
				width: "100px",
				targets: [6]
			},
			{ 
				orderable: false,
				targets: [6]
			}
		],
		order: [1, 'asc']
	});



	// External table additions
	// ------------------------------

	// Add placeholder to the datatable filter option
	$('.dataTables_filter input[type=search]').attr('placeholder','Difilter saja ...');


	// Enable Select2 select for the length option
	$('.dataTables_length select').select2({
		minimumResultsForSearch: Infinity,
		width: 'auto'
	});

});
   
function loadData(page, id){
	/*
	$.get(page, function(data) {
		//$("#"+id).html(data);
		$("#"+id).load(page);
	});
	*/
	$.ajax({
		async: true, 
		url: page,
		beforeSend: function(){
			showLoading2(id);
		}
	}).done(function(data) {
		$("#"+id).html(data);
	});
}

function CallPage(page, bgmodal, modal)
{
	$.ajax({
		url: page,
		beforeSend: function(){
			//showLoading();
			//$('#loader').fadeOut(1000);
		},
		success:function(response){
			$("#"+bgmodal).html(response);
			$('#'+modal).modal('show');
		},
		dataType:"html"
	});
}

function submit_data(page, refresh_link){
	var judul = "Apakah Anda yakin melakukan Submit untuk data ini?";
	var isi = "Perhatian, data yang telah disubmit akan di-lock. Terima kasih"; 

	swal({
		title: judul,
		text: isi,
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: page,
					success:function(response){
						swal("Data berhasil di-submit!", {
							icon: "success",
						});
						loadData(refresh_link, "data");
					},
					error: function(){
						alert('Error Updating!');
					},
					dataType:"html"
				});

				
				
			}
		});
	
}

function deleteData(page, refresh_page, nama_div)
{
	swal({
		title: "Apakah Anda yakin menghapus data ini?",
		text: "Perhatian, sekali data dihapus, maka data ini tidak dapat di-recover lagi.",
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: page,
				type: "POST",
				dataType: 'json',
				success:function(response){
					if (response.success == true) {
						swal("Data berhasil dihapus!", {
							icon: "success",
						});
						loadData(refresh_page, nama_div);
					} else {
						swal(response.messages, {
							icon: "error",
						});
					}
					
					
				},
				error: function(xhr, statusText){
					alert("Error: "+xhr.status+" - "+statusText);
				}
				
			});
			
		} else {
			swal("Your imaginary file is safe!");
		}
		});
}

function deleteData_awal(page, refresh_page, nama_div)
{
	swal({
		title: "Apakah Anda yakin menghapus data ini?",
		text: "Perhatian, sekali data dihapus, maka data ini tidak dapat di-recover lagi.",
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: page,
				type: "POST",
				data: $('#htmlForm').serialize(),
				success:function(html){
					
					swal("Data berhasil dihapus!", {
						icon: "success",
					});
					loadData(refresh_page, nama_div);
				},
				error: function(){
					alert('Error Updating!');
				}
				
			});
			
		} else {
			swal("Your imaginary file is safe!");
		}
		});
}

function addData(btn, page, modal)
{
    $(btn).click(function(){
        $.ajax({
                url: page,
                success:function(response){
                    //$("#tmpModal23").html(response);
                    $('#modalTambah').modal('show');
                },
                dataType:"html"
            });
        return false;
    });
}
		
function showLoading(){
	//document.getElementById("loader").style = "visibility: visible";
	//$('#loader').fadeOut(1000);
	/*
	$('.panel-flat').block({
		message: '<i class="icon-spinner spinner"></i>',
		overlayCSS: {
			backgroundColor: '#fff',  //#1B2024
			opacity: 0.85,
			cursor: 'wait'
		},
		css: {
			border: 0,
			padding: 0,
			backgroundColor: 'none'
		}
	});
	window.setTimeout(function () {
		$('.panel-flat').unblock();
	}, 5000);

*/
	var light_2 = $(this).parent();
	$('.panel').block({
		message: '<i class="icon-spinner2 spinner"></i>',
		overlayCSS: {
			backgroundColor: '#fff',
			opacity: 0.8,
			cursor: 'wait'
		},
		css: {
			border: 0,
			padding: 0,
			backgroundColor: 'none'
		}
	});
	window.setTimeout(function () {
		$('.panel').unblock();
	}, 2000);

	
}

function showLoading2(panel){
	var light_2 = $(this).parent();
	var tab = '#'+panel;
	$(tab).block({
		message: '<i class="icon-spinner2 spinner"></i>',
		overlayCSS: {
			backgroundColor: '#fff',
			opacity: 0.8,
			cursor: 'wait'
		},
		css: {
			border: 0,
			padding: 0,
			backgroundColor: 'none'
		}
	});
	window.setTimeout(function () {
		$(tab).unblock();
	}, 2000);

	
}

$('#pnotify-solid-success').on('click', function () {
	new PNotify({
		title: 'Success notice',
		text: 'Check me out! I\'m a notice.',
		addclass: 'bg-success'
	});
});


function notif(judul, isi){
	setTimeout(function() {
		new PNotify({
			title: '<b>'+judul+'</b>',
			text: isi,
			addclass: 'bg-success'
		});
	}, 1300);
}

function notif_bahaya(){
	var isi = "Silahkan hubungi Bang Firman.";
	var judul = "Bah!";
	
	new PNotify({
		title: '<b>Now look here</b>',
		text: 'There\'s something you need to know, and I won\'t go away until you come to grips with it.',
		addclass: 'bg-danger',
		hide: false,
		buttons: {
			closer: true,
			sticker: false
		}
	});
        
}

function formatAngka(angka) {
    if (typeof(angka) != 'string') angka = angka.toString();
    var reg = new RegExp('([0-9]+)([0-9]{3})');
    while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
    return angka;
}

function addCommas(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
